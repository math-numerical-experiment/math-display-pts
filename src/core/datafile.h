#ifndef _DATAFILE_H_
#define _DATAFILE_H_

#include <zlib.h>
#include <glib.h>
#include <glib/gregex.h>
#include "dataobj.h"

typedef enum {
  DATA_FILE_TYPE_PLAIN,
  DATA_FILE_TYPE_GZ
} DATA_FILE_TYPE;

typedef struct {
  const char *path;
  DATA_FILE_TYPE type;
  void *fp;
} DATA_FILE;

DATA_FILE *data_file_new (const char *path);
void data_file_open_read (DATA_FILE *data_file);
void data_file_close (DATA_FILE *data_file);
void data_file_delete (DATA_FILE *data_file);
bool data_file_gets_line(char **line, int *line_size, int *max_line_size, DATA_FILE *data_file);

void dataobj_init_read_file(DataObj *obj, const char *file);

#endif /* _DATAFILE_H_ */
