#ifndef _QUATERNION_H_
#define _QUATERNION_H_

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include <GL/glc.h>
#include <math.h>

void quat_copy(GLdouble ret[], GLdouble u[]);
void quat_conjugate(GLdouble ret[], GLdouble r[]);
void quat_multiple(GLdouble ret[], GLdouble u[], GLdouble v[]);
void quat_rotation_to_quaternion(GLdouble q[], GLdouble angle, GLdouble direction[]);
void quat_rotation_matrix(GLdouble m[][4], GLdouble q[]);
void quat_inverse_rotation_matrix(GLdouble m[][4], GLdouble q[]);

#endif /* _QUATERNION_H_ */
