#ifndef _DATAOBJ_FR_H_
#define _DATAOBJ_FR_H_

#include <stdarg.h>
#include "ptdata_fr.h"
#include "draw_ptdata.h"
#include "dataobj.h"
#include <GL/freeglut.h>
#include <GL/glc.h>

typedef struct __MPFRDataObject{
  MPFRPtData data[1];
  int data_size;
  int *split;
  int split_used;
  int split_allocated;
  PtData *dbl;
  int dbl_size;
  PlotStyle default_style;
} MPFRDataObject;

void mpfr_dataobj_init(MPFRDataObject *obj);
void mpfr_dataobj_free_fr(MPFRDataObject *obj);
void mpfr_dataobj_free_dbl(MPFRDataObject *obj);
void mpfr_dataobj_free(MPFRDataObject *obj);

void mpfr_dataobj_allocate_fr_if_needed(MPFRDataObject *obj, int num);
void mpfr_dataobj_set_split(MPFRDataObject *obj, int split_num);
void mpfr_dataobj_delete_all_splits(MPFRDataObject *obj);
void mpfr_dataobj_add_fr(MPFRDataObject *obj, mpfr_t fr);
void mpfr_dataobj_add_frs(MPFRDataObject *obj, int num, __mpfr_struct *frs);
void mpfr_dataobj_add_vfrs(MPFRDataObject *obj, int num, ...);
void mpfr_dataobj_allocate_dbl_if_needed(MPFRDataObject *obj, int num);
void mpfr_dataobj_add_dbl(MPFRDataObject *obj, double dbl);
void mpfr_dataobj_add_dbls(MPFRDataObject *obj, int num, double *dbls);
void mpfr_dataobj_add_vdbls(MPFRDataObject *obj, int num, ...);

void mpfr_dataobj_translation(double *ret, __mpfr_struct *x, __mpfr_struct *base_pt, __mpfr_struct *base_scale);
void mpfr_dataobj_set_mpfr_to_dbl(MPFRDataObject *obj);
void mpfr_dataobj_set_dbl_data(MPFRDataObject *obj, __mpfr_struct *base_pt, __mpfr_struct *base_scale);
void mpfr_dataobj_set_default_style(MPFRDataObject *obj, PlotStyle style);

void mpfr_dataobj_init_read_file(MPFRDataObject *obj, const char *file);

void mpfr_dataobj_draw_lines(MPFRDataObject *obj);
void mpfr_dataobj_draw_dots(MPFRDataObject *obj);
void mpfr_dataobj_draw_quads(MPFRDataObject *obj);
void mpfr_dataobj_draw_quads_edge(MPFRDataObject *obj);
void mpfr_dataobj_draw_points_cross(MPFRDataObject *obj, double size);
void mpfr_dataobj_output_to_gnuplot(MPFRDataObject *obj, FILE *pipe_gp);
void mpfr_dataobj_output_to_gnuplot_for_quads(MPFRDataObject *obj, FILE *pipe_gp);
void mpfr_dataobj_output_to_gnuplot_default_style(MPFRDataObject *obj, FILE *pipe_gp);

#endif /* _DATAOBJ_FR_H_ */

