#include "datafile.h"

/* MAX_DATA_STRING_SIZE_ONE_LINE must be divided by 8 */
#define MAX_DATA_STRING_SIZE_ONE_LINE 64
#define DATA_DELIMITERS "\t "

static void data_file_init_line(int *max_line_size, char **line)
{
  *max_line_size = MAX_DATA_STRING_SIZE_ONE_LINE;
  *line = (char *) malloc(sizeof(char) * (*max_line_size));
}

/* Return false if the line ends or an error raises. */
static bool data_file_fgets(char *line, int max_line_size, DATA_FILE *data_file)
{
  switch (data_file->type) {
  case DATA_FILE_TYPE_PLAIN:
    return (fgets(line, max_line_size, data_file->fp) != NULL);
  case DATA_FILE_TYPE_GZ:
    return (gzgets(data_file->fp, line, max_line_size) != NULL);
  default:
    fprintf(stderr, "Invalid data file type");
    abort();
    break;
  } 
}

/* Return true if a line is got. Otherwise, false. */
bool data_file_gets_line(char **line, int *line_size, int *max_line_size, DATA_FILE *data_file)
{
  if (data_file_fgets(*line, *max_line_size, data_file)) {
    do {
      *line_size = strlen(*line);
      if((*line)[*line_size - 1] == '\n'){
        (*line)[*line_size - 1] = '\0';
        *line_size -= 1;
        break;
      }
      *max_line_size += MAX_DATA_STRING_SIZE_ONE_LINE;
      *line = (char *) realloc(*line, sizeof(char) * (*max_line_size));
    } while (data_file_fgets(*line + (*line_size), MAX_DATA_STRING_SIZE_ONE_LINE - 1, data_file));
    return true;
  }
  return false;
}

static DATA_FILE_TYPE data_file_get_file_type (const char *path)
{
  if (g_regex_match_simple("\\.gz$", path, 0, 0)) {
    return DATA_FILE_TYPE_GZ;
  } else {
    return DATA_FILE_TYPE_PLAIN;
  }
}

DATA_FILE *data_file_new (const char *path)
{
  DATA_FILE *data_file;
  data_file = (DATA_FILE *) malloc(sizeof(DATA_FILE));
  data_file->path = path;
  data_file->fp = NULL;
  data_file->type = data_file_get_file_type(path);
  return data_file;
}

void data_file_open_read (DATA_FILE *data_file)
{
  switch (data_file->type) {
  case DATA_FILE_TYPE_PLAIN:
    data_file->fp = fopen(data_file->path, "r");
    break;
  case DATA_FILE_TYPE_GZ:
    data_file->fp = gzopen(data_file->path, "r");
    break;
  default:
    fprintf(stderr, "Invalid data file type");
    abort();
    break;
  }
  if (data_file->fp == NULL) {
    fprintf(stderr, "ERROR: Can not open \"%s\".\n", data_file->path);
    abort();
  }
}

void data_file_close (DATA_FILE *data_file)
{
  if (data_file->fp) {
    switch (data_file->type) {
    case DATA_FILE_TYPE_PLAIN:
      fclose(data_file->fp);
      break;
    case DATA_FILE_TYPE_GZ:
      break;
    default:
      fprintf(stderr, "Invalid data file type");
      abort();
      break;
    }
    data_file->fp = NULL;
  }
}

void data_file_delete (DATA_FILE *data_file)
{
  data_file_close(data_file);
  free(data_file);
}

static void data_file_set_obj_double (DATA_FILE *data_file, DataObj *obj)
{
  gchar **buf;
  int max_line_size, line_size, i;
  char *line;
  double *pt;
  data_file_init_line(&max_line_size, &line);
  pt = dataobj_allocate_double_pt(obj->dim);
  while (data_file_gets_line(&line, &line_size, &max_line_size, data_file)) {
    if (line[0] == '\0') {
      dataobj_add_splitter(obj, -1);
    } else if (line[0] != '#') {
      buf = g_strsplit_set(line, DATA_DELIMITERS, 0);
      for (i = 0; i < obj->dim; i++) {
        if (buf[i]) {
          pt[i] = atof(buf[i]);
        } else {
          fprintf(stderr, "Invalid data: number of culomns of data and dimension does not coincide.\n");
          abort();
        }
      }
      if (buf[i]) {
        fprintf(stderr, "Invalid data: number of culomns of data and dimension does not coincide.\n");
        abort();
      }
      dataobj_copy_insert(obj, -1, pt);
      g_strfreev(buf);
    }
  }
  free(pt);
  free(line);
}

void dataobj_init_read_file(DataObj *obj, const char *file)
{
  DATA_FILE *data_file;
  data_file = data_file_new(file);
  data_file_open_read(data_file);
  switch (obj->data_type) {
  case DATA_DOUBLE:
    data_file_set_obj_double(data_file, obj);
    break;
  case DATA_MPFR:
    printf("Not yet implemented.\n");
    abort();
    break;
  case DATA_MPFI:
    printf("Not yet implemented.\n");
    abort();
    break;
  default:
    printf("Invalid data type.\n");
    abort();
  }
  data_file_delete(data_file);
}
