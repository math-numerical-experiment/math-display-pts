#include "ptdata.h"

void ptdata_init(PtData *ptdata){
  ptdata->size = 0;
  ptdata->ptr_allocated = 0;
  ptdata->ptr_max = PTDATA_PTR_SIZE;
  ptdata->ptr = (double **) malloc(PTDATA_PTR_SIZE * sizeof(double *));
  if (ptdata->ptr == NULL) {
    printf("Can not allocate memory.\n");
    abort();
  }
}

void ptdata_free(PtData *ptdata){
  int i;
  for (i = 0; i < ptdata->ptr_allocated; i++) {
    free(*(ptdata->ptr + i));
  }
  free(ptdata->ptr);
}

void ptdata_allocate_ptr(PtData *ptdata){
  ptdata->ptr_max += PTDATA_PTR_SIZE;
  ptdata->ptr = (double **) realloc(ptdata->ptr, ptdata->ptr_max * sizeof(double *));
  if (ptdata->ptr == NULL) {
    printf("Can not allocate memory.\n");
    abort();
  }
}

void ptdata_allocate_farther(PtData *ptdata){
  if(ptdata->ptr_allocated < ptdata->ptr_max - 1){
    *(ptdata->ptr + ptdata->ptr_allocated) = (double *) malloc(PTDATA_MAX_DOUBLE_ARY_SIZE * sizeof(double));
    if (ptdata->ptr[ptdata->ptr_allocated] == NULL){
      printf("Can not allocate memory.\n");
      abort();
    }
    ptdata->ptr_allocated += 1;
    ptdata->size += PTDATA_MAX_DOUBLE_ARY_SIZE;
  }else{
    ptdata_allocate_ptr(ptdata);
    ptdata_allocate_farther(ptdata);
  }
}

double *ptdata_get_dbl_ptr(PtData *ptdata, int num){
  div_t res = div(num, PTDATA_MAX_DOUBLE_ARY_SIZE);
  return *(ptdata->ptr + res.quot) + res.rem;
}

bool ptdata_allocated_p(PtData *ptdata, int num){
  return (num < ptdata->size ? true : false);
}

