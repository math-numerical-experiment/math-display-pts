#include "display_func.h"

#define GNUPLOT_PREFIX "img"
#define GNUPLOT_MAX_NAME 100

void set_filename_string(char **strs, int num, const char *name){
  if(strlen(name) > SCREEN_MAX_STRING_SIZE_ONE_LINE - 10){
    char tmp_line[SCREEN_MAX_STRING_SIZE_ONE_LINE];
    strncpy(tmp_line, name, SCREEN_MAX_STRING_SIZE_ONE_LINE - 10);
    tmp_line[SCREEN_MAX_STRING_SIZE_ONE_LINE - 10] = '\0';
    sprintf(*(strs + num), "[%d] %s", num, tmp_line);
  }else{
    sprintf(*(strs + num), "[%d] %s", num, name);
  }
}

void disp_init_mfd(DisplayState *disp, int num){
  int i;
  disp->num_of_mfds = num;
  disp->mfd = (bool *) malloc(sizeof(disp) * num);
  for(i = 0; i < num; i++){
    *(disp->mfd + i) = true;
  }
}

void disp_toggle_mfd(DisplayState *disp, int num){
  *(disp->mfd + num) = (*(disp->mfd + num) ? false : true);
}

int disp_number_showing_mfds(DisplayState *disp){
  int i, num = 0;
  for(i = 0; i < disp->num_of_mfds; i++){
    if(*(disp->mfd + i)) num++;
  }
  return num;
}

void disp_toggle_scale_bar(DisplayState *disp){
  disp->scale_bar = (disp->scale_bar ? false : true);
}

void disp_toggle_grid(DisplayState *disp){
  disp->grid = (disp->grid ? false : true);
}

void disp_cycle_state(DisplayState *disp){
  disp->state += 1;
  if(disp->state == disp->num_of_state){
    disp->state = 0;
  }
}

void disp_draw_objects(DisplayState *disp, Screen2D *screen, Color *col, DataObject *objects, int obj_num){
  if(disp_number_showing_mfds(disp) > 0){
    int i;
    for(i = 0; i < obj_num; i++){
      color_change(col);
      if(disp->mfd[i]){
	switch((objects + i)->default_style){
	case STY_DOTS:
	  dataobj_draw_dots(objects + i);
	  break;
	case STY_LINES:
	  dataobj_draw_lines(objects + i);
	  break;
	case STY_POINTS:
	  dataobj_draw_points_cross(objects + i, screen->cross_size / screen->zoom);
	  break;
	case STY_LINES_POINTS:
	  dataobj_draw_lines(objects + i);
	  dataobj_draw_points_cross(objects + i, screen->cross_size / screen->zoom);
	  break;
	case STY_QUADS:
	  dataobj_draw_quads(objects + i);
	  break;
	case STY_QUADS_EDGE:
	  dataobj_draw_quads_edge(objects + i);
	  break;
	}
      }
    }
  }
}

void disp_gnuplot_objects(DisplayState *disp, Screen2D *screen, DataObject *objects, int obj_num){
  if(disp_number_showing_mfds(disp) > 0){
    char gp_file[GNUPLOT_MAX_NAME];
    time_t now;
    struct tm *t_st_now;
    FILE *gp;
    int i;
    bool first = true;

    time(&now);
    t_st_now = localtime(&now);
    sprintf(gp_file, "%s%d%02d%02d%02d%02d%02d", GNUPLOT_PREFIX,
	    t_st_now->tm_year+1900, t_st_now->tm_mon+1, t_st_now->tm_mday, t_st_now->tm_hour, t_st_now->tm_min, t_st_now->tm_sec);
    gp = popen("gnuplot","w");
    printf("Making %s.eps\n", gp_file);
    fprintf(gp, "set term postscript eps\nset output '%s.eps'\n", gp_file);
    screen2d_gnuplot_set_range(screen, gp);
    fprintf(gp, "set size ratio -1\n");
    screen2d_gnuplot_set_divisions(screen, gp);

    fprintf(gp, "plot ");
    for(i = 0; i < obj_num; i++){
      if(disp->mfd[i]){
	if(!first){
	  fprintf(gp, ", ");
	}else{
	  first = false;
	}
	draw_ptdata_puts_gnuplot_style(gp, (objects + i)->default_style);
      }
    }
    fprintf(gp, "\n");
      
    for(i = 0; i < obj_num; i++){
      if(disp->mfd[i]){
	dataobj_output_to_gnuplot_default_style(objects + i, gp);
      }
    }
    pclose(gp);
    printf("Finish making %s.eps\n", gp_file);
  }
}

