#ifndef _DATAOBJ_H_
#define _DATAOBJ_H_

#include <stdarg.h>
#include "draw_ptdata.h"
#include "datafile.h"

#define DEFAULT_STYLE STY_DOTS

typedef struct __DataObject{
  PtData data[1];
  int data_size;
  int *split;
  int split_used;
  int split_allocated;
  PlotStyle default_style;
} DataObject;

void dataobj_init(DataObject *obj);
void dataobj_free(DataObject *obj);
void dataobj_allocate_if_needed(DataObject *obj, int num);
void dataobj_set_split(DataObject *obj, int split_num);
void dataobj_delete_all_splits(DataObject *obj);
void dataobj_add_dbl(DataObject *obj, double dbl);
void dataobj_add_dbls(DataObject *obj, int num, double *dbls);
void dataobj_add_vdbls(DataObject *obj, int num, ...);
void dataobj_set_default_style(DataObject *obj, PlotStyle style);

void dataobj_read_line_init(int *max_line_size, char **line);
char *dataobj_gets_line(char **line, int *line_size, int *max_line_size, FILE *fp);
void dataobj_init_read_file(DataObject *obj, const char *file);

void dataobj_draw_lines(DataObject *obj);
void dataobj_draw_dots(DataObject *obj);
void dataobj_draw_quads(DataObject *obj);
void dataobj_draw_quads_edge(DataObject *obj);
void dataobj_draw_points_cross(DataObject *obj, double size);
void dataobj_output_to_gnuplot(DataObject *obj, FILE *pipe_gp);
void dataobj_output_to_gnuplot_for_quads(DataObject *obj, FILE *pipe_gp);
void dataobj_output_to_gnuplot_default_style(DataObject *obj, FILE *pipe_gp);

#endif /* _DATAOBJ_H_ */

