#ifndef _PTDATA_H_
#define _PTDATA_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/* PTDATA_MAX_DOUBLE_ARY_SIZE must be divided by 8 */
#define PTDATA_MAX_DOUBLE_ARY_SIZE 1024
#define PTDATA_PTR_SIZE 1024

typedef struct __PtData{
  double **ptr;
  int size;
  int ptr_allocated;
  int ptr_max;
} PtData;

void ptdata_init(PtData *ptdata);
void ptdata_free(PtData *ptdata);
void ptdata_allocate_farther(PtData *ptdata);
double *ptdata_get_dbl_ptr(PtData *ptdata, int num);
bool ptdata_allocated_p(PtData *ptdata, int num);
int ptdata_set_data(PtData *ptdata, char *file);

#endif /* _PTDATA_H_ */
