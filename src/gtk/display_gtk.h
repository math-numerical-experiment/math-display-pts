#ifndef _DISPLAY_GTK_H_
#define _DISPLAY_GTK_H_

#include <gtk/gtk.h>
#include <gtk/gtkgl.h>
#include <GL/gl.h>

#include "../core/displaypts_cli.h"

#define INITIAL_WINDOW_TITLE "Display 3D"
#define INITIAL_WINDOW_WIDTH 800
#define INITIAL_WINDOW_HEIGHT 800

typedef struct {
  GLdouble center[3];
  GLdouble size[4];
  GLfloat light_position[4];
  /* GLdouble init_viewpoint = 5.0; */
  /* GLfloat init_light_position[4] = { 1.0, 1.0, 1.0, 0.0 }; */
  double plane_center[3];
  const char *default_plane;
  gboolean show_axis;
} InitSettings;

Screen *screen;
CreateImage cimage[1];
InitSettings *settings;

GtkWidget *create_menu_window ();

GtkWidget *create_drawing_window (const char *init_plane);

#endif /* _DISPLAY_GTK_H_ */
