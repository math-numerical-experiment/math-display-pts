#ifndef _IMAGEFILES_H_
#define _IMAGEFILES_H_

#include "header.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <time.h>
#include <gl2ps.h>

typedef struct {
  char *basename;
  int format;
  int count;
} CreateImage;

void disp_cimage_init(CreateImage *cimage);
void disp_cimage_free(CreateImage *cimage);
void disp_cimage_set_basename(CreateImage *cimage, const char *name);
void disp_cimage_set_format(CreateImage *cimage, int format);
void disp_cimage_output(CreateImage *cimage, void (*display)(void));
void disp_cimage_screenshot(CreateImage *cimage, int width, int height);

#endif /* _IMAGEFILES_H_ */
