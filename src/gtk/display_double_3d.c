#include "../core/displaypts_cli.h"

#define INITIAL_WINDOW_TITLE "Display 3D"
#define INITIAL_WINDOW_POSITION_X 0
#define INITIAL_WINDOW_POSITION_Y 0
#define INITIAL_WINDOW_WIDTH 800
#define INITIAL_WINDOW_HEIGHT 800

#define SCREEN_INITIAL_CRD_X 0.0
#define SCREEN_INITIAL_CRD_Y 0.0
#define SCREEN_INITIAL_ZOOM 1.0
#define SCREEN_INITIAL_CRD_SIZE_X 2.0
#define SCREEN_INITIAL_CRD_SIZE_y 2.0

#define SCREEN_DEFAULT_MOVE 1.0
#define SCREEN_DEFAULT_ZOOM 2.0
#define SCREEN_GRID_SIZE SCREEN_INITIAL_CRD_SIZE_X / 3.0
#define SCREEN_POINT_CROSS_SIZE 0.03

#define SCREEN_MOVEMENT 1.0
#define SCREEN_ROTATION 360.0 / 12.0

#define SPIRAL_ROTATION 0.1

#define DIMENSION 3
#define NUMBER_DISPLAY_TYPE 5


Screen *screen;
CreateImage cimage[1];

void display_func(void){
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  /* static GLfloat lit_amb[4]={1.0, 1.0, 1.0, 0.0}; */
  /* static GLfloat lit_dif[4]={1.0, 1.0, 1.0, 0.0}; */
  /* static GLfloat lit_spc[4]={1.0, 1.0, 1.0, 0.0}; */
  /* static GLfloat lit_pos[4]={3.0, 3.0, 5.0, 0.0}; */

  /* glLightfv(GL_LIGHT0, GL_AMBIENT, lit_amb); */
  /* glLightfv(GL_LIGHT0, GL_DIFFUSE, lit_dif); */
  /* glLightfv(GL_LIGHT0, GL_SPECULAR, lit_spc); */
  /* glLightfv(GL_LIGHT0, GL_POSITION, lit_pos); */

  /* glEnable(GL_LIGHT0); */
  /* glEnable(GL_LIGHTING); */
  
  /* gl2psEnable(GL2PS_BLEND); */
  
  /* glClearColor(1.0, 1.0, 1.0, 0.0); */
  /* glClearColor(1.0, 1.0, 1.0, 0.0); */

  glEnable(GL_LIGHT0);
  glEnable(GL_LIGHTING);
  
  glMatrixMode(GL_MODELVIEW);


  glEnable(GL_DEPTH_TEST);

  glColor4d(1.0, 0.3, 0.3, 1.0);

  GLfloat material_ambient[] = { 0.135, 0.2225, 0.1575, 1.0 };
  GLfloat material_diffuse[] = { 0.54, 0.89, 0.63, 1.0 };
  GLfloat material_specular[] = { 0.316228, 0.316228, 0.316228, 1.0 };
  GLfloat material_shininess[] = { 12.8 };
  /* GLfloat material_ambient[] = { 0.1, 0.1, 0.1, 1.0 }; */
  /* GLfloat material_diffuse[] = { 0.6, 0.6, 0.6, 1.0 }; */
  /* GLfloat material_specular[] = { 0.0, 0.0, 0.0, 1.0 }; */
  /* GLfloat material_shininess[] = { 20.0 }; */
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material_ambient);
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material_diffuse);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material_specular);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, material_shininess);

  /* glutSolidTeapot(2); */

  /* glBegin(GL_POLYGON); */
  /* glNormal3f(3 , 0 , -2); */
  /* glVertex3f(0 , -0.9 , -2); */
  /* glVertex3f(3 , -0.9 , -7); */
  /* glVertex3f(0 , 0.9 , -2); */
  /* glNormal3f(-3 , 0 , -2); */
  /* glVertex3f(0 , -0.9 , -2); */
  /* glVertex3f(-3 , -0.9 , -7); */
  /* glVertex3f(0 , 0.9 , -2); */
  /* glEnd(); */

  /* glColor4d(1.0, 1.0, 0.0, 0.0); */
  screen_plot_all_objects(screen);

  /* glPushMatrix(); */
  /* glTranslated(0.0, 0.0, -10.0); */
  /* glutSolidSphere(1.0, 100, 100); */
  /* glPopMatrix(); */
  
  glDisable(GL_LIGHTING);

  glColor4d(0.3, 0.3, 1.0, 1.0);
  screen_show_axis(screen);

  glColor4d(0.6, 0.0, 0.6, 0.5);
  screen_show_plane(screen);


  glDisable(GL_DEPTH_TEST);

  glutSwapBuffers();
  /* glFlush(); */
}

void reshape_func(int w, int h){
  screen_change_window_size(screen, w, h);
  screen_apply_projection(screen);
  glViewport(0, 0, w, h);
}

void mouse_func(int button, int state, int x, int y){
  /* if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){ */
  /*   screen2d_move_to_pixel(screen, x, y); */
  /*   glutPostRedisplay(); */
  /* } */
}

void keyboard_func(unsigned char key, int x, int y){
  switch(key){
  case 'q':
    glutLeaveMainLoop();
    break;
  case 'C':
    screen_reset_rotation(screen);
    break;
  case 'r':
    screen_rotate_around_x(screen, SCREEN_ROTATION);
    break;
  case 'R':
    screen_rotate_around_x(screen, -SCREEN_ROTATION);
    break;
  case 'e':
    screen_rotate_around_y(screen, SCREEN_ROTATION);
    break;
  case 'E':
    screen_rotate_around_y(screen, -SCREEN_ROTATION);
    break;
  case 'w':
    screen_rotate_around_z(screen, SCREEN_ROTATION);
    break;
  case 'W':
    screen_rotate_around_z(screen, -SCREEN_ROTATION);
    break;
  case 'i':
    screen_zoom(screen, 2.0);
    break;
  case 'u':
    screen_zoom(screen, 1.0 / 2.0);
    break;
  case 'h':
    screen_left(screen, SCREEN_MOVEMENT);
    break;
  case 'j':
    screen_down(screen, SCREEN_MOVEMENT);
    break;
  case 'k':
    screen_up(screen, SCREEN_MOVEMENT);
    break;
  case 'l':
    screen_right(screen, SCREEN_MOVEMENT);
    break;
  case 'f':
    screen_forward(screen, SCREEN_MOVEMENT);
    break;
  case 'b':
    screen_backward(screen, SCREEN_MOVEMENT);
    break;
  case 'o':
    disp_cimage_screenshot(cimage, screen->win_size[0], screen->win_size[1]);
    break;
  case 'O':
    disp_cimage_output(cimage, display_func);
    break;
  default:
    return;
  }
  glutPostRedisplay();
}

double sp_angle, spiral[3];

/* void animate_func(int value) { */
/*   /\* DataObj *obj; *\/ */
/*   /\* obj = screen_get_object(screen, 0); *\/ */
/*   /\* sp_angle += SPIRAL_ROTATION; *\/ */
/*   /\* spiral[0] += SPIRAL_ROTATION; *\/ */
/*   /\* spiral[1] = cos(sp_angle); *\/ */
/*   /\* spiral[2] = sin(sp_angle); *\/ */
/*   /\* dataobj_copy_insert(obj, -1, spiral); *\/ */
/*   /\* if (sp_angle > 2 * M_PI) { *\/ */
/*   /\*   sp_angle -= 2 * M_PI; *\/ */
/*   /\* } *\/ */
/*   glutPostRedisplay(); */
/*   glutTimerFunc(100, animate_func, 0); */
/* } */

int main (int argc, char *argv[])
{
  const char *progname = "display-double-3d";

  struct arg_lit *help = arg_lit0("h", "help", "print this help and exit");

  struct arg_file *pt_files = arg_filen("p", "points", "<file>", 0, argc + 2, "files for displaying points");
  struct arg_file *dots_files = arg_filen("d", "dots", "<file>", 0, argc + 2, "files for displaying dots");
  struct arg_file *lines_files = arg_filen("l", "lines", "<file>", 0, argc + 2, "files for displaying lines");
  struct arg_file *linespts_files = arg_filen("i", "linespoints", "<file>", 0, argc + 2, "files for displaying linespoints");
  struct arg_str *default_style_arg = arg_strn("s", "style", "<style>", 0, 1, "default style for displaying.");
  struct arg_file *surface_files = arg_filen("s", "surface", "<file>", 0, argc + 2, "files for displaying surface");
  struct arg_file *files = arg_filen(NULL, NULL, "<file>", 0, argc + 2, "files for displaying lines");

  struct arg_str *init_plane = arg_strn(NULL, "plane", NULL, 0, 1, "Set the initial plane.");

  struct arg_end *end = arg_end(20);
  void* argtable[] = {
    pt_files, dots_files, lines_files, linespts_files, default_style_arg,
    surface_files, help, files, init_plane, end
  };

  int nerrors = arg_parse(argc, argv, argtable);
  
  if (help->count > 0) {
    printf("Usage: %s", progname);
    arg_print_syntax(stdout, argtable,"\n");
    printf("Display points in files.\n\n");
    arg_print_glossary(stdout, argtable," %-30s %s\n");
    printf("\nReport bugs to <yt@math.sci.hokudai.ac.jp>.\n");
    exit(0);
  }

  if (nerrors > 0){
    /* Display the error details contained in the arg_end struct.*/
    arg_print_errors(stdout, end, progname);
    printf("Try '%s --help' for more information.\n", progname);
    exit(1);
  }

  DisplayType default_style = DISPLAY_TYPE_LINES;
  if (default_style_arg->count > 0) {
    switch (default_style_arg->sval[0][0]) {
    case 'p':
      default_style = DISPLAY_TYPE_POINTS;
      break;
    case 'd':
      default_style = DISPLAY_TYPE_DOTS;
      break;
    case 'l':
      if (strlen(default_style_arg->sval[0]) >= 6 && default_style_arg->sval[0][5] == 'p') {
        default_style = DISPLAY_TYPE_LINES_POINTS;
      } else {
        default_style = DISPLAY_TYPE_LINES;
      }
      break;
    default:
      fprintf(stderr, "Invalid style: %s", default_style_arg->sval[0]);
      abort();
    }
  }

  DataFileArg *arg;
  arg = data_file_arg_new();
  data_file_arg_add(arg, pt_files, DISPLAY_TYPE_POINTS, DATA_DOUBLE, DIMENSION);
  data_file_arg_add(arg, dots_files, DISPLAY_TYPE_DOTS, DATA_DOUBLE, DIMENSION);
  data_file_arg_add(arg, lines_files, DISPLAY_TYPE_LINES, DATA_DOUBLE, DIMENSION);
  data_file_arg_add(arg, linespts_files, DISPLAY_TYPE_LINES_POINTS, DATA_DOUBLE, DIMENSION);
  data_file_arg_add(arg, surface_files, DISPLAY_TYPE_SURFACE, DATA_DOUBLE, DIMENSION);
  data_file_arg_add(arg, files, default_style, DATA_DOUBLE, DIMENSION);

  DataObj **obj_list;
  int obj_num, i;
  obj_list = data_file_arg_initialize_objects(arg, &obj_num);

  GLdouble init_center[3] = { 0.0, 0.0, 0.0 };
  GLdouble init_size[4] = { 8.0, 8.0, 10.0, 100.0 };
  /* GLdouble init_viewpoint = 5.0; */
  GLfloat init_light_position[4] = { 1.0, 0.0, 1.0, 0.0 };
  /* GLfloat init_light_position[4] = { 1.0, 1.0, 1.0, 0.0 }; */

  disp_cimage_init(cimage);
  disp_cimage_set_basename(cimage, progname);
  
  int glut_argc = 0;
  glutInit(&glut_argc, NULL);
  /* glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH); */
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
  glutInitWindowSize(INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);
  glutInitWindowPosition(INITIAL_WINDOW_POSITION_X, INITIAL_WINDOW_POSITION_Y);
  glutCreateWindow(INITIAL_WINDOW_TITLE);

  /* screen = screen_new(3, -1); */
  screen = screen_new(3, 256);
  screen_initialize_font(screen);
  screen_apply_projection(screen);
  screen_set_window_size(screen, INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);

  /* DataObj *obj; */
  /* obj = dataobj_new(3, DATA_DOUBLE); */
  /* dataobj_set_display_type(obj, DISPLAY_TYPE_LINE_STRIP); */

  /* sp_angle = 0.0; */
  /* spiral[0] = 0.0; */
  /* for (i = 0; i < 10; i++) { */
  /*   sp_angle += SPIRAL_ROTATION; */
  /*   spiral[0] += SPIRAL_ROTATION; */
  /*   spiral[1] = cos(sp_angle); */
  /*   spiral[2] = sin(sp_angle); */
  /*   dataobj_copy_insert(obj, -1, spiral); */
  /* } */
  /* screen_add_object(screen, obj); */

  for (i = 0; i < obj_num; i++) {
    screen_add_object(screen, obj_list[i]);    
  }

  glutReshapeFunc(reshape_func);
  glutMouseFunc(mouse_func);
  glutKeyboardFunc(keyboard_func);
  glutDisplayFunc(display_func);

  glEnableClientState(GL_VERTEX_ARRAY);

  glShadeModel(GL_SMOOTH);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  /* glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE); */

  /* glEnable(GL_POLYGON_OFFSET_FILL); */
  /* /\* glPolygonOffset(1.1, 4.0); *\/ */
  /* glPolygonOffset(1.0, 1.0); */

  screen_set_center(screen, init_center);
  screen_set_size(screen, init_size[0], init_size[1]);
  screen_set_depth(screen, init_size[2], init_size[3]);
  screen_set_light_value(screen, GL_POSITION, init_light_position);
  screen_apply_light_settings(screen);

  if (init_plane->count > 0) {
    cli_screen_initial_rotation(screen, init_plane->sval[0]);
  }

  screen_apply_projection(screen);

  double plane_center[3] = { 0.0, 0.0, -2.0 };

  screen_set_basal_plane_center(screen, plane_center);
  screen_set_basal_plane_type(screen, PLANE_GRID_XY);

  /* glutTimerFunc(100, animate_func, 0); */
  glutMainLoop();

  return 0;
}
