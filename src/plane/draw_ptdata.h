#ifndef _DRAW_PTDATA_H_
#define _DRAW_PTDATA_H_

#include "ptdata.h"
#include <GL/freeglut.h>
#include <GL/glc.h>

typedef enum __plot_style { STY_DOTS, STY_LINES, STY_POINTS, STY_LINES_POINTS, STY_QUADS, STY_QUADS_EDGE } PlotStyle;

void draw_ptdata_lines(PtData *ptdata, int start, int end);
void draw_ptdata_dots(PtData *ptdata, int start, int end);
void draw_ptdata_quads(PtData *ptdata, int start, int end);
void draw_ptdata_quads_edge(PtData *ptdata, int start, int end);
void draw_ptdata_point_cross(PtData *ptdata, int start, int end, double size);

void draw_ptdata_to_gnuplot(PtData *ptdata, int start, int end, FILE *pipe_gp);
void draw_ptdata_to_gnuplot_as_quads(PtData *ptdata, int start, int end, FILE *pipe_gp);
void draw_ptdata_puts_gnuplot_style(FILE *pipe_gp, PlotStyle style);

#endif /* _DRAW_PTDATA_H_ */
