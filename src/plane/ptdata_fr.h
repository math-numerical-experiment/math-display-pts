#ifndef _PTDATA_FR_H_
#define _PTDATA_FR_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <mpfr.h>

#define MAX_MPFR_ARY_SIZE 1024
#define MPFR_PTDATA_PTR_SIZE 1024

typedef struct __MPFRPtData{
  __mpfr_struct **ptr;
  int size;
  int ptr_allocated;
  int ptr_max;
  int prec;
} MPFRPtData;

void mpfr_ptdata_init(MPFRPtData *ptdata);
void mpfr_ptdata_free(MPFRPtData *ptdata);
void mpfr_ptdata_allocate_farther(MPFRPtData *ptdata);
__mpfr_struct *mpfr_ptdata_get_mpfr_ptr(MPFRPtData *ptdata, int num);
bool mpfr_ptdata_allocated_p(MPFRPtData *ptdata, int num);
int mpfr_ptdata_set_data(MPFRPtData *ptdata, char *file);

#endif /* _PTDATA_FR_H_ */

