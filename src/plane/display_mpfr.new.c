#include "display_double.h"

#define DIMENSION 2

#define INITIAL_WINDOW_TITLE "Display points of multiple precision"
#define INITIAL_WINDOW_POSITION_X 0
#define INITIAL_WINDOW_POSITION_Y 0
#define INITIAL_WINDOW_WIDTH 800
#define INITIAL_WINDOW_HEIGHT 800

#define SCREEN_INITIAL_CRD { 0.0, 0.0, 0.0 }
#define SCREEN_INITIAL_ZOOM 1.0
#define SCREEN_INITIAL_SIZE { 8.0, 8.0, 10.0, 100.0 }
#define SCREEN_INITIAL_PREC 256
#define SCREEN_INITIAL_LIGHT_POSITION { 1.0, 0.0, 1.0, 0.0 }

#define SCREEN_DEFAULT_MOVE 1.0
#define SCREEN_DEFAULT_ZOOM 2.0
#define SCREEN_GRID_SIZE SCREEN_INITIAL_CRD_SIZE_X / 3.0
#define SCREEN_POINT_CROSS_SIZE 0.03

#define SCREEN_MOVEMENT 1.0
#define SCREEN_ROTATION 360.0 / 12.0
#define SCREEN_MARK_SIZE 0.05

#define NUMBER_DISPLAY_TYPE 5

#define DEFAULT_CENTER_X 0.0
#define DEFAULT_CENTER_Y 0.0
#define DEFAULT_CENTER_Z 10.0

Screen *screen;
CreateImage cimage[1];

static void display_func(void){
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glEnable(GL_LIGHT0);
  glEnable(GL_LIGHTING);

  glMatrixMode(GL_MODELVIEW);


  glEnable(GL_DEPTH_TEST);

  glDisable(GL_LIGHTING);

  glColor4d(0.3, 0.3, 1.0, 1.0);
  screen_show_axis(screen);

  glColor4d(0.6, 0.0, 0.6, 0.5);
  screen_show_plane(screen);


  glDisable(GL_DEPTH_TEST);

  glutSwapBuffers();
  /* glFlush(); */
}

static void reshape_func(int w, int h){
  screen_change_window_size(screen, w, h);
  screen_apply_projection(screen);
  glViewport(0, 0, w, h);
}

static void mouse_func(int button, int state, int x, int y){
  /* if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){ */
  /*   screen2d_move_to_pixel(screen, x, y); */
  /*   glutPostRedisplay(); */
  /* } */
}

static void keyboard_func(unsigned char key, int x, int y){
  switch(key){
  case 'q':
    glutLeaveMainLoop();
    break;
  case 'C':
    screen_reset_rotation(screen);
    break;
  case 'r':
    screen_rotate_around_z(screen, SCREEN_ROTATION);
    break;
  case 'R':
    screen_rotate_around_z(screen, -SCREEN_ROTATION);
    break;
  case 'i':
    screen_zoom(screen, 2.0);
    break;
  case 'u':
    screen_zoom(screen, 1.0 / 2.0);
    break;
  case 'h':
    screen_left(screen, SCREEN_MOVEMENT);
    break;
  case 'j':
    screen_down(screen, SCREEN_MOVEMENT);
    break;
  case 'k':
    screen_up(screen, SCREEN_MOVEMENT);
    break;
  case 'l':
    screen_right(screen, SCREEN_MOVEMENT);
    break;
  case 'f':
    screen_forward(screen, SCREEN_MOVEMENT);
    break;
  case 'b':
    screen_backward(screen, SCREEN_MOVEMENT);
    break;
  case 'o':
    disp_cimage_screenshot(cimage, screen->win_size[0], screen->win_size[1]);
    break;
  case 'O':
    disp_cimage_output(cimage, display_func);
    break;
  case '1':
    screen_rotate_to_surface(screen, SURFACE_XY, 1);
    break;
  default:
    return;
  }
  glutPostRedisplay();
}

int main (int argc, char *argv[])
{
  const char *progname = "display-plane";

  struct arg_lit *help = arg_lit0("h", "help", "Print this help and exit");
  struct arg_int *prec = arg_intn(NULL, "prec", "<prec>", 0, 1, "set MPFR precision");
  struct arg_str *center = arg_str0("c", "center", "\"x y\"", "Set the coordinate of initial center point");

  struct arg_file *pt_files = arg_filen("p", "points", "<file>", 0, argc + 2, "Files for displaying points");
  struct arg_file *dots_files = arg_filen("d", "dots", "<file>", 0, argc + 2, "Files for displaying dots");
  struct arg_file *lines_files = arg_filen("l", "lines", "<file>", 0, argc + 2, "Files for displaying lines");
  struct arg_file *linespts_files = arg_filen("i", "linespoints", "<file>", 0, argc + 2, "Files for displaying linespoints");
  struct arg_file *fi_files = arg_filen("b", NULL, "<file>", 0, argc + 2, "files for MPFI points");
  struct arg_file *files = arg_filen(NULL, NULL, "<file>", 0, argc + 2, "Files for displaying default style");
  struct arg_str *default_style_arg = arg_strn("s", "style", "<style>", 0, 1, "Default style for displaying.");

  struct arg_end *end = arg_end(20);
  void* argtable[] = {
    pt_files, dots_files, lines_files, linespts_files, fi_files, default_style_arg,
    prec, center, files, help, end
  };

  int nerrors = arg_parse(argc, argv, argtable);
  DisplayType default_style;
  DataFileArg *arg;
  DataObj **obj_list;
  int obj_num, i, init_prec;
  GLdouble init_center[3] = SCREEN_INITIAL_CRD;
  GLdouble init_size[4] = SCREEN_INITIAL_SIZE;
  GLfloat init_light_position[4] = SCREEN_INITIAL_LIGHT_POSITION;
  int glut_argc = 0;
  double plane_center[3] = { 0.0, 0.0, 0.0 };

  if (help->count > 0) {
    printf("Usage: %s", progname);
    arg_print_syntax(stdout, argtable,"\n");
    printf("Display points in files.\n\n");
    arg_print_glossary(stdout, argtable," %-30s %s\n");
    printf("\nReport bugs to <yt@math.sci.hokudai.ac.jp>.\n");
    exit(0);
  }

  if (nerrors > 0){
    /* Display the error details contained in the arg_end struct.*/
    arg_print_errors(stdout, end, progname);
    printf("Try '%s --help' for more information.\n", progname);
    exit(1);
  }

  init_prec = (prec->count > 0 ? prec->ival[0] : SCREEN_INITIAL_PREC);
  mpfr_set_default_prec(init_prec);

  default_style = DISPLAY_TYPE_LINES;
  if (default_style_arg->count > 0) {
    switch (default_style_arg->sval[0][0]) {
    case 'p':
      default_style = DISPLAY_TYPE_POINTS;
      break;
    case 'd':
      default_style = DISPLAY_TYPE_DOTS;
      break;
    case 'l':
      if (strlen(default_style_arg->sval[0]) >= 6 && default_style_arg->sval[0][5] == 'p') {
        default_style = DISPLAY_TYPE_LINES_POINTS;
      } else {
        default_style = DISPLAY_TYPE_LINES;
      }
      break;
    default:
      fprintf(stderr, "Invalid style: %s", default_style_arg->sval[0]);
      abort();
    }
  }

  arg = data_file_arg_new();
  data_file_arg_add(arg, pt_files, DISPLAY_TYPE_POINTS, DATA_MPFR, DIMENSION);
  data_file_arg_add(arg, dots_files, DISPLAY_TYPE_DOTS, DATA_MPFR, DIMENSION);
  data_file_arg_add(arg, lines_files, DISPLAY_TYPE_LINES, DATA_MPFR, DIMENSION);
  data_file_arg_add(arg, linespts_files, DISPLAY_TYPE_LINES_POINTS, DATA_MPFR, DIMENSION);
  data_file_arg_add(arg, fi_files, DISPLAY_TYPE_POINTS, DATA_MPFI, DIMENSION);
  data_file_arg_add(arg, files, default_style, DATA_MPFR, DIMENSION);

  obj_list = data_file_arg_initialize_objects(arg, &obj_num);

  disp_cimage_init(cimage);
  disp_cimage_set_basename(cimage, progname);

  glutInit(&glut_argc, NULL);
  /* glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH); */
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
  glutInitWindowSize(INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);
  glutInitWindowPosition(INITIAL_WINDOW_POSITION_X, INITIAL_WINDOW_POSITION_Y);
  glutCreateWindow(INITIAL_WINDOW_TITLE);

  screen = screen_new(DIMENSION, (init_prec >= SCREEN_INITIAL_PREC ? init_prec : SCREEN_INITIAL_PREC));
  screen_initialize_font(screen);
  screen_apply_projection(screen);
  screen_set_window_size(screen, INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);

  for (i = 0; i < obj_num; i++) {
    screen_add_object(screen, obj_list[i]);
  }

  glutReshapeFunc(reshape_func);
  glutMouseFunc(mouse_func);
  glutKeyboardFunc(keyboard_func);
  glutDisplayFunc(display_func);

  glEnableClientState(GL_VERTEX_ARRAY);

  glShadeModel(GL_SMOOTH);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  /* glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE); */

  /* glEnable(GL_POLYGON_OFFSET_FILL); */
  /* /\* glPolygonOffset(1.1, 4.0); *\/ */
  /* glPolygonOffset(1.0, 1.0); */

  screen_set_center(screen, init_center);
  screen_set_size(screen, init_size[0], init_size[1]);
  screen_set_depth(screen, init_size[2], init_size[3]);
  screen_set_light_value(screen, GL_POSITION, init_light_position);
  screen_apply_light_settings(screen);

  screen_apply_projection(screen);

  screen_set_basal_plane_center(screen, plane_center);
  screen_set_basal_plane_type(screen, PLANE_GRID_XY);

  screen_zoom(screen, 1.0 / 8.0);
  /* screen_rotate_around_x(screen, -SCREEN_ROTATION); */
  /* screen_rotate_around_y(screen, -SCREEN_ROTATION); */
  /* screen_up(screen, SCREEN_MOVEMENT * 10); */
  /* screen_left(screen, SCREEN_MOVEMENT * 10); */

  glutMainLoop();

  return 0;
}
