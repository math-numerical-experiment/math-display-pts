#include "display_gtk.h"

static void menu_toggle_show_axis (GtkToggleButton *widget, gpointer data)
{
  if (gtk_toggle_button_get_active(widget)) {
    settings->show_axis = true;
  } else {
    settings->show_axis = false;
  }
}

GtkWidget *create_menu_window ()
{
  GtkWidget *menu_win, *vbox, *checkbox;

  menu_win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (menu_win), INITIAL_WINDOW_TITLE);
  gtk_widget_set_size_request(menu_win, 300, -1);
  g_signal_connect (G_OBJECT (menu_win), "delete_event", G_CALLBACK (gtk_main_quit), NULL);

  vbox = gtk_vbox_new (TRUE,0);
  gtk_container_add (GTK_CONTAINER(menu_win), vbox);

  checkbox = gtk_check_button_new_with_label("Show axis");

  if (settings->show_axis) {
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), TRUE);
  } else {
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), FALSE);
  }
  g_signal_connect(G_OBJECT(checkbox), "toggled", G_CALLBACK(menu_toggle_show_axis), NULL);

  gtk_box_pack_start (GTK_BOX(vbox), checkbox, TRUE, TRUE, 0);

  GtkWidget *quit_button;
  quit_button = gtk_button_new_with_label("Quit");
  gtk_box_pack_start(GTK_BOX(vbox), quit_button, TRUE, TRUE, 0);
  g_signal_connect(G_OBJECT(quit_button), "clicked", G_CALLBACK(gtk_main_quit), NULL);

  return menu_win;
}

/* static void cb_button_toggled(GtkToggleButton *widget, gpointer user_data) */
/* { */
/*   gboolean active; */
/*   gchar *str[] = { "FALSE", "TRUE" }; */

/*   active = gtk_toggle_button_get_active(widget); */
/*   g_print("Check button state : %s\n", str[active]); */
/* } */

/* GtkWidget *create_menu_window () */
/* { */
/*   GtkWidget *menu; */
/*   GtkWidget *box; */
/*   GtkWidget *button; */

/*   menu = gtk_window_new(GTK_WINDOW_TOPLEVEL); */
/*   gtk_window_set_title(GTK_WINDOW(menu), "GtkCheckButton Sample"); */
/*   gtk_widget_set_size_request(menu, 300, -1); */
/*   g_signal_connect(G_OBJECT(menu), "destroy", G_CALLBACK(gtk_main_quit), NULL); */

/*   box = gtk_vbox_new(TRUE, 0); */
/*   gtk_container_add(GTK_CONTAINER(menu), box); */

/*   button = gtk_check_button_new_with_label("Please click me."); */
/*   gtk_box_pack_start(GTK_BOX(box), button, TRUE, TRUE, 0); */
/*   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE); */
/*   g_signal_connect(G_OBJECT(button), "toggled", G_CALLBACK(cb_button_toggled), NULL); */

/*   return menu; */
/* } */
