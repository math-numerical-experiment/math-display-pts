#ifndef _DISPLAYPTS_CLI_H_
#define _DISPLAYPTS_CLI_H_

#include "header.h"
#include "displaypts.h"

#include <glib.h>
#include <glib/garray.h>
#include <argtable2.h>

typedef struct {
  GArray *file;
  GArray *type;
  GArray *dim;
  GArray *data_type;
} DataFileArg;

DataFileArg *data_file_arg_new ();
void data_file_arg_delete (DataFileArg *arg);
void data_file_arg_add(DataFileArg *arg, void *file_arg, DisplayType type, DataType data_type, int dim);
DataObj **data_file_arg_initialize_objects(DataFileArg *arg, int *size);

void cli_screen_initial_rotation (Screen *screen, const char *s);

#endif /* _DISPLAYPTS_CLI_H_ */
