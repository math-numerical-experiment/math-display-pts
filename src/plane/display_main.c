#include "display_func.h"
#include "screen_fr.h"
#include "dataobj_fr.h"

#define INITIAL_WINDOW_TITLE "Plot Points"
#define INITIAL_WINDOW_POSITION_X 0
#define INITIAL_WINDOW_POSITION_Y 0
#define INITIAL_WINDOW_WIDTH 800
#define INITIAL_WINDOW_HEIGHT 800

#define SCREEN_INITIAL_CRD_X 0.0
#define SCREEN_INITIAL_CRD_Y 0.0
#define SCREEN_INITIAL_ZOOM 1.0
#define SCREEN_INITIAL_CRD_SIZE_X 2.0
#define SCREEN_INITIAL_CRD_SIZE_y 2.0

#define SCREEN_DEFAULT_MOVE 1.0
#define SCREEN_DEFAULT_ZOOM 2.0
#define SCREEN_GRID_SIZE SCREEN_INITIAL_CRD_SIZE_X / 3.0
#define SCREEN_POINT_CROSS_SIZE 0.03

/* #define NO_ANTIALIAS */

Screen2D screen[1];
DataObject *objects;
int obj_count;
Color col[1] = { { 0, NOT_USE_ALPHA }};
char **filenames;

DisplayState disp[1] = {{ NULL, 0, false, false, 0, 3 }};

void disp_show_grid(DisplayState *disp){
  if(disp->grid){

    glColor3f(0.3, 0.3, 0.3);
    screen2d_show_grid(screen);
  }
}

void disp_show_state(DisplayState *disp){
  glColor4f(1.0, 1.0, 1.0, 1.0);

  switch(disp->state){
  case 1:
    screen2d_show_current_state(screen);
    break;
  case 2:
    screen2d_show_lines_from_top(screen, filenames, obj_count);
    break;
  }
  if(disp->scale_bar){
    screen2d_show_scale_bar(screen);
  }

}

void display_func(void){
  glMatrixMode(GL_MODELVIEW);
  
  glClear(GL_COLOR_BUFFER_BIT);

  disp_show_grid(disp);

  color_count_reset(col);

  disp_draw_objects(disp, screen, col, objects, obj_count);
  
  disp_show_state(disp);
 
  glFlush();
}

void reshape_func(int w, int h){
  screen2d_change_win_size(screen, w, h);
  glViewport(0, 0, w, h);
}

void mouse_func(int button, int state, int x, int y){
  if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
    screen2d_move_to_pixel(screen, x, y);
    glutPostRedisplay();
  }
}

void keyboard_func(unsigned char key, int x, int y){
  switch(key){
  case 'q':
    glutLeaveMainLoop();
    break;
  case 'r':
    break;
  case 'h':
    screen2d_left(screen);
    break;
  case 'j':
    screen2d_down(screen);
    break;
  case 'k':
    screen2d_up(screen);
    break;
  case 'l':
    screen2d_right(screen);
    break;
  case 'i':
    screen2d_zoom_in(screen);
    break;
  case 'o':
    screen2d_zoom_out(screen);
    break;
  case 'a':
    screen2d_zoom_to(screen, SCREEN_INITIAL_ZOOM);
    break;
  case 's':
    screen2d_move_to(screen, SCREEN_INITIAL_CRD_X, SCREEN_INITIAL_CRD_Y);
    break;
  case 'd':
    disp_toggle_scale_bar(disp);
    break;
  case 'f':
    disp_cycle_state(disp);
    break;
  case 'g':
    disp_toggle_grid(disp);
    break;
  case 'p':
    disp_gnuplot_objects(disp, screen, objects, obj_count);
    break;
  case '0':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9':
    disp_toggle_mfd(disp, key - 48);
    break;
  default:
    return;
  }
  glutPostRedisplay();
}

int main(int argc, char *argv[]){
  const char *progname = "display-double";

  struct arg_lit *help = arg_lit0("h", "help", "print this help and exit");
  struct arg_file *pt_files = arg_filen("p", NULL, "<file>", 0, argc + 2, "files for displaying points");
  struct arg_file *dots_files = arg_filen("d", NULL, "<file>", 0, argc + 2, "files for displaying dots");
  struct arg_file *lines_files = arg_filen("l", NULL, "<file>", 0, argc + 2, "files for displaying lines");
  struct arg_file *linespts_files = arg_filen("i", NULL, "<file>", 0, argc + 2, "files for displaying linespoints");
  struct arg_file *files = arg_filen(NULL, NULL, "<file>", 0, argc + 2, "files for displaying default style");
  struct arg_str *default_style_arg = arg_strn("s", "style", "<style>", 0, 1, "default style for displaying.");
  struct arg_end *end = arg_end(20);
  void* argtable[] = { pt_files, dots_files, lines_files, linespts_files, default_style_arg, help, files, end };

  int nerrors = arg_parse(argc, argv, argtable);
  PlotStyle default_style;

  if (help->count > 0) {
    printf("Usage: %s", progname);
    arg_print_syntax(stdout, argtable,"\n");
    printf("Display points in files.\n\n");
    arg_print_glossary(stdout, argtable," %-30s %s\n");
    printf("\nReport bugs to <yt@math.sci.hokudai.ac.jp>.\n");
    exit(0);
  }

  if (nerrors > 0){
    /* Display the error details contained in the arg_end struct.*/
    arg_print_errors(stdout, end, progname);
    printf("Try '%s --help' for more information.\n", progname);
    exit(1);
  }

  default_style = STY_LINES;
  if (default_style_arg->count > 0) {
    switch (default_style_arg->sval[0][0]) {
    case 'p':
      default_style = STY_POINTS;
      break;
    case 'd':
      default_style = STY_DOTS;
      break;
    default:
      if (default_style_arg->sval[0][5] == 'p') {
	default_style = STY_LINES_POINTS;
      }
      break;
    }
  }

  screen2d_set_variables(screen, SCREEN_INITIAL_CRD_X, SCREEN_INITIAL_CRD_Y, SCREEN_INITIAL_ZOOM,
			 SCREEN_INITIAL_CRD_SIZE_X, SCREEN_INITIAL_CRD_SIZE_y, SCREEN_DEFAULT_MOVE, SCREEN_DEFAULT_ZOOM, SCREEN_GRID_SIZE, SCREEN_POINT_CROSS_SIZE);
  screen2d_set_win_size(screen, INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);

  obj_count = files->count + pt_files->count + dots_files->count + lines_files->count + linespts_files->count;
  objects = (DataObject *) malloc(obj_count * sizeof(DataObject));
  screen2d_malloc_fit_lines(&filenames, obj_count);
  disp_init_mfd(disp, obj_count);
  
  int i, file_count = 0;
  for(i = 0; i < files->count; i++){
    dataobj_init_read_file(objects + file_count, *(files->filename + i));
    dataobj_set_default_style(objects + i, default_style);
    set_filename_string(filenames, file_count, *(files->basename + i));
    file_count += 1;
  }
  for(i = 0; i < pt_files->count; i++){
    dataobj_init_read_file(objects + file_count, *(pt_files->filename + i));
    dataobj_set_default_style(objects + file_count, STY_POINTS);
    set_filename_string(filenames, file_count, *(pt_files->basename + i));
    file_count += 1;
  }
  for(i = 0; i < dots_files->count; i++){
    dataobj_init_read_file(objects + file_count, *(dots_files->filename + i));
    dataobj_set_default_style(objects + file_count, STY_DOTS);
    set_filename_string(filenames, file_count, *(dots_files->basename + i));
    file_count += 1;
  }
  for(i = 0; i < lines_files->count; i++){
    dataobj_init_read_file(objects + file_count, *(lines_files->filename + i));
    dataobj_set_default_style(objects + i, STY_LINES);
    set_filename_string(filenames, file_count, *(lines_files->basename + i));
    file_count += 1;
  }
  for(i = 0; i < linespts_files->count; i++){
    dataobj_init_read_file(objects + file_count, *(linespts_files->filename + i));
    dataobj_set_default_style(objects + file_count, STY_LINES_POINTS);
    set_filename_string(filenames, file_count, *(linespts_files->basename + i));
    file_count += 1;
  }

  int glut_argc = 0;
  glutInit(&glut_argc, NULL);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(screen->win_size[0], screen->win_size[1]);
  glutInitWindowPosition(INITIAL_WINDOW_POSITION_X, INITIAL_WINDOW_POSITION_Y);
  glutCreateWindow(INITIAL_WINDOW_TITLE);

  screen2d_initialize_font(screen);
  
  glutReshapeFunc(reshape_func);
  glutMouseFunc(mouse_func);
  glutKeyboardFunc(keyboard_func);
  glutDisplayFunc(display_func);

  glEnableClientState(GL_VERTEX_ARRAY);

#ifndef NO_ANTIALIAS
  glEnable (GL_POLYGON_SMOOTH);
  glEnable (GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
#endif /* NO_ANTIALIAS */

  glutMainLoop();
  
  return 0;
}
