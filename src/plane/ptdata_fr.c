#include "ptdata_fr.h"

static void mpfr_ptdata_init_ptr(MPFRPtData *ptdata, int seg_num){
  int i;
  for(i = 0; i < MPFR_PTDATA_PTR_SIZE; i++){
    mpfr_init2(*(ptdata->ptr + seg_num) + i, ptdata->prec);
  }
}

static void mpfr_ptdata_clear_ptr(MPFRPtData *ptdata, int seg_num){
  int i;
  for(i = 0; i < MPFR_PTDATA_PTR_SIZE; i++){
    mpfr_clear(*(ptdata->ptr + seg_num) + i);
  }
}

void mpfr_ptdata_init(MPFRPtData *ptdata){
  ptdata->prec = mpfr_get_default_prec();
  ptdata->size = 0;
  ptdata->ptr_allocated = 0;
  ptdata->ptr_max = MPFR_PTDATA_PTR_SIZE;
  ptdata->ptr = (__mpfr_struct **) malloc(MPFR_PTDATA_PTR_SIZE * sizeof(__mpfr_struct *));
  if (ptdata->ptr == NULL) {
    printf("Can not allocate memory.\n");
    abort();
  }
}

void mpfr_ptdata_free(MPFRPtData *ptdata){
  int i;
  for (i = 0; i < ptdata->ptr_allocated; i++) {
    mpfr_ptdata_clear_ptr(ptdata, i);
    free(*(ptdata->ptr + i));
  }
  free(ptdata->ptr);
}

void mpfr_ptdata_allocate_ptr(MPFRPtData *ptdata){
  ptdata->ptr_max += MPFR_PTDATA_PTR_SIZE;
  ptdata->ptr = (__mpfr_struct **) realloc(ptdata->ptr, ptdata->ptr_max * sizeof(__mpfr_struct *));
  if (ptdata->ptr == NULL) {
    printf("Can not allocate memory.\n");
    abort();
  }
}

void mpfr_ptdata_allocate_farther(MPFRPtData *ptdata){
  if(ptdata->ptr_allocated < ptdata->ptr_max - 1){
    *(ptdata->ptr + ptdata->ptr_allocated) = (__mpfr_struct *) malloc(MAX_MPFR_ARY_SIZE * sizeof(__mpfr_struct));
    if (ptdata->ptr[ptdata->ptr_allocated] == NULL){
      printf("Can not allocate memory.\n");
      abort();
    }
    mpfr_ptdata_init_ptr(ptdata, ptdata->ptr_allocated);
    ptdata->ptr_allocated += 1;
    ptdata->size += MAX_MPFR_ARY_SIZE;
  }else{
    mpfr_ptdata_allocate_ptr(ptdata);
    mpfr_ptdata_allocate_farther(ptdata);
  }
}

__mpfr_struct *mpfr_ptdata_get_mpfr_ptr(MPFRPtData *ptdata, int num){
  div_t res = div(num, MAX_MPFR_ARY_SIZE);
  return *(ptdata->ptr + res.quot) + res.rem;
}

bool mpfr_ptdata_allocated_p(MPFRPtData *ptdata, int num){
  return (num < ptdata->size ? true : false);
}

