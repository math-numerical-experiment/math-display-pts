#ifndef _DISPLAY_MAIN_H_
#define _DISPLAY_MAIN_H_

#include <argtable2.h>
#include <time.h>
#include "screen.h"
#include "dataobj.h"

typedef struct __DisplayState {
  bool *mfd;
  int num_of_mfds;
  bool grid;
  bool scale_bar;
  int state;
  int num_of_state;
} DisplayState;

void set_filename_string(char **strs, int num, const char *name);
void disp_init_mfd(DisplayState *disp, int num);
void disp_toggle_mfd(DisplayState *disp, int num);
int disp_number_showing_mfds(DisplayState *disp);
void disp_cycle_state(DisplayState *disp);
void disp_toggle_grid(DisplayState *disp);
void disp_toggle_scale_bar(DisplayState *disp);
void disp_draw_objects(DisplayState *disp, Screen2D *screen, Color *col, DataObject *objects, int obj_num);
void disp_gnuplot_objects(DisplayState *disp, Screen2D *screen, DataObject *objects, int obj_num);

#endif /* _DISPLAY_MAIN_H_ */

