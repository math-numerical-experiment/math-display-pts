void vector3d_copy(double *a, double *b)
{
  a[0] = b[0];
  a[1] = b[1];
  a[2] = b[2];
}

double vector3d_distance(double *a, double *b)
{
  double s[3];
  s[0] = a[0] - b[0];
  s[1] = a[1] - b[1];
  s[2] = a[2] - b[2];
  return sqrt(s[0] * s[0] + s[1] * s[1] + s[2] * s[2]);
}
