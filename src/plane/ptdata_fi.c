#include "ptdata_fi.h"

static void mpfi_ptdata_init_ptr(MPFIPtData *ptdata, int seg_num){
  int i;
  for(i = 0; i < MPFI_PTDATA_PTR_SIZE; i++){
    mpfi_init2(*(ptdata->ptr + seg_num) + i, ptdata->prec);
  }
}

static void mpfi_ptdata_clear_ptr(MPFIPtData *ptdata, int seg_num){
  int i;
  for(i = 0; i < MPFI_PTDATA_PTR_SIZE; i++){
    mpfi_clear(*(ptdata->ptr + seg_num) + i);
  }
}

void mpfi_ptdata_init(MPFIPtData *ptdata){
  ptdata->prec = mpfr_get_default_prec();
  ptdata->size = 0;
  ptdata->ptr_allocated = 0;
  ptdata->ptr_max = MPFI_PTDATA_PTR_SIZE;
  ptdata->ptr = (__mpfi_struct **) malloc(MPFI_PTDATA_PTR_SIZE * sizeof(__mpfi_struct *));
  if (ptdata->ptr == NULL) {
    printf("Can not allocate memory.\n");
    abort();
  }
}

void mpfi_ptdata_free(MPFIPtData *ptdata){
  int i;
  for (i = 0; i < ptdata->ptr_allocated; i++) {
    mpfi_ptdata_clear_ptr(ptdata, i);
    free(*(ptdata->ptr + i));
  }
  free(ptdata->ptr);
}

void mpfi_ptdata_allocate_ptr(MPFIPtData *ptdata){
  ptdata->ptr_max += MPFI_PTDATA_PTR_SIZE;
  ptdata->ptr = (__mpfi_struct **) realloc(ptdata->ptr, ptdata->ptr_max * sizeof(__mpfi_struct *));
  if (ptdata->ptr == NULL) {
    printf("Can not allocate memory.\n");
    abort();
  }
}

void mpfi_ptdata_allocate_farther(MPFIPtData *ptdata){
  if(ptdata->ptr_allocated < ptdata->ptr_max - 1){
    *(ptdata->ptr + ptdata->ptr_allocated) = (__mpfi_struct *) malloc(MAX_MPFI_ARY_SIZE * sizeof(__mpfi_struct));
    if (ptdata->ptr[ptdata->ptr_allocated] == NULL){
      printf("Can not allocate memory.\n");
      abort();
    }
    mpfi_ptdata_init_ptr(ptdata, ptdata->ptr_allocated);
    ptdata->ptr_allocated += 1;
    ptdata->size += MAX_MPFI_ARY_SIZE;
  }else{
    mpfi_ptdata_allocate_ptr(ptdata);
    mpfi_ptdata_allocate_farther(ptdata);
  }
}

__mpfi_struct *mpfi_ptdata_get_mpfi_ptr(MPFIPtData *ptdata, int num){
  div_t res = div(num, MAX_MPFI_ARY_SIZE);
  return *(ptdata->ptr + res.quot) + res.rem;
}

bool mpfi_ptdata_allocated_p(MPFIPtData *ptdata, int num){
  return (num < ptdata->size ? true : false);
}

