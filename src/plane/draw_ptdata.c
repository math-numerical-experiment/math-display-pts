#include "draw_ptdata.h"

void draw_ptdata_lines(PtData *ptdata, int start, int end){
  div_t div_s = div(start, PTDATA_MAX_DOUBLE_ARY_SIZE), div_e = div(end, PTDATA_MAX_DOUBLE_ARY_SIZE);
  if(div_s.quot == div_e.quot){
    glVertexPointer(2, GL_DOUBLE, 0, *(ptdata->ptr + div_s.quot));
    glDrawArrays(GL_LINE_STRIP, div_s.rem / 2, (div_e.rem - div_s.rem) / 2);
  }else{
    int i, connect;

    glVertexPointer(2, GL_DOUBLE, 0, *(ptdata->ptr + div_s.quot));
    glDrawArrays(GL_LINE_STRIP, div_s.rem / 2, (PTDATA_MAX_DOUBLE_ARY_SIZE - div_s.rem) / 2);

    for (i = div_s.quot + 1; i < div_e.quot; i++) {
      glVertexPointer(2, GL_DOUBLE, 0, *(ptdata->ptr + i));
      glDrawArrays(GL_LINE_STRIP, 0, PTDATA_MAX_DOUBLE_ARY_SIZE / 2);

      connect = PTDATA_MAX_DOUBLE_ARY_SIZE * i;
      glBegin(GL_LINES);
      glVertex3d(*ptdata_get_dbl_ptr(ptdata, connect - 2), *ptdata_get_dbl_ptr(ptdata, connect - 1), 0.0);
      glVertex3d(*ptdata_get_dbl_ptr(ptdata, connect), *ptdata_get_dbl_ptr(ptdata, connect + 1), 0.0);
      glEnd();
    }

    glVertexPointer(2, GL_DOUBLE, 0, *(ptdata->ptr + div_e.quot));
    glDrawArrays(GL_LINE_STRIP, 0, div_e.rem / 2);
  }
}

void draw_ptdata_dots(PtData *ptdata, int start, int end){
  div_t div_s = div(start, PTDATA_MAX_DOUBLE_ARY_SIZE), div_e = div(end, PTDATA_MAX_DOUBLE_ARY_SIZE);
  if(div_s.quot == div_e.quot){
    glVertexPointer(2, GL_DOUBLE, 0, *(ptdata->ptr + div_s.quot));
    glDrawArrays(GL_POINTS, div_s.rem / 2, (div_e.rem - div_s.rem) / 2);
  }else{
    int i;

    glVertexPointer(2, GL_DOUBLE, 0, *(ptdata->ptr + div_s.quot));
    glDrawArrays(GL_POINTS, div_s.rem / 2, (PTDATA_MAX_DOUBLE_ARY_SIZE - div_s.rem) / 2);

    for (i = div_s.quot + 1; i < div_e.quot; i++) {
      glVertexPointer(2, GL_DOUBLE, 0, *(ptdata->ptr + i));
      glDrawArrays(GL_POINTS, 0, PTDATA_MAX_DOUBLE_ARY_SIZE / 2);
    }

    glVertexPointer(2, GL_DOUBLE, 0, *(ptdata->ptr + div_e.quot));
    glDrawArrays(GL_POINTS, 0, div_e.rem / 2);
  }
}

void draw_ptdata_quads(PtData *ptdata, int start, int end){
  div_t div_s = div(start, PTDATA_MAX_DOUBLE_ARY_SIZE), div_e = div(end, PTDATA_MAX_DOUBLE_ARY_SIZE);
  if(div_s.quot == div_e.quot){
    glVertexPointer(2, GL_DOUBLE, 0, *(ptdata->ptr + div_s.quot));
    glDrawArrays(GL_QUADS, div_s.rem / 2, (div_e.rem - div_s.rem) / 2);
  }else{
    int i;

    glVertexPointer(2, GL_DOUBLE, 0, *(ptdata->ptr + div_s.quot));
    glDrawArrays(GL_QUADS, div_s.rem / 2, (PTDATA_MAX_DOUBLE_ARY_SIZE - div_s.rem) / 2);

    for (i = div_s.quot + 1; i < div_e.quot; i++) {
      glVertexPointer(2, GL_DOUBLE, 0, *(ptdata->ptr + i));
      glDrawArrays(GL_QUADS, 0, PTDATA_MAX_DOUBLE_ARY_SIZE / 2);
    }

    glVertexPointer(2, GL_DOUBLE, 0, *(ptdata->ptr + div_e.quot));
    glDrawArrays(GL_QUADS, 0, div_e.rem / 2);
  }
}

static void draw_ptdata_quads_edge_segment(double *dbl, int start, int end){
  int i;
  for(i = start; i < end; i += 8){
    glVertexPointer(2, GL_DOUBLE, 0, dbl + i);
    glDrawArrays(GL_LINE_LOOP, 0, 4);
  }
}

void draw_ptdata_quads_edge(PtData *ptdata, int start, int end){
  div_t div_s = div(start, PTDATA_MAX_DOUBLE_ARY_SIZE), div_e = div(end, PTDATA_MAX_DOUBLE_ARY_SIZE);
  if(div_s.quot == div_e.quot){
    draw_ptdata_quads_edge_segment(*(ptdata->ptr + div_s.quot), div_s.rem, div_e.rem);
  }else{
    int i;

    draw_ptdata_quads_edge_segment(*(ptdata->ptr + div_s.quot), div_s.rem, PTDATA_MAX_DOUBLE_ARY_SIZE);
    
    for (i = div_s.quot + 1; i < div_e.quot; i++) {
      draw_ptdata_quads_edge_segment(*(ptdata->ptr + i), 0, PTDATA_MAX_DOUBLE_ARY_SIZE);
    }

    draw_ptdata_quads_edge_segment(*(ptdata->ptr + div_e.quot), 0, div_e.rem);
  }
}

void draw_ptdata_point_cross(PtData *ptdata, int start, int end, double size){
  double x, y;
  int i;
  glBegin(GL_LINES);
  for(i = start; i < end; i += 2){
    x = *ptdata_get_dbl_ptr(ptdata, i);
    y = *ptdata_get_dbl_ptr(ptdata, i + 1);
    glVertex3f(x - size, y - size, 0.0);
    glVertex3f(x + size, y + size, 0.0);
    glVertex3f(x + size, y - size, 0.0);
    glVertex3f(x - size, y + size, 0.0);
  }
  glEnd();
}

void draw_ptdata_to_gnuplot(PtData *ptdata, int start, int end, FILE *pipe_gp){
  int i;
  for(i = start; i < end; i += 2){
    fprintf(pipe_gp, "%.18e %.18e\n", *ptdata_get_dbl_ptr(ptdata, i), *ptdata_get_dbl_ptr(ptdata, i + 1));
  }
}

void draw_ptdata_to_gnuplot_as_quads(PtData *ptdata, int start, int end, FILE *pipe_gp){
  int i, j;
  for(i = start; i < end; i += 8){
    for(j = 0; j < 8; j += 2){
      fprintf(pipe_gp, "%.18e %.18e\n", *ptdata_get_dbl_ptr(ptdata, i + j), *ptdata_get_dbl_ptr(ptdata, i + j + 1));
    }
    fprintf(pipe_gp, "%.18e %.18e\n\n", *ptdata_get_dbl_ptr(ptdata, i), *ptdata_get_dbl_ptr(ptdata, i + 1));
  }
}

void draw_ptdata_puts_gnuplot_style(FILE *pipe_gp, PlotStyle style){
  switch(style){
  case STY_DOTS:
    fprintf(pipe_gp, "'-' with dots notitle");
    break;
  case STY_QUADS:
    fprintf(pipe_gp, "'-' with filledcurves closed notitle");
    break;
  case STY_LINES:
  case STY_QUADS_EDGE:
    fprintf(pipe_gp, "'-' with lines notitle");
    break;
  case STY_LINES_POINTS:
    fprintf(pipe_gp, "'-' with linespoints notitle");
    break;
  case STY_POINTS:
    fprintf(pipe_gp, "'-' with points notitle");
    break;
  }
}
