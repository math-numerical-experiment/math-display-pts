#include <cutter.h>
#include "../src/core/displaypts.h"

#define DIMENSION1 3

void test_mpfr_dataobj ()
{
  DataObj *obj;
  mpfr_ptr pt1, pt2;
  int i, prec;
  prec = 128;
  obj = dataobj_new(DIMENSION1, DATA_MPFR);
  dataobj_set_prec(obj, prec);
  pt1 = dataobj_allocate_mpfr_pt(DIMENSION1, prec);

  mpfr_set_str(dataobj_mpfr_pt_element(pt1, 0), "-0.3", 10, MPFR_RNDN);
  mpfr_set_str(dataobj_mpfr_pt_element(pt1, 1), "1.5", 10, MPFR_RNDN);
  mpfr_set_str(dataobj_mpfr_pt_element(pt1, 2), "0.7", 10, MPFR_RNDN);

  dataobj_copy_insert(obj, -1, pt1);

  pt2 = dataobj_get_mpfr_ptr(obj, 0);

  for (i = 0; i < DIMENSION1; i++) {
    /* mpfr_printf("pt1 = %Re, pt2 = %Re\n", dataobj_mpfr_pt_element(pt1, i), dataobj_mpfr_pt_element(pt2, i)); */
    cut_assert_equal_int(mpfr_cmp(dataobj_mpfr_pt_element(pt1, i), dataobj_mpfr_pt_element(pt2, i)), 0);
  }

  dataobj_clear_mpfr_pt(pt1, DIMENSION1);
  dataobj_delete(obj);
}

void test_dataobj_read_file ()
{
  int i, j, k;
  double *pt;
  char *filepath;
  DataObj *obj;
  obj = dataobj_new(DIMENSION1, DATA_DOUBLE);
  if (tcstatfile("data_read_file_double.dat", NULL, NULL, NULL)) {
    filepath = "data_read_file_double.dat";
  } else if (tcstatfile("test/data_read_file_double.dat", NULL, NULL, NULL)) {
    filepath = "test/data_read_file_double.dat";
  }

  dataobj_init_read_file(obj, filepath);

  k = 1;
  cut_assert_equal_int(dataobj_size(obj), 2);
  for (j = 0; j < dataobj_size(obj); j++) {
    pt = dataobj_get_double_ptr(obj, j);
    for (i = 0; i < DIMENSION1; i++) {
      cut_assert_equal_double(pt[i], 0.01, (double) k);
      k += 1;
    }
  }

  dataobj_delete(obj);
}

void test_dataobj_read_file_gz ()
{
  int i, j, k;
  double *pt;
  char *filepath;
  DataObj *obj;
  obj = dataobj_new(DIMENSION1, DATA_DOUBLE);
  if (tcstatfile("data_read_file_double.dat.gz", NULL, NULL, NULL)) {
    filepath = "data_read_file_double.dat.gz";
  } else if (tcstatfile("test/data_read_file_double.dat.gz", NULL, NULL, NULL)) {
    filepath = "test/data_read_file_double.dat.gz";
  }

  dataobj_init_read_file(obj, filepath);

  k = 1;
  cut_assert_equal_int(dataobj_size(obj), 2);
  for (j = 0; j < dataobj_size(obj); j++) {
    pt = dataobj_get_double_ptr(obj, j);
    for (i = 0; i < DIMENSION1; i++) {
      cut_assert_equal_double(pt[i], 0.01, (double) k);
      k += 1;
    }
  }

  dataobj_delete(obj);
}
