#include "screen.h"

#define GLC_FONT_NAME "DejaVu Sans Mono"
#define DEFAULT_CROSS_LENGTH 0.05

#define degree_to_radian(deg) (deg / 180.0 * M_PI)
#define radian_to_degree(rad) (rad / M_PI * 180.0)

/* If prec is positive, use mpfr. Otherwise, use double precision. */
Screen *screen_new(int dim, int prec)
{
  Screen *screen;
  screen = malloc(sizeof(Screen));
  if (!screen) {
    fprintf(stderr, "Can not allocate memory of Screen.\n");
    abort();
  }
  screen->prec = prec;
  if (screen->prec > 0) {
    screen->base_pt = dataobj_allocate_mpfr_pt(3, screen->prec);
    if (!screen->base_pt) {
      fprintf(stderr, "Can not allocate memory of Screen.\n");
      abort();
    }
    mpfr_init2(screen->base_scale, screen->prec);
    mpfr_set_si(dataobj_mpfr_pt_element(screen->base_pt, 0), 0, GMP_RNDN);
    mpfr_set_si(dataobj_mpfr_pt_element(screen->base_pt, 1), 0, GMP_RNDN);
    mpfr_set_si(dataobj_mpfr_pt_element(screen->base_pt, 2), 0, GMP_RNDN);
    mpfr_set_si(screen->base_scale, 1, GMP_RNDN);
  }

  screen->dim = dim;
  screen->zoom = 1.0;

  screen->center[0] = 0.0;
  screen->center[1] = 0.0;
  screen->center[2] = 0.0;
  screen->size[0] = 1.0;
  screen->size[1] = 1.0;
  screen->size[2] = 1.0;
  screen->size[3] = 1.0;

  screen->quaternion[0] = 1.0;
  screen->quaternion[1] = 0.0;
  screen->quaternion[2] = 0.0;
  screen->quaternion[3] = 0.0;
  
  screen->lambient[0] = 1.0;
  screen->lambient[1] = 1.0;
  screen->lambient[2] = 1.0;
  screen->lambient[3] = 0.0;
  screen->ldiffuse[0] = 1.0;
  screen->ldiffuse[1] = 1.0;
  screen->ldiffuse[2] = 1.0;
  screen->ldiffuse[3] = 0.0;
  screen->lspecular[0] = 1.0;
  screen->lspecular[1] = 1.0;
  screen->lspecular[2] = 1.0;
  screen->lspecular[3] = 0.0;
  screen->lposition[0] = 0.0;
  screen->lposition[1] = 0.0;
  screen->lposition[2] = 1.0;
  screen->lposition[3] = 0.0;

  screen->objects = tclistnew();

  screen->basal_plane_center[0] = 0.0;
  screen->basal_plane_center[1] = 0.0;
  screen->basal_plane_center[2] = 0.0;
  screen->basal_plane_type = PLANE_NOT_SHOWN;

  screen->cross_length = DEFAULT_CROSS_LENGTH;

  return screen;
}

void screen_initialize_font (Screen *screen)
{
  glcContext(glcGenContext());
  screen->font = glcGenFontID();
  glcNewFontFromFamily(screen->font, GLC_FONT_NAME);
  glcFont(screen->font);
}

void screen_initialize_light (Screen *screen)
{
  glLightfv(GL_LIGHT0, GL_AMBIENT, screen->lambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, screen->ldiffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, screen->lspecular);
  glLightfv(GL_LIGHT0, GL_POSITION, screen->lposition);
  glEnable(GL_LIGHT0);
}

void screen_delete(Screen *screen)
{
  tclistdel(screen->objects);
  free(screen);
}

void screen_set_window_size(Screen *screen, int win_x, int win_y)
{
  screen->win_size[0] = win_x;
  screen->win_size[1] = win_y;
}

/*
  viewpoint is on z-axis.
  We look at origin and the top of screen is for the y-direction.
*/
void screen_apply_projection(Screen *screen)
{
  GLdouble m[4][4];

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-screen->size[0] / 2.0, screen->size[0] / 2.0,
          -screen->size[1] / 2.0, screen->size[1] / 2.0,
          -screen->size[2], screen->size[3]);
  glMatrixMode(GL_MODELVIEW);
  quat_inverse_rotation_matrix(m, screen->quaternion);
  glTranslated(screen->center[0], screen->center[1], screen->center[2]);
  glMultMatrixd(&m[0][0]);
  gluLookAt(0.0, 0.0, screen->size[2], 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
  quat_rotation_matrix(m, screen->quaternion);
  glMultMatrixd(&m[0][0]);
  glTranslated(-screen->center[0], -screen->center[1], -screen->center[2]);
}

void screen_set_center(Screen *screen, GLdouble *center)
{
  int i;
  for (i = 0; i < screen->dim; i++) {
    screen->center[i] = center[i];
  }
}

void screen_set_basal_plane_center(Screen *screen, GLdouble *center)
{
  int i;
  for (i = 0; i < screen->dim; i++) {
    screen->basal_plane_center[i] = center[i];
  }
}

void screen_set_basal_plane_type(Screen *screen, PlaneType type)
{
  screen->basal_plane_type = type;
}

void screen_set_size(Screen *screen, GLdouble xsize, GLdouble ysize)
{
  screen->size[0] = xsize;
  screen->size[1] = ysize;
}

void screen_set_depth(Screen *screen, GLdouble z1, GLdouble z2)
{
  screen->size[2] = z1;
  screen->size[3] = z2;
}

void screen_change_window_size(Screen *screen, int new_win_x, int new_win_y)
{
  screen->size[0] = (GLdouble) new_win_x / (GLdouble) screen->win_size[0] * screen->size[0];
  screen->size[1] = (GLdouble) new_win_y / (GLdouble) screen->win_size[1] * screen->size[1];
  screen_set_window_size(screen, new_win_x, new_win_y);
}

void screen_set_light_value(Screen *screen, GLenum type, GLfloat *value)
{
  switch(type) {
  case GL_AMBIENT:
    screen->lambient[0] = value[0];
    screen->lambient[1] = value[1];
    screen->lambient[2] = value[2];
    screen->lambient[3] = value[3];
    break;
  case GL_DIFFUSE:
    screen->ldiffuse[0] = value[0];
    screen->ldiffuse[1] = value[1];
    screen->ldiffuse[2] = value[2];
    screen->ldiffuse[3] = value[3];
    break;
  case GL_SPECULAR:
    screen->lspecular[0] = value[0];
    screen->lspecular[1] = value[1];
    screen->lspecular[2] = value[2];
    screen->lspecular[3] = value[3];
    break;
  case GL_POSITION:
    screen->lposition[0] = value[0];
    screen->lposition[1] = value[1];
    screen->lposition[2] = value[2];
    screen->lposition[3] = value[3];
    break;
  }
}

void screen_apply_light_settings(Screen *screen)
{
  glLightfv(GL_LIGHT0, GL_AMBIENT, screen->lambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, screen->ldiffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, screen->lspecular);
  glLightfv(GL_LIGHT0, GL_POSITION, screen->lposition);
}

void screen_move_coordinate(Screen *screen, GLdouble x, GLdouble y, GLdouble z)
{
  glTranslated(-x, -y, -z);
  screen->center[0] += x;
  screen->center[1] += y;
  screen->center[2] += z;
}

void screen_move(Screen *screen, ScreenDir dir, double move)
{
  double q1[4], q2[4], tmp[4];
  if (screen->dim == 2 && (move == MOVE_FORWARD || move == MOVE_BACKWARD)) {
    return;
  }
  q1[0] = 0.0;
  switch(dir) {
  case MOVE_LEFT:
    q1[1] = -1.0;
    q1[2] = 0.0;
    q1[3] = 0.0;
    break;
  case MOVE_RIGHT:
    q1[1] = 1.0;
    q1[2] = 0.0;
    q1[3] = 0.0;
    break;
  case MOVE_UP:
    q1[1] = 0.0;
    q1[2] = 1.0;
    q1[3] = 0.0;
    break;
  case MOVE_DOWN:
    q1[1] = 0.0;
    q1[2] = -1.0;
    q1[3] = 0.0;
    break;
  case MOVE_FORWARD:
    q1[1] = 0.0;
    q1[2] = 0.0;
    q1[3] = -1.0;
    break;
  case MOVE_BACKWARD:
    q1[1] = 0.0;
    q1[2] = 0.0;
    q1[3] = 1.0;
    break;
  }
  quat_multiple(tmp, q1, screen->quaternion);
  quat_conjugate(q1, screen->quaternion);
  quat_multiple(q2, q1, tmp);
  screen_move_coordinate(screen, q2[1], q2[2], q2[3]);
}

void screen_zoom(Screen *screen, GLdouble zoom)
{
  glTranslated(screen->center[0], screen->center[1], screen->center[2]);
  if (screen->dim == 2) {
    glScalef(zoom, zoom, 1.0);
  } else {
    glScalef(zoom, zoom, zoom);
  }
  glTranslated(-screen->center[0], -screen->center[1], -screen->center[2]);
  screen->zoom *= zoom;
}

void screen_rotate(Screen *screen, GLdouble angle, GLdouble *direction)
{
  GLdouble q[4], tmp[4];
  if (screen->dim == 2 && (direction[0] > 0.0 || direction[1] > 0.0)) {
    fprintf(stderr, "Invalid direction vector for rotation.\n");
    abort();
  }
  glTranslated(screen->center[0], screen->center[1], screen->center[2]);
  glRotated(angle, direction[0], direction[1], direction[2]);
  glTranslated(-screen->center[0], -screen->center[1], -screen->center[2]);

  quat_rotation_to_quaternion(q, degree_to_radian(angle), direction);
  quat_multiple(tmp, screen->quaternion, q);
  quat_copy(screen->quaternion, tmp);
}

void screen_reset_rotation(Screen *screen)
{
  GLdouble m[4][4];
  quat_inverse_rotation_matrix(m, screen->quaternion);
  glTranslated(screen->center[0], screen->center[1], screen->center[2]);
  glMultMatrixd(&m[0][0]);
  glTranslated(-screen->center[0], -screen->center[1], -screen->center[2]);

  screen->quaternion[0] = 1.0;
  screen->quaternion[1] = 0.0;
  screen->quaternion[2] = 0.0;
  screen->quaternion[3] = 0.0;
}

void screen_rotate_around_x(Screen *screen, GLdouble angle)
{
  GLdouble direction[3] = { 1.0, 0.0, 0.0 };
  screen_rotate(screen, angle, direction);
}

void screen_rotate_around_y(Screen *screen, GLdouble angle)
{
  GLdouble direction[3] = { 0.0, 1.0, 0.0 };
  screen_rotate(screen, angle, direction);
}

void screen_rotate_around_z(Screen *screen, GLdouble angle)
{
  GLdouble direction[3] = { 0.0, 0.0, 1.0 };
  screen_rotate(screen, angle, direction);
}

void screen_rotate_to_surface(Screen *screen, Surface surface, int quadrant)
{
  double angle;
  switch (quadrant) {
  case 2:
    angle = 90.0;
    break;
  case 3:
    angle = 180.0;
    break;
  case 4:
    angle = 270.0;
    break;
  default:
    angle = -1.0;
    break;
  }
  screen_reset_rotation(screen);
  switch (surface) {
  case SURFACE_XY:
    if (angle > 0.0) {
      screen_rotate_around_z(screen, -angle);
    }
    break;
  case SURFACE_XZ:
    screen_rotate_around_x(screen, -90.0);
    if (angle > 0.0) {
      screen_rotate_around_y(screen, angle);
    }
    break;
  case SURFACE_YX:
    screen_rotate_around_z(screen, -90.0);
    screen_rotate_around_y(screen, 180.0);
    if (angle > 0.0) {
      screen_rotate_around_z(screen, angle);
    }
    break;
  case SURFACE_YZ:
    screen_rotate_around_z(screen, -90.0);
    screen_rotate_around_y(screen, -90.0);
    if (angle > 0.0) {
      screen_rotate_around_x(screen, -angle);
    }
    break;
  case SURFACE_ZX:
    screen_rotate_around_z(screen, 90.0);
    screen_rotate_around_x(screen, 90.0);
    if (angle > 0.0) {
      screen_rotate_around_y(screen, -angle);
    }
    break;
  case SURFACE_ZY:
    screen_rotate_around_y(screen, 90.0);
    if (angle > 0.0) {
      screen_rotate_around_x(screen, angle);
    }
    break;
  default:
    break;
  }
}

static void screen_puts_axis_name(GLdouble screen_zoom, GLdouble m[][4], GLdouble x, GLdouble y, GLdouble z, char *letter, GLdouble text_size)
{
  glPushMatrix();
  glTranslated(x, y, z);
  glMultMatrixd(&m[0][0]);
  glScaled(text_size / screen_zoom, text_size / screen_zoom, text_size / screen_zoom);
  glcRenderString(letter);
  glPopMatrix();
}

/* axis == 0 -> x, axis == 1 -> y, axis ==2 -> z */
static void screen_puts_axis_arrow(GLdouble x, GLdouble y, GLdouble z, GLdouble distance, GLdouble len, int axis)
{
  glPushMatrix();
  glTranslated(x, y, z);
  glBegin(GL_TRIANGLES);
  switch (axis) {
  case 0:
    glVertex3d(distance, 0.0, 0.0);
    glVertex3d(distance - len, len / 2.0, 0.0);
    glVertex3d(distance - len, -len / 2.0, 0.0);
    break;
  case 1:
    glVertex3d(0.0, distance, 0.0);
    glVertex3d(len / 2.0, distance - len, 0.0);
    glVertex3d(-len / 2.0, distance - len, 0.0);
    break;
  case 2:
    glVertex3d(0.0, 0.0, distance);
    glVertex3d(0.0, len / 2.0, distance - len);
    glVertex3d(0.0, -len / 2.0, distance - len);
    break;
  }
  glEnd();
  glPopMatrix();
}


void screen_show_axis(Screen *screen)
{
  double len;
  len = screen->size[0];
  if (screen->size[1] > len) {
    len = screen->size[1];
  }
  len /= screen->zoom;
  glBegin(GL_LINES);
  glVertex3d(screen->center[0] - len, screen->center[1], screen->center[2]);
  glVertex3d(screen->center[0] + len, screen->center[1], screen->center[2]);
  glVertex3d(screen->center[0], screen->center[1] - len, screen->center[2]);
  glVertex3d(screen->center[0], screen->center[1] + len, screen->center[2]);
  glVertex3d(screen->center[0], screen->center[1], screen->center[2] - len);
  glVertex3d(screen->center[0], screen->center[1], screen->center[2] + len);
  glEnd();

  len = screen->size[0];
  if (screen->size[1] < len) {
    len = screen->size[1];
  }
  len = len / screen->zoom / 3.0;

  GLdouble m[4][4], text_size;
  quat_inverse_rotation_matrix(m, screen->quaternion);

  text_size = 0.5;
  glcRenderStyle(GLC_TRIANGLE);

  screen_puts_axis_name(screen->zoom, m, screen->center[0] + len, screen->center[1], screen->center[2], "x", text_size);
  screen_puts_axis_name(screen->zoom, m, screen->center[0], screen->center[1] + len, screen->center[2], "y", text_size);
  if (screen->dim == 3) {
    screen_puts_axis_name(screen->zoom, m, screen->center[0], screen->center[1], screen->center[2] + len, "z", text_size);    
  }

  len = len / 3.0 * 2.0;
  double mark;
  mark = len / 10.0;

  screen_puts_axis_arrow(screen->center[0], screen->center[1], screen->center[2], len, mark, 0);
  screen_puts_axis_arrow(screen->center[0], screen->center[1], screen->center[2], len, mark, 1);
  if (screen->dim == 3) {
    screen_puts_axis_arrow(screen->center[0], screen->center[1], screen->center[2], len, mark, 2);    
  }

}

static void screen_show_plane_grid (PlaneType plane_type, double *plane_center, double len, int grid_num)
{
  int i;
  double grid, crd;
  grid = (2.0 * len) / (double) grid_num;
  glBegin(GL_LINES);
  switch (plane_type) {
  case PLANE_GRID_XY:
    crd = plane_center[1] - len;
    glVertex3d(plane_center[0] - len, plane_center[1] - len, plane_center[2]);
    glVertex3d(plane_center[0] + len, plane_center[1] - len, plane_center[2]);
    glVertex3d(plane_center[0] - len, plane_center[1] + len, plane_center[2]);
    glVertex3d(plane_center[0] + len, plane_center[1] + len, plane_center[2]);
    for (i = 1; i < grid_num; i++) {
      crd += grid;
      glVertex3d(plane_center[0] - len, crd, plane_center[2]);
      glVertex3d(plane_center[0] + len, crd, plane_center[2]);
    }

    crd = plane_center[0] - len;
    glVertex3d(plane_center[0] - len, plane_center[1] - len, plane_center[2]);
    glVertex3d(plane_center[0] - len, plane_center[1] + len, plane_center[2]);
    glVertex3d(plane_center[0] + len, plane_center[1] - len, plane_center[2]);
    glVertex3d(plane_center[0] + len, plane_center[1] + len, plane_center[2]);
    for (i = 1; i < grid_num; i++) {
      crd += grid;
      glVertex3d(crd, plane_center[1] - len, plane_center[2]);
      glVertex3d(crd, plane_center[1] + len, plane_center[2]);
    }
    break;
  case PLANE_GRID_YZ:
    crd = plane_center[1] - len;
    glVertex3d(plane_center[0], plane_center[1] - len, plane_center[2] - len);
    glVertex3d(plane_center[0], plane_center[1] + len, plane_center[2] - len);
    glVertex3d(plane_center[0], plane_center[1] - len, plane_center[2] + len);
    glVertex3d(plane_center[0], plane_center[1] + len, plane_center[2] + len);
    for (i = 1; i < grid_num; i++) {
      crd += grid;
      glVertex3d(plane_center[0], plane_center[1] - len, crd);
      glVertex3d(plane_center[0], plane_center[1] + len, crd);
    }

    crd = plane_center[2] - len;
    glVertex3d(plane_center[0] - len, plane_center[1], plane_center[2] - len);
    glVertex3d(plane_center[0] - len, plane_center[1], plane_center[2] + len);
    glVertex3d(plane_center[0] + len, plane_center[1], plane_center[2] - len);
    glVertex3d(plane_center[0] + len, plane_center[1], plane_center[2] + len);
    for (i = 1; i < grid_num; i++) {
      crd += grid;
      glVertex3d(plane_center[0], plane_center[2] - len, crd);
      glVertex3d(plane_center[0], plane_center[2] + len, crd);
    }
    break;
  case PLANE_GRID_XZ:
    crd = plane_center[2] - len;
    glVertex3d(plane_center[0] - len, plane_center[1], plane_center[2] - len);
    glVertex3d(plane_center[0] + len, plane_center[1], plane_center[2] - len);
    glVertex3d(plane_center[0] - len, plane_center[1], plane_center[2] + len);
    glVertex3d(plane_center[0] + len, plane_center[1], plane_center[2] + len);
    for (i = 1; i < grid_num; i++) {
      crd += grid;
      glVertex3d(plane_center[0] - len, plane_center[1], crd);
      glVertex3d(plane_center[0] + len, plane_center[1], crd);
    }

    crd = plane_center[0] - len;
    glVertex3d(plane_center[0] - len, plane_center[1] - len, plane_center[2]);
    glVertex3d(plane_center[0] - len, plane_center[1] + len, plane_center[2]);
    glVertex3d(plane_center[0] + len, plane_center[1] - len, plane_center[2]);
    glVertex3d(plane_center[0] + len, plane_center[1] + len, plane_center[2]);
    for (i = 1; i < grid_num; i++) {
      crd += grid;
      glVertex3d(crd, plane_center[1] - len, plane_center[2]);
      glVertex3d(crd, plane_center[1] + len, plane_center[2]);
    }
    break;
  default:
    fprintf(stderr, "Invalid plane type.\n");
    abort();
  }
  glEnd();
}

void screen_show_plane (Screen *screen)
{
  if (screen->dim == 3 && screen->basal_plane_type != PLANE_NOT_SHOWN) {
    double len, plane_center[3];
    len = screen->size[0];
    if (screen->size[1] > len) {
      len = screen->size[1];
    }
    len = len / (screen->zoom * 3);
    plane_center[0] = screen->center[0] + screen->basal_plane_center[0] / screen->zoom;
    plane_center[1] = screen->center[1] + screen->basal_plane_center[1] / screen->zoom;
    plane_center[2] = screen->center[2] + screen->basal_plane_center[2] / screen->zoom;
    switch (screen->basal_plane_type) {
    case PLANE_FILL_XY:
      glBegin(GL_QUADS);
      glVertex3d(plane_center[0] - len, plane_center[1] - len, plane_center[2]);
      glVertex3d(plane_center[0] + len, plane_center[1] - len, plane_center[2]);
      glVertex3d(plane_center[0] + len, plane_center[1] + len, plane_center[2]);
      glVertex3d(plane_center[0] - len, plane_center[1] + len, plane_center[2]);
      glEnd();
      break;
    case PLANE_FILL_YZ:
      glBegin(GL_QUADS);
      glVertex3d(plane_center[0], plane_center[1] - len, plane_center[2] - len);
      glVertex3d(plane_center[0], plane_center[1] + len, plane_center[2] - len);
      glVertex3d(plane_center[0], plane_center[1] + len, plane_center[2] + len);
      glVertex3d(plane_center[0], plane_center[1] - len, plane_center[2] + len);
      glEnd();
      break;
    case PLANE_FILL_XZ:
      glBegin(GL_QUADS);
      glVertex3d(plane_center[0] - len, plane_center[1], plane_center[2] - len);
      glVertex3d(plane_center[0] + len, plane_center[1], plane_center[2] - len);
      glVertex3d(plane_center[0] + len, plane_center[1], plane_center[2] + len);
      glVertex3d(plane_center[0] - len, plane_center[1], plane_center[2] + len);
      glEnd();
      break;
    default:
      screen_show_plane_grid(screen->basal_plane_type, plane_center, len, 6);
      break;
    }
  }
}

void screen_put_text(Screen *screen, GLdouble *pt, char *text, GLdouble size)
{
  GLdouble m[4][4];
  quat_inverse_rotation_matrix(m, screen->quaternion);
  glPushMatrix();
  glTranslated(pt[0], pt[1], pt[2]);
  glMultMatrixd(&m[0][0]);
  glScaled(size / screen->zoom, size / screen->zoom, size / screen->zoom);
  glcRenderStyle(GLC_TRIANGLE);
  glcRenderString(text);
  glPopMatrix();
}

/* void screen_text_lines(Screen2D *screen, char **lines, int num){ */
/*   int i; */
/*   glPushMatrix(); */

/*   glTranslated(screen->center[0], screen->center[1], screen->center[2]); */
/*   glScaled(1.0 / screen->zoom, 1.0 / screen->zoom, 1.0); */
/*   glTranslated(-screen->size[0] + 0.1, screen->size[1] - 0.15, STRING_Z_POS); */
/*   glScaled(0.1, 0.1, 1.0); */

/*   glcRenderStyle(GLC_TRIANGLE);   */
/*   for(i = 0; i < num; i++){ */
/*     glPushMatrix(); */
/*     glcRenderString(*(lines + i)); */
/*     glPopMatrix(); */
/*     glTranslated(0.0, -2.0, 0); */
/*   } */

/*   glPopMatrix(); */
/* } */


void screen_add_object(Screen *screen, DataObj *obj)
{
  tclistpush(screen->objects, &obj, sizeof(DataObj *));
}

DataObj *screen_get_object(Screen *screen, int num)
{
  return *(DataObj **)tclistval2(screen->objects, num);
}

void screen_plot_all_objects(Screen *screen)
{
  int i;
  DataObj *obj;
  for (i = 0; i < tclistnum(screen->objects); i++) {
    obj = screen_get_object(screen, i);
    dataobj_draw_coordinate(obj);
    if (obj->display_type == DISPLAY_TYPE_LINES_POINTS ||
        obj->display_type == DISPLAY_TYPE_POINTS) {
      dataobj_draw_cross(obj, screen->cross_length / screen->zoom);
    }
  }
}

