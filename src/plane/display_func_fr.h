#ifndef _DISPLAY_FUNC_FR_H_
#define _DISPLAY_FUNC_FR_H_

#include "display_func.h"
#include "screen_fr.h"
#include "dataobj_fr.h"
#include "dataobj_fi.h"

void mpfr_disp_change_base_scale(MPFRScreen2D *screen, double expansion);
void mpfr_disp_draw_objects(DisplayState *disp, int disp_start_num, MPFRScreen2D *screen, Color *col, MPFRDataObject *objects, int obj_num);
void mpfi_disp_draw_objects(DisplayState *disp, int disp_start_num, MPFRScreen2D *screen, Color *col, MPFIDataObject *objects, int obj_num);
void mpfr_disp_gnuplot_objects(DisplayState *disp, MPFRDataObject *fr_objects, int fr_obj_num,
			       MPFIDataObject *fi_objects, int fi_obj_num, MPFRScreen2D *screen);

#endif /* _DISPLAY_FUNC_FR_H_ */

