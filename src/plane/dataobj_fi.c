#include "dataobj_fi.h"

#define MAX_DATA_STRING_SIZE_ONE_LINE 64
#define NUMBER_OF_SPLIT 64
#define DATA_DELIMITERS "\t /"
#define DIM 2

void mpfi_dataobj_init(MPFIDataObject *obj)
{
  obj->split = NULL;
  obj->split_allocated = 0;
  obj->split_used = 0;
  obj->data_size = 0;
  mpfi_ptdata_init(obj->data);
  obj->dbl = NULL;
  obj->dbl_size = 0;
  obj->quads = NULL;
  obj->quads_size = 0;
}

void mpfi_dataobj_free_fi(MPFIDataObject *obj)
{
  mpfi_ptdata_free(obj->data);
  obj->data_size = 0;
}

void mpfi_dataobj_free_dbl(MPFIDataObject *obj)
{
  ptdata_free(obj->dbl);
  obj->dbl = NULL;
  obj->dbl_size = 0;
}

void mpfi_dataobj_free_quads(MPFIDataObject *obj)
{
  ptdata_free(obj->quads);
  obj->quads = NULL;
  obj->quads_size = 0;
}

void mpfi_dataobj_free(MPFIDataObject *obj)
{
  mpfi_dataobj_free_fi(obj);
  mpfi_dataobj_free_dbl(obj);
  mpfi_dataobj_free_quads(obj);
  free(obj->split);
}

void mpfi_dataobj_allocate_fi_if_needed(MPFIDataObject *obj, int num)
{
  while(!mpfi_ptdata_allocated_p(obj->data, num)){
    mpfi_ptdata_allocate_farther(obj->data);
  }
}

void mpfi_dataobj_add_fi(MPFIDataObject *obj, mpfi_t fi)
{
  mpfi_dataobj_allocate_fi_if_needed(obj, obj->data_size);
  mpfi_set(mpfi_ptdata_get_mpfi_ptr(obj->data, obj->data_size), fi);
  obj->data_size += 1;
}

void mpfi_dataobj_add_fis(MPFIDataObject *obj, int num, __mpfi_struct *fis)
{
  int i;
  mpfi_dataobj_allocate_fi_if_needed(obj, obj->data_size + num - 1);
  for (i = 0; i < num; i++) {
    mpfi_set(mpfi_ptdata_get_mpfi_ptr(obj->data, obj->data_size) + i, fis + i);
  }
  obj->data_size += num;
}

void mpfi_dataobj_add_vfis(MPFIDataObject *obj, int num, ...)
{
  va_list list;
  int i;

  mpfi_dataobj_allocate_fi_if_needed(obj, obj->data_size + num - 1);
  
  va_start( list, num );
  for (i = 0; i < num; i += 1) {
    mpfi_set(mpfi_ptdata_get_mpfi_ptr(obj->data, obj->data_size) + i, va_arg(list, __mpfi_struct *));
  }
  va_end(list);
  obj->data_size += num;
}

void mpfi_dataobj_allocate_dbl_if_needed(MPFIDataObject *obj, int num)
{
  while(!ptdata_allocated_p(obj->dbl, num)){
    ptdata_allocate_farther(obj->dbl);
  }
}

void mpfi_dataobj_add_dbl(MPFIDataObject *obj, double dbl)
{
  mpfi_dataobj_allocate_dbl_if_needed(obj, obj->dbl_size);
  *ptdata_get_dbl_ptr(obj->dbl, obj->dbl_size) = dbl;
  obj->dbl_size += 1;
}

void mpfi_dataobj_add_dbls(MPFIDataObject *obj, int num, double *dbls)
{
  int i;
  mpfi_dataobj_allocate_dbl_if_needed(obj, obj->dbl_size + num - 1);
  for (i = 0; i < num; i++) {
    *(ptdata_get_dbl_ptr(obj->dbl, obj->dbl_size) + i) = *(dbls + i);
  }
  obj->dbl_size += num;
}

void mpfi_dataobj_add_vdbls(MPFIDataObject *obj, int num, ...)
{
  va_list list;
  int i;
  mpfi_dataobj_allocate_dbl_if_needed(obj, obj->dbl_size + num - 1);
  va_start( list, num );
  for (i = 0; i < num; i += 1) {
    *(ptdata_get_dbl_ptr(obj->dbl, obj->dbl_size) + i) = va_arg(list, double);
  }
  va_end(list);
  obj->dbl_size += num;
}

void mpfi_dataobj_allocate_quads_if_needed(MPFIDataObject *obj, int num)
{
  while(!ptdata_allocated_p(obj->quads, num)){
    ptdata_allocate_farther(obj->quads);
  }
}

void mpfi_dataobj_add_quads_dbl(MPFIDataObject *obj, double dbl)
{
  mpfi_dataobj_allocate_quads_if_needed(obj, obj->quads_size);
  *ptdata_get_dbl_ptr(obj->quads, obj->quads_size) = dbl;
  obj->quads_size += 1;
}

void mpfi_dataobj_add_quads_dbls(MPFIDataObject *obj, int num, double *dbls)
{
  int i;
  mpfi_dataobj_allocate_quads_if_needed(obj, obj->quads_size + num - 1);
  for (i = 0; i < num; i++) {
    *(ptdata_get_dbl_ptr(obj->quads, obj->quads_size) + i) = *(dbls + i);
  }
  obj->quads_size += num;
}

void mpfi_dataobj_add_quads_vdbls(MPFIDataObject *obj, int num, ...)
{
  va_list list;
  int i;
  mpfi_dataobj_allocate_quads_if_needed(obj, obj->quads_size + num - 1);
  va_start( list, num );
  for (i = 0; i < num; i += 1) {
    *(ptdata_get_dbl_ptr(obj->quads, obj->quads_size) + i) = va_arg(list, double);
  }
  va_end(list);
  obj->quads_size += num;
}

static void mpfi_dataobj_allocate_split(MPFIDataObject *obj)
{
  obj->split_allocated += NUMBER_OF_SPLIT;
  obj->split = (int *) realloc(obj->split, obj->split_allocated * sizeof(int));
  if (obj->split == NULL) {
    printf("Can not allocate memory.\n");
    abort();
  }
}

void mpfi_dataobj_set_split(MPFIDataObject *obj, int split_num)
{
  if(obj->split_used >= obj->split_allocated){
    mpfi_dataobj_allocate_split(obj);
  }
  *(obj->split + obj->split_used) = split_num;
  obj->split_used += 1;
}

void mpfi_dataobj_delete_all_splits(MPFIDataObject *obj)
{
  obj->split_used = 0;
}

static void mpfi_dataobj_initialize_dbl(MPFIDataObject *obj)
{
  if(obj->dbl == NULL){
    obj->dbl = (PtData *) malloc(sizeof(PtData));
    ptdata_init(obj->dbl);
  }
}

static void mpfi_dataobj_initialize_quads(MPFIDataObject *obj)
{
  if(obj->quads == NULL){
    obj->quads = (PtData *) malloc(sizeof(PtData));
    ptdata_init(obj->quads);
  }
}

void mpfi_dataobj_mid_translation(double *ret, __mpfi_struct *x, __mpfr_struct *base_pt, __mpfr_struct *base_scale)
{
  mpfr_t tmp;
  int prec[2] = { mpfi_get_prec(x), mpfi_get_prec(x + 1) };
  mpfr_init2(tmp, (prec[0] <= prec[1] ? prec[1] : prec[0]));

  mpfi_get_fr(tmp, x);
  mpfr_sub(tmp, tmp, base_pt, GMP_RNDN);
  mpfr_mul(tmp, tmp, base_scale, GMP_RNDN);
  *ret = mpfr_get_d(tmp, GMP_RNDN);
  
  mpfi_get_fr(tmp, x + 1);
  mpfr_sub(tmp, tmp, (base_pt + 1), GMP_RNDN);
  mpfr_mul(tmp, tmp, base_scale, GMP_RNDN);
  *(ret + 1) = mpfr_get_d(tmp, GMP_RNDN);

  mpfr_clear(tmp);
}

void mpfi_dataobj_set_mpfi_to_dbl(MPFIDataObject *obj)
{
  int i;
  mpfi_dataobj_initialize_dbl(obj);
  mpfi_dataobj_allocate_dbl_if_needed(obj, obj->data_size);
  for(i = 0; i < obj->data_size; i += 2){
    *ptdata_get_dbl_ptr(obj->dbl, i) = mpfi_get_d(mpfi_ptdata_get_mpfi_ptr(obj->data, i));
    *ptdata_get_dbl_ptr(obj->dbl, i + 1) = mpfi_get_d(mpfi_ptdata_get_mpfi_ptr(obj->data, i + 1));
  }
  obj->dbl_size += obj->data_size;
}

void mpfi_dataobj_set_dbl_data(MPFIDataObject *obj,  __mpfr_struct *base_pt, __mpfr_struct *base_scale){
  int i;
  mpfi_dataobj_initialize_dbl(obj);
  mpfi_dataobj_allocate_dbl_if_needed(obj, obj->data_size);
  for(i = 0; i < obj->data_size; i += 2){
    mpfi_dataobj_mid_translation(ptdata_get_dbl_ptr(obj->dbl, i), mpfi_ptdata_get_mpfi_ptr(obj->data, i), base_pt, base_scale);
  }
  obj->dbl_size += obj->data_size;
}

void mpfi_dataobj_set_mpfi_to_quads(MPFIDataObject *obj)
{
  int i, j;
  double x_left, x_right, y_left, y_right;
  mpfi_dataobj_initialize_quads(obj);
  mpfi_dataobj_allocate_quads_if_needed(obj, obj->data_size * 4);
  for(i = 0; i < obj->data_size; i += 2){
    j = i * 4;
    x_left = mpfr_get_d(&((mpfi_ptdata_get_mpfi_ptr(obj->data, i))->left), GMP_RNDD);
    x_right = mpfr_get_d(&((mpfi_ptdata_get_mpfi_ptr(obj->data, i))->right), GMP_RNDU);
    y_left = mpfr_get_d(&(mpfi_ptdata_get_mpfi_ptr(obj->data, i + 1)->left), GMP_RNDD);
    y_right = mpfr_get_d(&(mpfi_ptdata_get_mpfi_ptr(obj->data, i + 1)->right), GMP_RNDU);
        
    *ptdata_get_dbl_ptr(obj->quads, j) = x_left;
    *ptdata_get_dbl_ptr(obj->quads, j + 1) = y_left;
    *ptdata_get_dbl_ptr(obj->quads, j + 2) = x_right;
    *ptdata_get_dbl_ptr(obj->quads, j + 3) = y_left;
    *ptdata_get_dbl_ptr(obj->quads, j + 4) = x_right;
    *ptdata_get_dbl_ptr(obj->quads, j + 5) = y_right;
    *ptdata_get_dbl_ptr(obj->quads, j + 6) = x_left;
    *ptdata_get_dbl_ptr(obj->quads, j + 7) = y_right;
  }
  obj->quads_size += (obj->data_size * 4);
}

void mpfi_dataobj_init_read_file (MPFIDataObject *obj, const char *file)
{
  DATA_FILE *data_file;
  gchar **buf;
  int max_line_size, line_size, i;
  char *line;
  __mpfi_struct *ptr;

  mpfi_dataobj_init(obj);
  data_file = data_file_new(file);
  data_file_open_read(data_file);
  data_file_init_line(&max_line_size, &line);
  while (data_file_gets_line(&line, &line_size, &max_line_size, data_file)) {
    if (line[0] == '\0') {
      mpfi_dataobj_set_split(obj, obj->data_size);
    } else if (line[0] != '#') {
      mpfi_dataobj_allocate_fi_if_needed(obj, obj->data_size + DIM);
      buf = g_strsplit_set(line, DATA_DELIMITERS, 0);
      for (i = 0; i < DIM; i++) {
        if (buf[DIM * i] && buf[DIM * i + 1]) {
          ptr = mpfi_ptdata_get_mpfi_ptr(obj->data, obj->data_size);
          mpfr_set_str(&(ptr->left), buf[DIM * i], 10, GMP_RNDD);
          mpfr_set_str(&(ptr->right), buf[DIM * i + 1], 10, GMP_RNDU);
          obj->data_size += 1;
        } else {
          fprintf(stderr, "Invalid data: number of culomns of data and dimension does not coincide.\n");
          abort();
        }
      }
      g_strfreev(buf);
    }
  }
  free(line);
  data_file_delete(data_file);
}

void mpfi_dataobj_quads_translation(double *ret_x, double *ret_y, __mpfi_struct *x, __mpfr_struct *base_pt, __mpfr_struct *base_scale)
{
  mpfr_t tmp;
  int prec[2] = { mpfi_get_prec(x), mpfi_get_prec(x + 1) };
  mpfr_init2(tmp, (prec[0] <= prec[1] ? prec[1] : prec[0]));

  mpfr_sub(tmp, &(x->left), base_pt, GMP_RNDN);
  mpfr_mul(tmp, tmp, base_scale, GMP_RNDN);
  *ret_x = mpfr_get_d(tmp, GMP_RNDN);

  mpfr_sub(tmp, &(x->right), base_pt, GMP_RNDN);
  mpfr_mul(tmp, tmp, base_scale, GMP_RNDN);
  *(ret_x + 1) = mpfr_get_d(tmp, GMP_RNDN);

  mpfr_sub(tmp, &((x + 1)->left), (base_pt + 1), GMP_RNDN);
  mpfr_mul(tmp, tmp, base_scale, GMP_RNDN);
  *ret_y = mpfr_get_d(tmp, GMP_RNDN);

  mpfr_sub(tmp, &((x + 1)->right), (base_pt + 1), GMP_RNDN);
  mpfr_mul(tmp, tmp, base_scale, GMP_RNDN);
  *(ret_y + 1) = mpfr_get_d(tmp, GMP_RNDN);

  mpfr_clear(tmp);
}

void mpfi_dataobj_set_quads_data(MPFIDataObject *obj,  __mpfr_struct *base_pt, __mpfr_struct *base_scale){
  int i, j;
  double x[2], y[2];
  mpfi_dataobj_initialize_quads(obj);
  mpfi_dataobj_allocate_quads_if_needed(obj, obj->data_size * 4);
  for(i = 0; i < obj->data_size; i += 2){
    j = i * 4;
    mpfi_dataobj_quads_translation(x, y, mpfi_ptdata_get_mpfi_ptr(obj->data, i), base_pt, base_scale);

    *ptdata_get_dbl_ptr(obj->quads, j) = x[0];
    *ptdata_get_dbl_ptr(obj->quads, j + 1) = y[0];
    *ptdata_get_dbl_ptr(obj->quads, j + 2) = x[1];
    *ptdata_get_dbl_ptr(obj->quads, j + 3) = y[0];
    *ptdata_get_dbl_ptr(obj->quads, j + 4) = x[1];
    *ptdata_get_dbl_ptr(obj->quads, j + 5) = y[1];
    *ptdata_get_dbl_ptr(obj->quads, j + 6) = x[0];
    *ptdata_get_dbl_ptr(obj->quads, j + 7) = y[1];
  }
  obj->quads_size += (obj->data_size * 4);
}


void mpfi_dataobj_set_default_style(MPFIDataObject *obj, PlotStyle style){
  obj->default_style = style;
}

void mpfi_dataobj_draw_lines(MPFIDataObject *obj){
  if(obj->split_used > 0){
    int i;
    draw_ptdata_lines(obj->dbl, 0, obj->split[0]);
    for(i = 0; i < obj->split_used - 1; i++){
      draw_ptdata_lines(obj->dbl, obj->split[i], obj->split[i + 1]);
    }
    draw_ptdata_lines(obj->dbl, obj->split[i], obj->data_size);
  }else{
    draw_ptdata_lines(obj->dbl, 0, obj->data_size);
  }
}

void mpfi_dataobj_draw_dots(MPFIDataObject *obj){
  draw_ptdata_dots(obj->dbl, 0, obj->data_size);
}

void mpfi_dataobj_draw_quads(MPFIDataObject *obj){
  draw_ptdata_quads(obj->quads, 0, obj->data_size * 4);
}

void mpfi_dataobj_draw_quads_edge(MPFIDataObject *obj){
  draw_ptdata_quads_edge(obj->quads, 0, obj->data_size * 4);
}

void mpfi_dataobj_draw_points_cross(MPFIDataObject *obj, double size){
  draw_ptdata_point_cross(obj->dbl, 0, obj->data_size, size);
}

void mpfi_dataobj_output_to_gnuplot(MPFIDataObject *obj, FILE *pipe_gp){
  draw_ptdata_to_gnuplot(obj->dbl, 0, obj->data_size, pipe_gp);
}

void mpfi_dataobj_output_to_gnuplot_for_quads(MPFIDataObject *obj, FILE *pipe_gp){
  draw_ptdata_to_gnuplot_as_quads(obj->quads, 0, obj->data_size * 4, pipe_gp);
}

void mpfi_dataobj_output_to_gnuplot_default_style(MPFIDataObject *obj, FILE *pipe_gp){
  switch(obj->default_style){
  case STY_QUADS:
  case STY_QUADS_EDGE:
    mpfi_dataobj_output_to_gnuplot_for_quads(obj, pipe_gp);
    break;
  default:
    mpfi_dataobj_output_to_gnuplot(obj, pipe_gp);
    break;
  }
  fprintf(pipe_gp, "e\n");
}

