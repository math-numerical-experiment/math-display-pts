#include "header.h"

#include <inttypes.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include <GL/glc.h>

#include "dataobj.h"
#include "quaternion.h"

typedef enum {
  PLANE_NOT_SHOWN,
  PLANE_FILL_XY,
  PLANE_FILL_YZ,
  PLANE_FILL_XZ,
  PLANE_GRID_XY,
  PLANE_GRID_YZ,
  PLANE_GRID_XZ
} PlaneType;

typedef enum {
  SURFACE_XY,
  SURFACE_XZ,
  SURFACE_YX,
  SURFACE_YZ,
  SURFACE_ZX,
  SURFACE_ZY
} Surface;

typedef struct {
  mpfr_ptr base_pt;
  mpfr_t base_scale;
  int prec;

  int dim;
  GLdouble center[3];
  GLdouble size[4];
  GLdouble distance;
  GLdouble quaternion[4];
  GLdouble zoom;
  int win_size[2];

  GLint font;

  GLfloat lambient[4];
  GLfloat ldiffuse[4];
  GLfloat lspecular[4];
  GLfloat lposition[4];

  TCLIST *objects;

  GLdouble basal_plane_center[3];
  PlaneType basal_plane_type;

  double cross_length;
} Screen;

typedef enum {
  MOVE_LEFT,
  MOVE_RIGHT,
  MOVE_UP,
  MOVE_DOWN,
  MOVE_FORWARD,
  MOVE_BACKWARD
} ScreenDir;

Screen *screen_new(int dim, int prec);
void screen_initialize_font (Screen *screen);
void screen_initialize_light (Screen *screen);
void screen_delete(Screen *screen);
void screen_set_window_size(Screen *screen, int win_x, int win_y);
void screen_apply_projection(Screen *screen);
void screen_set_center(Screen *screen, GLdouble *center);
void screen_set_basal_plane_center(Screen *screen, GLdouble *center);
void screen_set_basal_plane_type(Screen *screen, PlaneType type);
void screen_set_size(Screen *screen, GLdouble xsize, GLdouble ysize);
void screen_set_depth(Screen *screen, GLdouble z1, GLdouble z2);
void screen_change_window_size(Screen *screen, int new_win_x, int new_win_y);
void screen_set_light_value(Screen *screen, GLenum type, GLfloat *value);
void screen_apply_light_settings(Screen *screen);
void screen_move(Screen *screen, ScreenDir dir, double move);
void screen_move_coordinate(Screen *screen, GLdouble x, GLdouble y, GLdouble z);
void screen_zoom(Screen *screen, GLdouble zoom);
void screen_reset_rotation(Screen *screen);
void screen_rotate_around_x(Screen *screen, GLdouble angle);
void screen_rotate_around_y(Screen *screen, GLdouble angle);
void screen_rotate_around_z(Screen *screen, GLdouble angle);
void screen_rotate_to_surface(Screen *screen, Surface surface, int quadrant);

#define screen_up(screen, move) screen_move(screen, MOVE_UP, move / screen->zoom)
#define screen_down(screen, move) screen_move(screen, MOVE_DOWN, move / screen->zoom)
#define screen_left(screen, move) screen_move(screen, MOVE_LEFT, move / screen->zoom)
#define screen_right(screen, move) screen_move(screen, MOVE_RIGHT, move / screen->zoom)
#define screen_forward(screen, move) screen_move(screen, MOVE_FORWARD, move / screen->zoom)
#define screen_backward(screen, move) screen_move(screen, MOVE_BACKWARD, move / screen->zoom)
#define screen_move_to(screen, x, y, z) screen_move_coordinate(screen, x - screen->center[0], y - screen->center[1], z - screen->center[2])

void screen_show_axis(Screen *screen);
void screen_show_plane (Screen *screen);

void screen_add_object(Screen *screen, DataObj *obj);
DataObj *screen_get_object(Screen *screen, int num);
void screen_plot_all_objects(Screen *screen);

