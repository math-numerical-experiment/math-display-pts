#include "displaypts_cli.h"

static struct arg_file *data_file_arg_get_file (DataFileArg *arg, int num)
{
  return g_array_index(arg->file, struct arg_file *, num);
}

static int data_file_arg_get_type (DataFileArg *arg, int num)
{
  return g_array_index(arg->type, int, num);
}

static int data_file_arg_get_dim (DataFileArg *arg, int num)
{
  return g_array_index(arg->dim, int, num);
}

static int data_file_arg_get_data_type (DataFileArg *arg, int  num)
{
  return g_array_index(arg->data_type, int, num);
}

DataFileArg *data_file_arg_new ()
{
  DataFileArg *arg;
  arg = malloc(sizeof(DataFileArg));
  arg->file = g_array_new(FALSE, FALSE, sizeof(struct arg_file *));
  arg->type = g_array_new(FALSE, FALSE, sizeof(int));
  arg->dim = g_array_new(FALSE, FALSE, sizeof(int));
  arg->data_type = g_array_new(FALSE, FALSE, sizeof(DataType));
  return arg;
}

void data_file_arg_delete (DataFileArg *arg)
{
  g_array_free(arg->file, TRUE);
  g_array_free(arg->type, TRUE);
  g_array_free(arg->dim, TRUE);
  g_array_free(arg->data_type, TRUE);
  free(arg);
}

void data_file_arg_add(DataFileArg *arg, void *file_arg, DisplayType display_type, DataType data_type, int dim)
{
  g_array_append_val(arg->file, file_arg);
  g_array_append_val(arg->type, display_type);
  g_array_append_val(arg->dim, dim);
  g_array_append_val(arg->data_type, data_type);
}

/* Return a list of Dataobj, which must be freed after the use. */
DataObj **data_file_arg_initialize_objects(DataFileArg *arg, int *size)
{
  int obj_count, i, j;
  DataObj **obj_list;
  DataType data_type;
  struct arg_file *f;

  *size = 0;
  for (i = 0; i < arg->file->len; i++) {
    *size += data_file_arg_get_file(arg, i)->count;
  }

  obj_list = (DataObj **) malloc(sizeof(DataObj *) * (*size));
  obj_count = 0;
  for (j = 0; j < arg->file->len; j++) {
    f = data_file_arg_get_file(arg, j);
    for (i = 0; i < f->count; i++) {
      data_type = data_file_arg_get_data_type(arg, j);
      obj_list[obj_count] = dataobj_new(data_file_arg_get_dim(arg, j), data_type);
      dataobj_set_display_type(obj_list[obj_count], data_file_arg_get_type(arg, j));
      dataobj_init_read_file(obj_list[obj_count], *(f->filename + i));
      obj_count += 1;
    }
  }
  return obj_list;
}

void cli_screen_initial_rotation (Screen *screen, const char *s)
{
  Surface surface;
  int len, quadrant;
  quadrant = 0;
  len = strlen(s);
  if ((len == 2 || len == 3) && (s[0] >= 'x' && s[0] <= 'z' && s[1] >= 'x' && s[1] <= 'z' && s[0] != s[1])) {
    if (len == 3 && s[2] >= '2' && s[2] <= '4') {
      quadrant = s[2] - '0';
    }

    if (s[0] == 'x' && s[1] == 'y') {
      surface = SURFACE_XY;
    } else if (s[0] == 'y' && s[1] == 'x') {
      surface = SURFACE_YX;
    } else if (s[0] == 'x' && s[1] == 'z') {
      surface = SURFACE_XZ;
    } else if (s[0] == 'z' && s[1] == 'x') {
      surface = SURFACE_ZX;
    } else if (s[0] == 'y' && s[1] == 'z') {
      surface = SURFACE_YZ;
    } else if (s[0] == 'z' && s[1] == 'y') {
      surface = SURFACE_ZY;
    }
  } else {
    fprintf(stderr, "Invalid plane argument.\n");
  }
  screen_rotate_to_surface(screen, surface, quadrant);
}
