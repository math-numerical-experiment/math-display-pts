#ifndef _SCREEN_H_
#define _SCREEN_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <GL/freeglut.h>
#include <GL/glc.h>

#define NOT_USE_ALPHA -1.0

#define SCREEN_MAX_STRING_SIZE_ONE_LINE 100
#define SCALE_BAR_LENGTH 0.5

typedef struct __Screen2D{
  double center[2];
  double zoom;
  double size[2];
  double default_move;
  double default_zoom;
  double grid_size;
  double cross_size;

  int win_size[2];
  int font;
} Screen2D;


void screen2d_set_variables(Screen2D *screen, double x, double y, double zoom, double size_x, double size_y,
			    double default_move, double default_zoom, double grid_size, double cross_size);
void screen2d_set_win_size(Screen2D *screen, int win_size_x, int win_size_y);
void screen2d_initialize_font(Screen2D *screen);
void screen2d_set_crd_size(Screen2D *screen, double size_x, double size_y);

void screen2d_move(Screen2D *screen, double x, double y);
#define screen2d_up(screen) screen2d_move(screen, 0.0, screen->default_move / screen->zoom)
#define screen2d_down(screen) screen2d_move(screen, 0.0, -screen->default_move / screen->zoom)
#define screen2d_left(screen) screen2d_move(screen, -screen->default_move / screen->zoom, 0.0)
#define screen2d_right(screen) screen2d_move(screen, screen->default_move / screen->zoom, 0.0)
#define screen2d_move_to(screen, x, y) screen2d_move(screen, x - screen->center[0], y - screen->center[1])

void screen2d_zoom(Screen2D *screen, double zoom);
#define screen2d_zoom_in(screen) screen2d_zoom(screen, screen->default_zoom)
#define screen2d_zoom_out(screen) screen2d_zoom(screen, 1.0 / screen->default_zoom)
#define screen2d_zoom_to(screen, amp) screen2d_zoom(screen, amp / screen->zoom)

void screen2d_ranges(Screen2D *screen, double *x, double *y);
void screen2d_move_to_pixel(Screen2D *screen, int x, int y);
void screen2d_change_win_size(Screen2D *screen, int new_win_x, int new_win_y);

void screen2d_cycle_show_grid(Screen2D *screen);
void screen2d_toggle_show_scale_bar(Screen2D *screen);
void screen2d_show_scale_bar(Screen2D *screen);
void screen2d_toggle_show_grid(Screen2D *screen);
void screen2d_show_grid(Screen2D *screen);
void screen2d_cycle_show_state(Screen2D *screen);
void screen2d_show_current_state(Screen2D *screen);

void screen2d_malloc_lines(char ***lines, int num, int size);
#define screen2d_malloc_fit_lines(lines, num) screen2d_malloc_lines(lines, num, SCREEN_MAX_STRING_SIZE_ONE_LINE)
void screen2d_free_lines(char ***lines, int num);
void screen2d_show_lines_from_top(Screen2D *screen, char **lines, int num);
void screen2d_gnuplot_set_range(Screen2D *screen, FILE *pipe_gp);
void screen2d_gnuplot_set_divisions(Screen2D *screen, FILE *pipe_gp);

typedef struct __Color{
  int count;
  float alpha;
} Color;

void color_count_reset(Color *col);
void color_set_alpha(Color *col, float alpha);
void color_change(Color *col);

#endif /* _SCREEN_H_ */

