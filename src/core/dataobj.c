#include "dataobj.h"

double *dataobj_allocate_double_pt(int dim)
{
  double *ptr;
  ptr = (double *) malloc(sizeof(double) * dim);
  if (!ptr) {
    fprintf(stderr, "Can not allocate memory of a point.\n");
    abort();
  }
  return ptr;
}

mpfr_ptr dataobj_allocate_mpfr_pt(int dim, int prec)
{
  int i;
  mpfr_ptr ptr;
  ptr = (mpfr_ptr) malloc(sizeof(__mpfr_struct) * dim);
  if (!ptr) {
    fprintf(stderr, "Can not allocate memory of a point.\n");
    abort();
  }
  if (prec <= 0) {
    prec = mpfr_get_default_prec();
  }
  for (i = 0; i < dim; i++) {
    mpfr_init2(ptr + i, prec);
  }
  return ptr;
}

void dataobj_clear_mpfr_pt(mpfr_ptr ptr, int dim)
{
  int i;
  for (i = 0; i < dim; i++) {
    mpfr_clear(ptr + i);
  }
}

DataObj *dataobj_new(int dim, DataType type)
{
  DataObj *dataobj;
  dataobj = malloc(sizeof(DataObj));
  if (!dataobj) {
    fprintf(stderr, "Can not allocate memory of DataObj.\n");
    abort();
  }
  dataobj->list = tclistnew();
  dataobj->split = tclistnew();
  dataobj->data_type = type;
  dataobj->hold = false;
  dataobj->dim = dim;
  dataobj->cache = NULL;
  dataobj->cache_allocated = 0;
  dataobj->cache_num = 0;
  dataobj->prec = -1;
  dataobj->display_type = DISPLAY_TYPE_DOTS;
  return dataobj;
}

void dataobj_delete(DataObj *dataobj)
{
  tclistdel(dataobj->list);
  tclistdel(dataobj->split);
  if (dataobj->cache) {
    free(dataobj->cache);
  }
  free(dataobj);
}

void dataobj_hold(DataObj *dataobj)
{
  dataobj->hold = true;
}

void dataobj_unhold(DataObj *dataobj)
{
  dataobj->hold = false;
}

void dataobj_set_display_type(DataObj *dataobj, DisplayType type)
{
  dataobj->display_type = type;
}

void dataobj_set_prec(DataObj *dataobj, int prec)
{
  dataobj->prec = prec;
}

/* If num is negative, push data at the end of list. */
void dataobj_insert(DataObj *dataobj, int num, void *pt)
{
  int size;
  if (dataobj->hold) {
    abort();
  }
  if (dataobj->data_type == DATA_MPFR) {
    size = sizeof(mpfr_ptr);
  } else if (dataobj->data_type == DATA_DOUBLE) {
    size = sizeof(double *);
  /* } else if (dataobj->data_type == DATA_MPFR) { */
  } else {
    fprintf(stderr, "Invalid data type\n");
    abort();
  }
  if (num >= 0) {
    tclistinsert(dataobj->list, num, &pt, size);
  } else {
    tclistpush(dataobj->list, &pt, size);
  }
}

void dataobj_copy_insert(DataObj *dataobj, int num, void *pt)
{
  int i;
  if (dataobj->data_type == DATA_MPFR) {
    mpfr_ptr ptr, new_ptr;
    ptr = (mpfr_ptr) pt;
    new_ptr = dataobj_allocate_mpfr_pt(dataobj->dim, dataobj->prec);
    for (i = 0; i < dataobj->dim; i++) {
      mpfr_set(new_ptr + i, ptr + i, GMP_RNDN);
    }
    dataobj_insert(dataobj, num, new_ptr);
  } else if (dataobj->data_type == DATA_DOUBLE) {
    double *ptr, *new_ptr;
    ptr = (double *) pt;
    new_ptr = dataobj_allocate_double_pt(dataobj->dim);
    for (i = 0; i < dataobj->dim; i++) {
      *(new_ptr + i) = *(ptr + i);
    }
    dataobj_insert(dataobj, num, new_ptr);
  /* } else if (dataobj->data_type == DATA_MPFR) { */
  } else {
    fprintf(stderr, "Invalid data type\n");
    abort();
  }
}

void dataobj_remove(DataObj *dataobj, int num)
{
  int size;
  if (dataobj->hold) {
    abort();
  }
  if (dataobj->data_type == DATA_MPFR) {
    mpfr_ptr ptr;
    ptr = *(mpfr_ptr *) tclistval2(dataobj->list, num);
    dataobj_clear_mpfr_pt(ptr, dataobj->dim);
    free(ptr);
  } else if (dataobj->data_type == DATA_DOUBLE) {
    double *ptr;
    ptr = *(double **) tclistval2(dataobj->list, num);
    free(ptr);
  /* } else if (dataobj->data_type == DATA_MPFR) { */
  } else {
    fprintf(stderr, "Invalid data type\n");
    abort();
  }
  tclistremove(dataobj->list, num, &size);
}

void dataobj_change(DataObj *dataobj, int num, void *pt)
{
  if (dataobj->hold) {
    abort();
  }
  int i;
  if (dataobj->data_type == DATA_DOUBLE) {
    double *ptr, *old_ptr;
    ptr = (double *) pt;
    old_ptr = *(double **) tclistval2(dataobj->list, num);
    for (i = 0; i < dataobj->dim; i++) {
      *(old_ptr + i) = *(ptr + i);
    }
  } else if (dataobj->data_type == DATA_MPFR) {
    mpfr_ptr ptr, old_ptr;
    ptr = (mpfr_ptr) pt;
    old_ptr = *(mpfr_ptr *) tclistval2(dataobj->list, num);
    for (i = 0; i < dataobj->dim; i++) {
      mpfr_set(old_ptr + i, ptr, GMP_RNDN);
    }
  } else if (dataobj->data_type == DATA_MPFI) {
    fprintf(stderr, "Under construction\n");
    abort();
  } else {
    fprintf(stderr, "Invalid data type\n");
    abort();
  }
}

/* If split is negative, add splitter to the tail. */
void dataobj_add_splitter(DataObj *dataobj, int split)
{
  int i, size, *val;
  if (split < 0) {
    split = tclistnum(dataobj->list);
  }
  if (tclistbsearch(dataobj->split, &split, sizeof(int)) < 0) {
    for (i = 0; i < tclistnum(dataobj->split); i++) {
      val = (int *) tclistval(dataobj->split, i, &size);
      if (split < *val) {
        break;
      }
    }
    tclistinsert(dataobj->split, i, &split, sizeof(int));
  }
}

void dataobj_clear_splitter(DataObj *dataobj)
{
  tclistclear(dataobj->split);
}

static void dataobj_allocate_cache(DataObj *dataobj)
{
  dataobj->cache_num = tclistnum(dataobj->list);
  dataobj->cache = (double *) realloc(dataobj->cache, sizeof(double) * dataobj->dim * dataobj->cache_num);
  if (!dataobj->cache) {
    fprintf(stderr, "Can not allocate memory of dataobj->cache.\n");
    abort();
  }
  dataobj->cache_allocated = dataobj->dim * dataobj->cache_num;
}

void dataobj_set_cache_for_double(DataObj *dataobj)
{
  int i, j, size;
  double *pt, *cache;
  if (dataobj->cache_num > dataobj->cache_allocated) {
    dataobj_allocate_cache(dataobj);
  }
  for (i = 0; i < dataobj->cache_num; i++) {
    pt = *(double **) tclistval(dataobj->list, i, &size);
    cache = dataobj->cache + dataobj->dim * i;
    for (j = 0; i < dataobj->dim; j++) {
      cache[j] = pt[j];
    }
  }
}

void dataobj_set_cache_for_mpfr(DataObj *dataobj, mpfr_ptr center, mpfr_ptr scale)
{
  int i, j, size;
  mpfr_ptr pt, buf = NULL;
  double *cache;
  if (dataobj->cache_num > dataobj->cache_allocated) {
    dataobj_allocate_cache(dataobj);
  }
  mpfr_init2(buf, (dataobj->prec <= 0 ? mpfr_get_default_prec() : dataobj->prec));
  for (i = 0; i < dataobj->cache_num; i++) {
    pt = *(mpfr_ptr *) tclistval(dataobj->list, i, &size);
    cache = dataobj->cache + dataobj->dim * i;
    for (j = 0; i < dataobj->dim; j++) {
      mpfr_sub(buf, pt + j, center + j, MPFR_RNDN);
      mpfr_mul(buf, buf, scale, MPFR_RNDN);
      cache[j] = mpfr_get_d(buf, MPFR_RNDN);
    }
  }
  mpfr_clear(buf);
}

static void dataobj_draw_vertex(DataObj *dataobj, GLenum mode, int start, int num)
{
  int i, size;
  double *pt;
  glBegin(mode);
  for (i = start; i < start + num; i++) {
    pt = *(double **) tclistval(dataobj->list, i, &size);
    switch(dataobj->dim) {
    case 2:
      glVertex2dv(pt);
      break;
    case 3:
      glVertex3dv(pt);
      break;
    }
  }
  glEnd();
}

static void dataobj_outer_project(double *z, double *x, double *y)
{
  double len;
  z[0] = x[1] * y[2] - x[2] * y[1];
  z[1] = x[2] * y[0] - x[0] * y[2];
  z[2] = x[0] * y[1] - x[1] * y[0];
  len = sqrt(z[0] * z[0] + z[1] * z[1] + z[2] * z[2]);
  z[0] /= len;
  z[1] /= len;
  z[2] /= len;
}

static void dataobj_normal_vector_of_triangle(double *n, double *x, double *y, double *z)
{
  double v1[3], v2[3];
  v1[0] = x[0] - z[0];
  v1[1] = x[1] - z[1];
  v1[2] = x[2] - z[2];
  v2[0] = y[0] - z[0];
  v2[1] = y[1] - z[1];
  v2[2] = y[2] - z[2];
  dataobj_outer_project(n, v1, v2);
}

static void dataobj_draw_surface (DataObj *dataobj)
{
  int i, j, line_size, split_num, current, next;
  double *pt_new[2], *pt_old[2], n[3];
  split_num = tclistnum(dataobj->split);
  if (split_num <= 0) {
    fprintf(stderr, "Invalid surface data: split_num=%d\n", split_num);
    abort();
  }
  line_size = *(int *) tclistval2(dataobj->split, 0);
  if (line_size < 2) {
    fprintf(stderr, "Invalid surface data: line_size=%d\n", line_size);
    abort();
  }
  for (i = 0; i < split_num; i++) {
    current = *(int *) tclistval2(dataobj->split, i);
    if (i < split_num - 1) {
      next = *(int *) tclistval2(dataobj->split, i + 1);
    } else {
      next = tclistnum(dataobj->list);
      if (next == current) {
        break;
      }
    }
    if (next - current != line_size) {
      fprintf(stderr, "Invalid surface data\n");
      abort();
    }
    glBegin(GL_TRIANGLE_STRIP);
    pt_old[0] = *(double **) tclistval2(dataobj->list, current - line_size);
    pt_old[1] = *(double **) tclistval2(dataobj->list, current);
    switch(dataobj->dim) {
    case 2:
      glVertex2dv(pt_old[0]);
      glVertex2dv(pt_old[1]);
      break;
    case 3:
      glVertex3dv(pt_old[0]);
      glVertex3dv(pt_old[1]);
      break;
    }
    for (j = 1; j < line_size; j++) {
      pt_new[0] = *(double **) tclistval2(dataobj->list, current - line_size + j);
      pt_new[1] = *(double **) tclistval2(dataobj->list, current + j);
      switch(dataobj->dim) {
      case 2:
        glVertex2dv(pt_new[0]);
        glVertex2dv(pt_new[1]);
        break;
      case 3:
        dataobj_normal_vector_of_triangle(n, pt_old[0], pt_old[1], pt_new[0]);
        glNormal3d(n[0], n[1], n[2]);
        glVertex3dv(pt_new[0]);
        dataobj_normal_vector_of_triangle(n, pt_old[1], pt_new[0], pt_new[1]);
        glNormal3d(n[0], n[1], n[2]);
        glVertex3dv(pt_new[1]);
        pt_old[0] = pt_new[0];
        pt_old[1] = pt_new[1];
        break;
      }
    }
    glEnd();
  }
}

static void dataobj_draw_primitive (DataObj *dataobj, int gl_line_type)
{
  int i, start, split_num, pt_max, next;
  start = 0;
  split_num = tclistnum(dataobj->split);
  if (dataobj->cache) {
    pt_max = dataobj->cache_num;
    glVertexPointer(dataobj->dim, GL_DOUBLE, 0, dataobj->cache);
    for (i = 0; i < split_num; i++) {
      next = *(int *) tclistval2(dataobj->split, i);
      glDrawArrays(gl_line_type, start, next - start);
      start = next;
      if (pt_max <= start) {
        break;
      }
    }
    if (pt_max > start) {
      glDrawArrays(gl_line_type, start, pt_max - start);
    }
  } else {
    pt_max = tclistnum(dataobj->list);
    for (i = 0; i < split_num; i++) {
      next = *(int *) tclistval2(dataobj->split, i);
      dataobj_draw_vertex(dataobj, gl_line_type, start, next - start);
      start = next;
      if (pt_max <= start) {
        break;
      }
    }
    if (pt_max > start) {
      dataobj_draw_vertex(dataobj, gl_line_type, start, pt_max - start);
    }
  }
}

void dataobj_draw_coordinate(DataObj *dataobj)
{
  if (dataobj->display_type == DISPLAY_TYPE_SURFACE) {
    dataobj_draw_surface(dataobj);
  } else {
    int gl_line_type;
    switch (dataobj->display_type) {
    case DISPLAY_TYPE_LINES:
      gl_line_type = GL_LINES;
      break;
    case DISPLAY_TYPE_LINE_STRIP:
      gl_line_type = GL_LINE_STRIP;
      break;
    case DISPLAY_TYPE_LINE_LOOP:
      gl_line_type = GL_LINE_LOOP;
      break;
    case DISPLAY_TYPE_DOTS:
      gl_line_type = GL_POINTS;
      break;
    case DISPLAY_TYPE_LINES_POINTS:
      gl_line_type = GL_LINES;
      break;
    case DISPLAY_TYPE_POINTS:
      break;
    default:
      fprintf(stderr, "Can not support the data type.\n");
      abort();
    }
    dataobj_draw_primitive(dataobj, gl_line_type);
  }
}

void dataobj_draw_cross (DataObj *dataobj, double len)
{
  int i, start, pt_max, size;
  double *pt;
  start = 0;
  pt_max = tclistnum(dataobj->list);
  /* Use of cache has not yet implemented */
  glBegin(GL_LINES);
  for (i = 0; i < pt_max; i++) {
    pt = *(double **) tclistval(dataobj->list, i, &size);
    switch(dataobj->dim) {
    case 2:
      glVertex2d(pt[0] + len, pt[1] + len);
      glVertex2d(pt[0] - len, pt[1] - len);
      glVertex2d(pt[0] - len, pt[1] + len);
      glVertex2d(pt[0] + len, pt[1] - len);
      break;
    case 3:
      glVertex3d(pt[0] + len, pt[1] + len, pt[2] + len);
      glVertex3d(pt[0] - len, pt[1] - len, pt[2] - len);
      glVertex3d(pt[0] - len, pt[1] + len, pt[2] + len);
      glVertex3d(pt[0] + len, pt[1] - len, pt[2] - len);
      glVertex3d(pt[0] + len, pt[1] + len, pt[2] - len);
      glVertex3d(pt[0] - len, pt[1] - len, pt[2] + len);
      glVertex3d(pt[0] - len, pt[1] + len, pt[2] - len);
      glVertex3d(pt[0] + len, pt[1] - len, pt[2] + len);
      break;
    }
  }
  glEnd();
}
