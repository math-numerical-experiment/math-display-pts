#include "screen.h"
#include <math.h>

#define SCREEN_DEPTH 1.0
#define GRID_Z_POS -0.5
#define STRING_Z_POS 0.5
#define GLC_FONT_NAME "DejaVu Sans Mono"

void screen2d_set_variables(Screen2D *screen, double x, double y, double zoom, double size_x, double size_y,
			    double default_move, double default_zoom, double grid_size, double cross_size){
  screen->center[0] = x;
  screen->center[1] = y;
  screen->zoom = zoom;
  screen->size[0] = size_x;
  screen->size[1] = size_y;
  screen->default_move = default_move;
  screen->default_zoom = default_zoom;
  screen->grid_size = grid_size;
  screen->cross_size = cross_size;
}

void screen2d_set_win_size(Screen2D *screen, int win_size_x, int win_size_y){
  screen->win_size[0] = win_size_x;
  screen->win_size[1] = win_size_y;
}

void screen2d_initialize_font(Screen2D *screen){
  screen->font = 1;
  glcContext(glcGenContext());
  glcNewFontFromFamily(1, GLC_FONT_NAME);
  glcFont(screen->font);
}

void screen2d_set_crd_size(Screen2D *screen, double size_x, double size_y){
  double old_center[2] = { screen->center[0], screen->center[1] }, old_zoom = screen->zoom;
  screen->size[0] = size_x;
  screen->size[1] = size_y;

  glMatrixMode(GL_MODELVIEW);
  screen2d_move_to(screen, 0.0, 0.0);
  screen2d_zoom_to(screen, 1.0);
  glLoadIdentity();

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-screen->size[0], screen->size[0], -screen->size[1], screen->size[1], -SCREEN_DEPTH, SCREEN_DEPTH);

  glMatrixMode(GL_MODELVIEW);
  screen2d_move_to(screen, old_center[0], old_center[1]);
  screen2d_zoom_to(screen, old_zoom);
}

void screen2d_move(Screen2D *screen, double x, double y){
  glTranslated(-x, -y, 0);
  screen->center[0] += x;
  screen->center[1] += y;
}

void screen2d_zoom(Screen2D *screen, double zoom){
  glTranslated(screen->center[0], screen->center[1], 0);
  glScalef(zoom, zoom, 1.0);
  glTranslated(-screen->center[0], -screen->center[1], 0);
  screen->zoom *= zoom;
}

void screen2d_ranges(Screen2D *screen, double *x, double *y){
  *x = screen->center[0] - screen->size[0] / screen->zoom;
  *(x + 1) = screen->center[0] + screen->size[0] / screen->zoom;
  *y = screen->center[1] - screen->size[1] / screen->zoom;
  *(y + 1) = screen->center[1] + screen->size[1] / screen->zoom;
}

void screen2d_move_to_pixel(Screen2D *screen, int x, int y){
  screen2d_move(screen, ((double) x - (double) screen->win_size[0] / 2.0) / (double) screen->win_size[0] * screen->size[0] * 2.0 / screen->zoom,
		((double) screen->win_size[1] / 2.0 - (double) y) / (double) screen->win_size[1] * screen->size[1] * 2.0 / screen->zoom);
}

void screen2d_change_win_size(Screen2D *screen, int new_win_x, int new_win_y){
  screen2d_set_crd_size(screen, (double) new_win_x / (double) screen->win_size[0] * screen->size[0],
			(double) new_win_y / (double) screen->win_size[1] * screen->size[1]);
  screen2d_set_win_size(screen, new_win_x, new_win_y);
}

void screen2d_show_grid(Screen2D *screen){
  double coord;

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glTranslated(screen->center[0], screen->center[1], 0);
  glScaled(1.0 / screen->zoom, 1.0 / screen->zoom, 1.0);

  glBegin(GL_LINES);
  glVertex3d(0.0, -screen->size[1], GRID_Z_POS);
  glVertex3d(0.0, screen->size[1], GRID_Z_POS);
  coord = screen->grid_size;
  while(coord < screen->size[0]){
    glVertex3d(coord, -screen->size[1], GRID_Z_POS);
    glVertex3d(coord, screen->size[1], GRID_Z_POS);
    glVertex3d(-coord, -screen->size[1], GRID_Z_POS);
    glVertex3d(-coord, screen->size[1], GRID_Z_POS);
    coord += screen->grid_size;
  }
  
  coord = screen->grid_size;
  glVertex3d(-screen->size[0], 0.0, GRID_Z_POS);
  glVertex3d(screen->size[0], 0.0, GRID_Z_POS);
  while(coord < screen->size[1]){
    glVertex3d(-screen->size[0], coord, GRID_Z_POS);
    glVertex3d(screen->size[0], coord, GRID_Z_POS);
    glVertex3d(-screen->size[0], -coord, GRID_Z_POS);
    glVertex3d(screen->size[0], -coord, GRID_Z_POS);
    coord += screen->grid_size;
  }
  glEnd();

  glPopMatrix();
}

void screen2d_show_scale_bar(Screen2D *screen){
  char scale[SCREEN_MAX_STRING_SIZE_ONE_LINE];

  glPushMatrix();

  glTranslated(screen->center[0], screen->center[1], 0);
  glScaled(1.0 / screen->zoom, 1.0 / screen->zoom, 1.0);
  glTranslated(-screen->size[0] + 0.5, -screen->size[1] + 0.15, 0);

  glBegin(GL_LINES);
  glVertex3d(-SCALE_BAR_LENGTH * 0.5, 0.0, 0.0);
  glVertex3d(SCALE_BAR_LENGTH * 0.5, 0.0, 0.0);
  glVertex3d(-SCALE_BAR_LENGTH * 0.5, SCALE_BAR_LENGTH * 0.05, 0.0);
  glVertex3d(-SCALE_BAR_LENGTH * 0.5, -SCALE_BAR_LENGTH * 0.05, 0.0);
  glVertex3d(SCALE_BAR_LENGTH * 0.5, SCALE_BAR_LENGTH * 0.05, 0.0);
  glVertex3d(SCALE_BAR_LENGTH * 0.5, -SCALE_BAR_LENGTH * 0.05, 0.0);
  glEnd();

  glTranslated(SCALE_BAR_LENGTH * 0.7, -SCALE_BAR_LENGTH * 0.06, STRING_Z_POS);
  glScaled(0.1, 0.1, 1.0);

  sprintf(scale, "% .18e", SCALE_BAR_LENGTH / screen->zoom);
  glcRenderStyle(GLC_TRIANGLE);  
  glcRenderString(scale);

  glPopMatrix();
}

void screen2d_malloc_lines(char ***lines, int num, int size){
  int i;
  *lines = (char **) malloc(sizeof(char *) * num);
  for(i = 0; i < num; i++){
    *(*lines + i) = (char *) malloc(sizeof(char) * size);
  }
}

void screen2d_free_lines(char ***lines, int num){
  int i;
  for(i = 0; i < num; i++){
    free(*(*lines + i));
  }
  free(*lines);
}

void screen2d_show_lines_from_top(Screen2D *screen, char **lines, int num){
  int i;
  glPushMatrix();

  glTranslated(screen->center[0], screen->center[1], 0);
  glScaled(1.0 / screen->zoom, 1.0 / screen->zoom, 1.0);
  glTranslated(-screen->size[0] + 0.1, screen->size[1] - 0.15, STRING_Z_POS);
  glScaled(0.1, 0.1, 1.0);

  glcRenderStyle(GLC_TRIANGLE);  
  for(i = 0; i < num; i++){
    glPushMatrix();
    glcRenderString(*(lines + i));
    glPopMatrix();
    glTranslated(0.0, -2.0, 0);
  }

  glPopMatrix();
}

void screen2d_show_current_state(Screen2D *screen){
  char **lines;
  int num = 6;
  double xrng[2], yrng[2];

  screen2d_malloc_lines(&lines, num, SCREEN_MAX_STRING_SIZE_ONE_LINE);
  screen2d_ranges(screen, xrng, yrng);
  
  sprintf(*lines, "center: % .18e", screen->center[0]);
  sprintf(*(lines + 1), "        % .18e", screen->center[1]);
  sprintf(*(lines + 2), "zoom  : % .18e", screen->zoom);
  sprintf(*(lines + 3), "grid  : % .18e", screen->grid_size / screen->zoom);
  sprintf(*(lines + 4), "range : % .18e % .18e", xrng[0], xrng[1]);
  sprintf(*(lines + 5), "        % .18e % .18e", yrng[0], yrng[1]);

  screen2d_show_lines_from_top(screen, lines, num);
  screen2d_free_lines(&lines, num);
}

void screen2d_gnuplot_set_range(Screen2D *screen, FILE *pipe_gp){
  double xrng[2], yrng[2];
  screen2d_ranges(screen, xrng, yrng);
  fprintf(pipe_gp, "set xrange [%.18e:%.18e]\n", xrng[0], xrng[1]);
  fprintf(pipe_gp, "set yrange [%.18e:%.18e]\n", yrng[0], yrng[1]);
}

static void screen2d_gnuplot_get_variables(int *exp, double *base, double min, double max){
  double factor, base_candidate, midpt;
  int base_integer;
  *exp = (int) floor(log10(max - min));
  factor = pow(10.0, -(double) *exp);
  base_integer = (int)floor(min * factor / 10.0);
  base_candidate = (double)(base_integer + 1) * 10.0 / factor;
  midpt = (min + max) / 2.0;
  if(base_candidate < midpt){
    *base = base_candidate;
  }else{
    *base = (double)base_integer * 10.0 / factor;
  }
}

static void screen2d_gnuplot_puts_tics(FILE *pipe_gp, int exp, double base, double min, double max){
  double rng, factor, size, real_pitch, img_pitch, real_crd, img_crd;
  bool first = true;

  rng = max - min;
  factor = pow(10, -floor(log10(rng)));
  size = rng * factor;
  if(size >= 5){
    img_pitch = 1.0;
  }else if(size >= 2){
    img_pitch = 0.5;
  }else{
    img_pitch = 0.2;
  }
  real_pitch = img_pitch / factor;

  img_crd = 0.0;
  real_crd = base;
  while(real_crd > min){
    if(real_crd < max){
      if(!first){
	fprintf(pipe_gp, ", ");
      }else{
	first = false;
      }
      fprintf(pipe_gp, "'%.1lf' %.18le", img_crd, real_crd);    
    }
    img_crd -= img_pitch;
    real_crd -= real_pitch;
  }

  img_crd = img_pitch;
  real_crd = base + real_pitch;
  while(real_crd < max){
    if(real_crd > min){
      if(!first){
	fprintf(pipe_gp, ", ");
      }else{
	first = false;
      }
      fprintf(pipe_gp, "'%.1lf' %.18le", img_crd, real_crd);    
    }
    img_crd += img_pitch;
    real_crd += real_pitch;
  }
}

void screen2d_gnuplot_set_divisions(Screen2D *screen, FILE *pipe_gp){
  int exp;
  double xrng[2], yrng[2], base;
  char format[32];

  screen2d_ranges(screen, xrng, yrng);
  screen2d_gnuplot_get_variables(&exp, &base, xrng[0], xrng[1]);

  if(exp > 2 || exp < -3){
    if(exp < 0){
      sprintf(format, "%%+.%dlf", -exp - 1);
    }else{
      sprintf(format, "%%+lf");
    }

    fprintf(pipe_gp, "set xlabel 'x1.0e%+d ", exp);
    fprintf(pipe_gp, format, base);
    fprintf(pipe_gp, "'\nset xtics(");
    screen2d_gnuplot_puts_tics(pipe_gp, exp, base, xrng[0], xrng[1]);
    fprintf(pipe_gp, ")\n");
  }

  screen2d_gnuplot_get_variables(&exp, &base, yrng[0], yrng[1]);
  if(exp > 2 || exp < -3){
    if(exp < 0){
      sprintf(format, "%%+.%dlf", -exp - 1);
    }else{
      sprintf(format, "%%+lf");
    }
    
    fprintf(pipe_gp, "set ylabel 'x1.0e%+d ", exp);
    fprintf(pipe_gp, format, base);
    fprintf(pipe_gp, "'\nset ytics(");
    screen2d_gnuplot_puts_tics(pipe_gp, exp, base, yrng[0], yrng[1]);
    fprintf(pipe_gp, ")\n");
  }
}

void color_count_reset(Color *col){
  col->count = 0;
}

void color_set_alpha(Color *col, float alpha){
  col->alpha = alpha;
}

void color_change(Color *col){
  double rgb[3] = { 1.0, 1.0, 1.0 };
  switch(col->count){
  case 0:
    rgb[0] = 0.0;
    break;
  case 1:
    rgb[1] = 0.0;
    break;
  case 2:
    rgb[2] = 0.0;
    break;
  case 3:
    rgb[0] = 0.0;
    rgb[1] = 0.0;
    break;
  case 4:
    rgb[1] = 0.0;
    rgb[2] = 0.0;
    break;
  case 5:
    rgb[0] = 0.0;
    rgb[2] = 0.0;
    break;
  }

  if(col->alpha >= 0.0 && col->alpha <= 1.0){
    glColor4f(rgb[0], rgb[1], rgb[2], col->alpha);
  }else{
    glColor3f(rgb[0], rgb[1], rgb[2]);
  }
  col->count += 1;
}

