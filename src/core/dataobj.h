#ifndef _DATAOBJ_H_
#define _DATAOBJ_H_

#include "header.h"

#include <tcutil.h>
#include <GL/freeglut.h>
#include <mpfr.h>

/* DATAOBJ_MAX_DOUBLE_ARY_SIZE must be divided by 8 */
#define DATAOBJ_MAX_DOUBLE_ARY_SIZE 1024
#define DATAOBJ_PTR_SIZE 1024

typedef enum {
  DATA_DOUBLE,
  DATA_MPFR,
  DATA_MPFI
} DataType;

typedef enum {
  DISPLAY_TYPE_LINES,
  DISPLAY_TYPE_LINE_STRIP,
  DISPLAY_TYPE_LINE_LOOP,
  DISPLAY_TYPE_LINES_POINTS,
  DISPLAY_TYPE_POINTS,
  DISPLAY_TYPE_DOTS,
  DISPLAY_TYPE_SURFACE
} DisplayType;

typedef struct {
  TCLIST *list;
  TCLIST *split;
  bool hold;
  DataType data_type;
  DisplayType display_type;
  int dim;
  double *cache;
  int cache_allocated;
  int cache_num;
  int prec;                     /* for MPFR and MPFI */
} DataObj;

double *dataobj_allocate_double_pt(int dim);
mpfr_ptr dataobj_allocate_mpfr_pt(int dim, int prec);
void dataobj_clear_mpfr_pt(mpfr_ptr ptr, int dim);

#define dataobj_mpfr_pt_element(vec, num) (vec + num)

DataObj *dataobj_new(int dim, DataType type);
#define dataobj_double_new(dim) dataobj_new(dim, DATA_DOUBLE)
#define dataobj_mpfr_new(dim) dataobj_new(dim, DATA_MPFR)
#define dataobj_mpfi_new(dim) dataobj_new(dim, DATA_MPFI)
void dataobj_delete(DataObj *dataobj);

void dataobj_hold(DataObj *dataobj);
void dataobj_unhold(DataObj *dataobj);
void dataobj_set_display_type(DataObj *dataobj, DisplayType type);
void dataobj_set_prec(DataObj *dataobj, int prec);
#define dataobj_get_prec(dataobj) dataobj->prec
void dataobj_insert(DataObj *dataobj, int num, void *pt);
void dataobj_copy_insert(DataObj *dataobj, int num, void *pt);
void dataobj_remove(DataObj *dataobj, int num);
void dataobj_change(DataObj *dataobj, int num, void *pt);
void dataobj_add_splitter(DataObj *dataobj, int split);
void dataobj_clear_splitter(DataObj *dataobj);
void dataobj_set_cache_for_double(DataObj *dataobj);
void dataobj_set_cache_for_mpfr(DataObj *dataobj, mpfr_ptr center, mpfr_ptr scale);
void dataobj_draw_coordinate(DataObj *dataobj);
void dataobj_draw_cross (DataObj *dataobj, double len);

#define dataobj_get_mpfr_ptr(dataobj, num) (*(mpfr_ptr *) tclistval2(dataobj->list, num))
#define dataobj_get_double_ptr(dataobj, num) (*(double **) tclistval2(dataobj->list, num))

#define dataobj_size(obj) tclistnum(obj->list)

#endif /* _DATAOBJ_H_ */
