#include "dataobj.h"

/* MAX_DATA_STRING_SIZE_ONE_LINE must be divided by 8 */
#define MAX_DATA_STRING_SIZE_ONE_LINE 64
#define NUMBER_OF_SPLIT 64
#define DATA_DELIMITERS "\t "
#define DIM 2

void dataobj_init(DataObject *obj){
  obj->split = NULL;
  obj->split_allocated = 0;
  obj->split_used = 0;
  obj->data_size = 0;
  ptdata_init(obj->data);
  obj->default_style = DEFAULT_STYLE;
}

static void dataobj_allocate_split(DataObject *obj){
  obj->split_allocated += NUMBER_OF_SPLIT;
  obj->split = (int *) realloc(obj->split, obj->split_allocated * sizeof(int));
  if (obj->split == NULL) {
    printf("Can not allocate memory.\n");
    abort();
  }
}

void dataobj_free(DataObject *obj){
  ptdata_free(obj->data);
  free(obj->split);
  dataobj_init(obj);
}

void dataobj_allocate_if_needed(DataObject *obj, int num){
  while(!ptdata_allocated_p(obj->data, num)){
    ptdata_allocate_farther(obj->data);
  }
}

void dataobj_set_split(DataObject *obj, int split_num){
  if(obj->split_used >= obj->split_allocated){
    dataobj_allocate_split(obj);
  }
  *(obj->split + obj->split_used) = split_num;
  obj->split_used += 1;
}

void dataobj_delete_all_splits(DataObject *obj){
  obj->split_used = 0;
}

void dataobj_add_dbl(DataObject *obj, double dbl){
  dataobj_allocate_if_needed(obj, obj->data_size);
  *ptdata_get_dbl_ptr(obj->data, obj->data_size) = dbl;
  obj->data_size += 1;
}

void dataobj_add_dbls(DataObject *obj, int num, double *dbls){
  int i;
  dataobj_allocate_if_needed(obj, obj->data_size + num - 1);
  for (i = 0; i < num; i++) {
    *(ptdata_get_dbl_ptr(obj->data, obj->data_size) + i) = *(dbls + i);
  }
  obj->data_size += num;
}

void dataobj_add_vdbls(DataObject *obj, int num, ...){
  va_list list;
  int i;
  dataobj_allocate_if_needed(obj, obj->data_size + num - 1);
  va_start( list, num );
  for (i = 0; i < num; i += 1) {
    *(ptdata_get_dbl_ptr(obj->data, obj->data_size) + i) = va_arg(list, double);
  }
  va_end(list);
  obj->data_size += num;
}

void dataobj_set_default_style(DataObject *obj, PlotStyle style){
  obj->default_style = style;
}

void dataobj_read_line_init(int *max_line_size, char **line){
  *max_line_size = MAX_DATA_STRING_SIZE_ONE_LINE;
  *line = (char *) malloc(sizeof(char) * (*max_line_size));
}

char *dataobj_gets_line(char **line, int *line_size, int *max_line_size, FILE *fp){
  if(fgets(*line, *max_line_size - 1, fp) != NULL){
    do {
      *line_size = strlen(*line);
      if((*line)[*line_size - 1] == '\n'){
	(*line)[*line_size - 1] = '\0';
	*line_size -= 1;
	break;
      }
      *max_line_size += MAX_DATA_STRING_SIZE_ONE_LINE;
      *line = (char *) realloc(*line, sizeof(char) * (*max_line_size));
    } while(fgets(*line + (*line_size), MAX_DATA_STRING_SIZE_ONE_LINE, fp) != NULL);
    return *line;
  }else{
    return NULL;
  }
}

void dataobj_init_read_file(DataObject *obj, const char *file){
  DATA_FILE *data_file;
  gchar **buf;
  int max_line_size, line_size, i;
  char *line;
  double pt[2];

  dataobj_init(obj);
  data_file = data_file_new(file);
  data_file_open_read(data_file);
  data_file_init_line(&max_line_size, &line);
  while (data_file_gets_line(&line, &line_size, &max_line_size, data_file)) {
    if (line[0] == '\0') {
      dataobj_set_split(obj, obj->data_size);
    } else if (line[0] != '#') {
      buf = g_strsplit_set(line, DATA_DELIMITERS, 0);
      for (i = 0; i < DIM; i++) {
        if (buf[i]) {
          pt[i] = atof(buf[i]);
        } else {
          fprintf(stderr, "Invalid data: number of culomns of data and dimension does not coincide.\n");
          abort();
        }
      }
      dataobj_allocate_if_needed(obj, obj->data_size + 1);
      *ptdata_get_dbl_ptr(obj->data, obj->data_size) = pt[0];
      *ptdata_get_dbl_ptr(obj->data, obj->data_size + 1) = pt[1];
      obj->data_size += 2;
      g_strfreev(buf);
    }
  }
  free(line);
  data_file_delete(data_file);
}

void dataobj_draw_lines(DataObject *obj){
  if(obj->split_used > 0){
    int i;
    draw_ptdata_lines(obj->data, 0, obj->split[0]);
    for(i = 0; i < obj->split_used - 1; i++){
      draw_ptdata_lines(obj->data, obj->split[i], obj->split[i + 1]);
    }
    draw_ptdata_lines(obj->data, obj->split[i], obj->data_size);
  }else{
    draw_ptdata_lines(obj->data, 0, obj->data_size);
  }
}

void dataobj_draw_dots(DataObject *obj){
  draw_ptdata_dots(obj->data, 0, obj->data_size);
}

void dataobj_draw_quads(DataObject *obj){
  draw_ptdata_quads(obj->data, 0, obj->data_size);
}

void dataobj_draw_quads_edge(DataObject *obj){
  draw_ptdata_quads_edge(obj->data, 0, obj->data_size);
}

void dataobj_draw_points_cross(DataObject *obj, double size){  
  draw_ptdata_point_cross(obj->data, 0, obj->data_size, size);
}

void dataobj_output_to_gnuplot(DataObject *obj, FILE *pipe_gp){
  int start, i;
  start = 0;
  for (i = 0; i < obj->split_used; i++) {
    draw_ptdata_to_gnuplot(obj->data, start, *(obj->split + i), pipe_gp);
    fprintf(pipe_gp, "\n");
    start = *(obj->split + i);
  }
  if (start < obj->data_size) {
    draw_ptdata_to_gnuplot(obj->data, start, obj->data_size, pipe_gp);
  }
}

void dataobj_output_to_gnuplot_for_quads(DataObject *obj, FILE *pipe_gp){
  draw_ptdata_to_gnuplot_as_quads(obj->data, 0, obj->data_size, pipe_gp);
}

void dataobj_output_to_gnuplot_default_style(DataObject *obj, FILE *pipe_gp){
  switch(obj->default_style){
  case STY_QUADS:
  case STY_QUADS_EDGE:
    dataobj_output_to_gnuplot_for_quads(obj, pipe_gp);
    break;
  default:
    dataobj_output_to_gnuplot(obj, pipe_gp);
    break;
  }
  fprintf(pipe_gp, "e\n");
}

