#include "display_func_fr.h"

#define GNUPLOT_PREFIX "img"
#define GNUPLOT_MAX_NAME 100

void mpfr_disp_draw_objects(DisplayState *disp, int disp_start_num, MPFRScreen2D *screen, Color *col, MPFRDataObject *objects, int obj_num){
  if(disp_number_showing_mfds(disp) > 0){
    int i;
    for(i = 0; i < obj_num; i++){
      color_change(col);
      if(disp->mfd[i + disp_start_num]){
	switch((objects + i)->default_style){
	case STY_DOTS:
	  mpfr_dataobj_draw_dots(objects + i);
	  break;
	case STY_LINES:
	  mpfr_dataobj_draw_lines(objects + i);
	  break;
	case STY_POINTS:
	  mpfr_dataobj_draw_points_cross(objects + i, screen->dbl_scrn->cross_size / screen->dbl_scrn->zoom);
	  break;
	case STY_LINES_POINTS:
	  mpfr_dataobj_draw_lines(objects + i);
	  mpfr_dataobj_draw_points_cross(objects + i, screen->dbl_scrn->cross_size / screen->dbl_scrn->zoom);
	  break;
	case STY_QUADS:
	  mpfr_dataobj_draw_quads(objects + i);
	  break;
	case STY_QUADS_EDGE:
	  mpfr_dataobj_draw_quads_edge(objects + i);
	  break;
	}
      }
    }
  }
}

void mpfi_disp_draw_objects(DisplayState *disp, int disp_start_num, MPFRScreen2D *screen, Color *col, MPFIDataObject *objects, int obj_num){
  if(disp_number_showing_mfds(disp) > 0){
    int i;
    for(i = 0; i < obj_num; i++){
      color_change(col);
      if(disp->mfd[i + disp_start_num]){
	switch((objects + i)->default_style){
	case STY_DOTS:
	  /* mpfr_dataobj_draw_dots(objects + i); */
	  /* break; */
	case STY_LINES:
	  /* mpfr_dataobj_draw_lines(objects + i); */
	  /* break; */
	case STY_POINTS:
	  /* mpfr_dataobj_draw_points_cross(objects + i, screen->dbl_scrn->cross_size / screen->dbl_scrn->zoom); */
	  /* break; */
	case STY_LINES_POINTS:
	  /* mpfr_dataobj_draw_lines(objects + i); */
	  /* mpfr_dataobj_draw_points_cross(objects + i, screen->dbl_scrn->cross_size / screen->dbl_scrn->zoom); */
	  /* break; */
	case STY_QUADS:
	  mpfi_dataobj_draw_quads(objects + i);
	  break;
	case STY_QUADS_EDGE:
	  mpfi_dataobj_draw_quads_edge(objects + i);
	  break;
	}
      }
    }
  }
}

void mpfr_disp_gnuplot_objects(DisplayState *disp, MPFRDataObject *fr_objects, int fr_obj_num,
			       MPFIDataObject *fi_objects, int fi_obj_num, MPFRScreen2D *screen){
  if(disp_number_showing_mfds(disp) > 0){
    char gp_file[GNUPLOT_MAX_NAME];
    time_t now;
    struct tm *t_st_now;
    FILE *gp;
    int i;
    bool first = true;

    time(&now);
    t_st_now = localtime(&now);
    sprintf(gp_file, "%s%d%02d%02d%02d%02d%02d", GNUPLOT_PREFIX,
	    t_st_now->tm_year+1900, t_st_now->tm_mon+1, t_st_now->tm_mday, t_st_now->tm_hour, t_st_now->tm_min, t_st_now->tm_sec);

    gp = popen("gnuplot","w");
    printf("Making %s.eps\n", gp_file);
    fprintf(gp, "set term postscript eps\nset output '%s.eps'\n", gp_file);
    screen2d_gnuplot_set_range(screen->dbl_scrn, gp);
    fprintf(gp, "set size ratio -1\n");
    mpfr_screen2d_gnuplot_set_divisions(screen, gp);

    fprintf(gp, "plot ");
    for(i = 0; i < fr_obj_num; i++){
      if(disp->mfd[i]){
	if(!first){
	  fprintf(gp, ", ");
	}else{
	  first = false;
	}
	draw_ptdata_puts_gnuplot_style(gp, (fr_objects + i)->default_style);
      }
    }
    for(i = 0; i < fi_obj_num; i++){
      if(disp->mfd[i + fr_obj_num]){
	if(!first){
	  fprintf(gp, ", ");
	}else{
	  first = false;
	}
	draw_ptdata_puts_gnuplot_style(gp, (fi_objects + i + fr_obj_num)->default_style);
      }
    }
    fprintf(gp, "\n");
      
    for(i = 0; i < fr_obj_num; i++){
      if(disp->mfd[i]){
	mpfr_dataobj_output_to_gnuplot_default_style(fr_objects + i, gp);
      }
    }
    for(i = 0; i < fi_obj_num; i++){
      if(disp->mfd[i + fr_obj_num]){
	mpfi_dataobj_output_to_gnuplot_default_style(fi_objects + i, gp);
      }
    }

    pclose(gp);
    printf("Finish making %s.eps\n", gp_file);
  }
}

