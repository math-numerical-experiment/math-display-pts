#include "imagefiles.h"

#define CREATEIMAGE_BASENAME_MAX_SIZE 1024

typedef struct {
  uint32_t bfSize;
  uint16_t bfReserved1;
  uint16_t bfReserved2;
  uint32_t bfOffBits;
} BMPFILEHEADER;

typedef struct {
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPixPerMeter;
  uint32_t biYPixPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImporant;
} BMPINFOHEADER;

void disp_cimage_init(CreateImage *cimage)
{
  cimage->basename = malloc(sizeof(char) * CREATEIMAGE_BASENAME_MAX_SIZE);
  cimage->count = 1;
  cimage->format = GL2PS_EPS;
}

void disp_cimage_free(CreateImage *cimage)
{
  if (cimage->basename) free(cimage->basename);
}

void disp_cimage_set_basename(CreateImage *cimage, const char *name)
{
  strncpy(cimage->basename, name, CREATEIMAGE_BASENAME_MAX_SIZE);
  if (cimage->basename[CREATEIMAGE_BASENAME_MAX_SIZE - 1] != '\0') {
    cimage->basename[CREATEIMAGE_BASENAME_MAX_SIZE - 1] = '\0';
  }
}

void disp_cimage_set_format(CreateImage *cimage, int format)
{
  cimage->format = format;
}

static char *disp_cimage_filename(CreateImage *cimage, const char *extention)
{
  char *filename;
  const char *extname;
  int size, num;
  time_t now;
  struct tm *t_st_now;
  struct stat st;

  num = 0;
  time(&now);
  t_st_now = localtime(&now);

  if (!extention) {
    switch(cimage->format){
    case GL2PS_PS:
      extname = ".ps";
      break;
    case GL2PS_EPS:
      extname = ".eps";
      break;
    case GL2PS_PDF:
      extname = ".pdf";
      break;
    case GL2PS_TEX:
      extname = ".tex";
      break;
    case GL2PS_SVG:
      extname = ".svg";
      break;
    }
  } else {
    extname = extention;
  }

  size = asprintf(&filename, "%s%02d_%d%02d%02d_%02d%02d%s", cimage->basename, cimage->count,
                  t_st_now->tm_year+1900, t_st_now->tm_mon+1, t_st_now->tm_mday,
                  t_st_now->tm_hour, t_st_now->tm_min, extname);

  while (num < 100) {
    if (size > 0) {
      if (stat(filename, &st) != 0) {
        return filename;
      }  else {
        num++;
        free(filename);
        size = asprintf(&filename, "%s%02d_%d%02d%02d_%02d%02d_%d%s", cimage->basename, cimage->count,
                        t_st_now->tm_year+1900, t_st_now->tm_mon+1, t_st_now->tm_mday,
                        t_st_now->tm_hour, t_st_now->tm_min, num, extname);
      }
    } else {
      break;
    }
  }
  return NULL;
}

void disp_cimage_output(CreateImage *cimage, void (*display)(void))
{
  FILE *fp;
  char *filename;
  int state = GL2PS_OVERFLOW, buffsize = 0;
  GLint viewport[4];
  int options = GL2PS_BEST_ROOT | GL2PS_OCCLUSION_CULL | GL2PS_USE_CURRENT_VIEWPORT;
  filename = disp_cimage_filename(cimage, NULL);

  fp = fopen(filename, "wb");
  if (!fp) {
    fprintf(stderr, "Unable to open file %s.\n", filename);
    exit(1);
  }
  printf("Saving image to file %s... ", filename);
  fflush(stdout);
  while (state == GL2PS_OVERFLOW) {
    buffsize += 1024*1024;
    gl2psBeginPage(filename, "gl2psTest", viewport, cimage->format, GL2PS_BSP_SORT, options,
                   GL_RGBA, 0, NULL, 0, 0, 0,
                   buffsize, fp, filename);
    (*display)();
    state = gl2psEndPage();
  }
  fclose(fp);
  free(filename);
  printf("Done!\n");
  cimage->count++;
}

void disp_cimage_screenshot(CreateImage *cimage, int width, int height)
{
  const int bpp = 24;
  const int datasize = height * ((((width * bpp / 8) + 3) >> 2) << 2);
  const int filesize = 2 + sizeof(BMPFILEHEADER) + sizeof(BMPINFOHEADER) + datasize;
  BMPFILEHEADER bmfh = { filesize, 0, 0, 54 };
  BMPINFOHEADER bmih = { 40, width, height, 1, bpp, 0, 0, 0, 0, 0, 0 };
  int format = GL_BGR;
  char *buf, *filename;

  filename = disp_cimage_filename(cimage, ".bmp");
  printf("Saving image to file %s... ", filename);
  fflush(stdout);

  buf = malloc(sizeof(char) * datasize);
  if (!buf) {
    fprintf(stderr, "Can not allocate memory of screenshot.\n");
    abort();
  }
  glReadPixels(0, 0, width, height, format, GL_UNSIGNED_BYTE, buf);

  FILE *fp;
  fp = fopen(filename, "wb");
  if (!fp) {
    fprintf(stderr, "Can not open %s\n", filename);
    abort();
  }
  fwrite("BM", sizeof(char), 2, fp);
  fwrite(&bmfh, sizeof(BMPFILEHEADER), 1, fp);
  fwrite(&bmih, sizeof(BMPINFOHEADER), 1, fp);
  fwrite(buf, sizeof(char), datasize, fp);
  fclose(fp);
  printf("Done!\n");
}
