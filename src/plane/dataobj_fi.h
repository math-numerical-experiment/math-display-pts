#ifndef _DATAOBJ_FI_H_
#define _DATAOBJ_FI_H_

#include <stdarg.h>
#include "ptdata_fi.h"
#include "draw_ptdata.h"
#include "dataobj.h"

typedef struct __MPFIDataObject{
  MPFIPtData data[1];
  int data_size;
  int *split;
  int split_used;
  int split_allocated;
  PtData *dbl;
  int dbl_size;
  PtData *quads;
  int quads_size;
  PlotStyle default_style;
} MPFIDataObject;

void mpfi_dataobj_init(MPFIDataObject *obj);
void mpfi_dataobj_free_fi(MPFIDataObject *obj);
void mpfi_dataobj_free_dbl(MPFIDataObject *obj);
void mpfi_dataobj_free_quads(MPFIDataObject *obj);
void mpfi_dataobj_free(MPFIDataObject *obj);

void mpfi_dataobj_allocate_fi_if_needed(MPFIDataObject *obj, int num);
void mpfi_dataobj_add_fi(MPFIDataObject *obj, mpfi_t fi);
void mpfi_dataobj_add_fis(MPFIDataObject *obj, int num, __mpfi_struct *fis);
void mpfi_dataobj_add_vfis(MPFIDataObject *obj, int num, ...);
void mpfi_dataobj_allocate_dbl_if_needed(MPFIDataObject *obj, int num);
void mpfi_dataobj_add_dbl(MPFIDataObject *obj, double dbl);
void mpfi_dataobj_add_dbls(MPFIDataObject *obj, int num, double *dbls);
void mpfi_dataobj_add_vdbls(MPFIDataObject *obj, int num, ...);
void mpfi_dataobj_allocate_quads_if_needed(MPFIDataObject *obj, int num);
void mpfi_dataobj_add_quads_dbl(MPFIDataObject *obj, double dbl);
void mpfi_dataobj_add_quads_dbls(MPFIDataObject *obj, int num, double *dbls);
void mpfi_dataobj_add_quads_vdbls(MPFIDataObject *obj, int num, ...);
void mpfi_dataobj_set_split(MPFIDataObject *obj, int split_num);
void mpfi_dataobj_delete_all_splits(MPFIDataObject *obj);

void mpfi_dataobj_mid_translation(double *ret, __mpfi_struct *x, __mpfr_struct *base_pt, __mpfr_struct *base_scale);
void mpfi_dataobj_quads_translation(double *ret_x, double *ret_y, __mpfi_struct *x, __mpfr_struct *base_pt, __mpfr_struct *base_scale);
void mpfi_dataobj_set_mpfi_to_dbl(MPFIDataObject *obj);
void mpfi_dataobj_set_dbl_data(MPFIDataObject *obj, __mpfr_struct *base_pt, __mpfr_struct *base_scale);
void mpfi_dataobj_set_mpfi_to_quads(MPFIDataObject *obj);
void mpfi_dataobj_set_quads_data(MPFIDataObject *obj,  __mpfr_struct *base_pt, __mpfr_struct *base_scale);
void mpfi_dataobj_set_default_style(MPFIDataObject *obj, PlotStyle style);
void mpfi_dataobj_init_read_file(MPFIDataObject *obj, const char *file);

void mpfi_dataobj_draw_lines(MPFIDataObject *obj);
void mpfi_dataobj_draw_dots(MPFIDataObject *obj);
void mpfi_dataobj_draw_quads(MPFIDataObject *obj);
void mpfi_dataobj_draw_quads_edge(MPFIDataObject *obj);
void mpfi_dataobj_draw_points_cross(MPFIDataObject *obj, double size);

void mpfi_dataobj_output_to_gnuplot(MPFIDataObject *obj, FILE *pipe_gp);
void mpfi_dataobj_output_to_gnuplot_for_quads(MPFIDataObject *obj, FILE *pipe_gp);
void mpfi_dataobj_output_to_gnuplot_default_style(MPFIDataObject *obj, FILE *pipe_gp);

#endif /* _DATAOBJ_FI_H_ */

