#ifndef _PTDATA_FI_H_
#define _PTDATA_FI_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <mpfr.h>
#include <mpfi.h>

#define MAX_MPFI_ARY_SIZE 1024
#define MPFI_PTDATA_PTR_SIZE 1024

typedef struct __MPFIPtData{
  __mpfi_struct **ptr;
  int size;
  int ptr_allocated;
  int ptr_max;
  int prec;
} MPFIPtData;

void mpfi_ptdata_init(MPFIPtData *ptdata);
void mpfi_ptdata_free(MPFIPtData *ptdata);
void mpfi_ptdata_allocate_farther(MPFIPtData *ptdata);
__mpfi_struct *mpfi_ptdata_get_mpfi_ptr(MPFIPtData *ptdata, int num);
bool mpfi_ptdata_allocated_p(MPFIPtData *ptdata, int num);
int mpfi_ptdata_set_data(MPFIPtData *ptdata, char *file);


#endif /* _PTDATA_FI_H_ */
