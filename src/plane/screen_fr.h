#ifndef _SCREEN_FR_H_
#define _SCREEN_FR_H_

#include <mpfr.h>
#include "screen.h"

#define SCREEN_MAX_SCALE 5.0e+4
#define SCREEN_MIN_SCALE 5.0e-2
#define SCREEN_MAX_SIZE 100.0

#define SCREEN_INITIAL_CRD_X 0.0
#define SCREEN_INITIAL_CRD_Y 0.0
#define SCREEN_INITIAL_ZOOM 1.0
#define SCREEN_INITIAL_CRD_SIZE_X 2.0
#define SCREEN_INITIAL_CRD_SIZE_y 2.0

#define SCREEN_DEFAULT_MOVE 1.0
#define SCREEN_DEFAULT_ZOOM 2.0
#define SCREEN_GRID_SIZE SCREEN_INITIAL_CRD_SIZE_X / 3.0
#define SCREEN_POINT_CROSS_SIZE 0.03

typedef struct __MPFRScreen2D{
  Screen2D dbl_scrn[1];
  __mpfr_struct *base_pt;
  mpfr_t base_scale;
  int prec;
} MPFRScreen2D;

void mpfr_screen2d_init(MPFRScreen2D *screen, int win_size_x, int win_size_y);
void mpfr_screen2d_set_prec(MPFRScreen2D *screen, int prec);

bool mpfr_screen2d_move(MPFRScreen2D *screen, double x, double y);
#define mpfr_screen2d_up(screen) mpfr_screen2d_move(screen, 0.0, screen->dbl_scrn->default_move / screen->dbl_scrn->zoom)
#define mpfr_screen2d_down(screen) mpfr_screen2d_move(screen, 0.0, -screen->dbl_scrn->default_move / screen->dbl_scrn->zoom)
#define mpfr_screen2d_left(screen) mpfr_screen2d_move(screen, -screen->dbl_scrn->default_move / screen->dbl_scrn->zoom, 0.0)
#define mpfr_screen2d_right(screen) mpfr_screen2d_move(screen, screen->dbl_scrn->default_move / screen->dbl_scrn->zoom, 0.0)
#define mpfr_screen2d_move_to(screen, x, y) mpfr_screen2d_move(screen, x - screen->dbl_scrn->center[0], y - screen->dbl_scrn->center[1])

bool mpfr_screen2d_zoom(MPFRScreen2D *screen, double zoom);
#define mpfr_screen2d_zoom_in(screen) mpfr_screen2d_zoom(screen, screen->dbl_scrn->default_zoom)
#define mpfr_screen2d_zoom_out(screen) mpfr_screen2d_zoom(screen, 1.0 / screen->dbl_scrn->default_zoom)
#define mpfr_screen2d_zoom_to(screen, amp) mpfr_screen2d_zoom(screen, amp / screen->dbl_scrn->zoom)

void mpfr_screen2d_change_base(MPFRScreen2D *screen, __mpfr_struct *new_pt, __mpfr_struct *new_scale);
void mpfr_screen2d_change_crd_dbl_to_fr(MPFRScreen2D *screen, __mpfr_struct *fr_crd, double *dbl_crd);
void mpfr_screen2d_change_crd_fr_to_dbl(MPFRScreen2D *screen, double *dbl_crd, __mpfr_struct *fr_crd);
bool mpfr_screen2d_translation(MPFRScreen2D *screen, double *ret, __mpfr_struct *x);
void mpfr_screen2d_change_base_scale(MPFRScreen2D *screen, double expansion);

void mpfr_screen2d_center_pt(MPFRScreen2D *screen, __mpfr_struct *center_pt);
void mpfr_screen2d_scale(MPFRScreen2D *screen, __mpfr_struct *scale);
void mpfr_screen2d_ranges(MPFRScreen2D *screen, __mpfr_struct *x, __mpfr_struct *y);
void mpfr_screen2d_grid_size(MPFRScreen2D *screen, mpfr_t grid_size);

void mpfr_screen2d_show_current_state(MPFRScreen2D *screen);
void mpfr_screen2d_show_basic_info(MPFRScreen2D *screen);
void mpfr_screen2d_show_scale_bar(MPFRScreen2D *screen);

void mpfr_screen2d_gnuplot_set_divisions(MPFRScreen2D *screen, FILE *pipe_gp);

#endif /* _SCREEN_FR_H_ */
