#include "display_gtk.h"

#define SCREEN_INITIAL_CRD_X 0.0
#define SCREEN_INITIAL_CRD_Y 0.0
#define SCREEN_INITIAL_ZOOM 1.0
#define SCREEN_INITIAL_CRD_SIZE_X 2.0
#define SCREEN_INITIAL_CRD_SIZE_y 2.0

#define SCREEN_DEFAULT_MOVE 1.0
#define SCREEN_DEFAULT_ZOOM 2.0
#define SCREEN_GRID_SIZE SCREEN_INITIAL_CRD_SIZE_X / 3.0
#define SCREEN_POINT_CROSS_SIZE 0.03

#define SCREEN_MOVEMENT 1.0
#define SCREEN_ROTATION 360.0 / 12.0

static void drawing_redisplay (GdkGLDrawable *gldrawable) {
  if (gdk_gl_drawable_is_double_buffered (gldrawable)) {
    gdk_gl_drawable_swap_buffers (gldrawable);
  } else {
    glFlush ();
  }
}

static GdkGLConfig* drawing_create_glconfig (void)
{
  GdkGLConfig *glconfig;
  /* Configure OpenGL-capable visual. */
  /* Try double-buffered visual */
  glconfig = gdk_gl_config_new_by_mode(GDK_GL_MODE_RGB | GDK_GL_MODE_DEPTH | GDK_GL_MODE_DOUBLE);
  if (glconfig == NULL) {
    g_print ("*** Cannot find the double-buffered visual.\n");
    g_print ("*** Trying single-buffered visual.\n");
    /* Try single-buffered visual */
    glconfig = gdk_gl_config_new_by_mode(GDK_GL_MODE_RGB | GDK_GL_MODE_DEPTH | GDK_GL_MODE_SINGLE);
    if (glconfig == NULL) {
      g_print ("*** No appropriate OpenGL-capable visual found.\n");
      return NULL;
    }
  }
  return glconfig;
}

static void display_func ()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  /* static GLfloat lit_amb[4]={1.0, 1.0, 1.0, 0.0}; */
  /* static GLfloat lit_dif[4]={1.0, 1.0, 1.0, 0.0}; */
  /* static GLfloat lit_spc[4]={1.0, 1.0, 1.0, 0.0}; */
  /* static GLfloat lit_pos[4]={3.0, 3.0, 5.0, 0.0}; */

  /* glLightfv(GL_LIGHT0, GL_AMBIENT, lit_amb); */
  /* glLightfv(GL_LIGHT0, GL_DIFFUSE, lit_dif); */
  /* glLightfv(GL_LIGHT0, GL_SPECULAR, lit_spc); */
  /* glLightfv(GL_LIGHT0, GL_POSITION, lit_pos); */

  /* glEnable(GL_LIGHT0); */
  /* glEnable(GL_LIGHTING); */
  
  /* gl2psEnable(GL2PS_BLEND); */
  
  /* glClearColor(1.0, 1.0, 1.0, 0.0); */
  /* glClearColor(1.0, 1.0, 1.0, 0.0); */

  glEnable(GL_LIGHT0);
  glEnable(GL_LIGHTING);
  
  glMatrixMode(GL_MODELVIEW);


  glEnable(GL_DEPTH_TEST);

  glColor4d(1.0, 0.3, 0.3, 1.0);

  GLfloat material_ambient[] = { 0.135, 0.2225, 0.1575, 1.0 };
  GLfloat material_diffuse[] = { 0.54, 0.89, 0.63, 1.0 };
  GLfloat material_specular[] = { 0.316228, 0.316228, 0.316228, 1.0 };
  GLfloat material_shininess[] = { 12.8 };
  /* GLfloat material_ambient[] = { 0.1, 0.1, 0.1, 1.0 }; */
  /* GLfloat material_diffuse[] = { 0.6, 0.6, 0.6, 1.0 }; */
  /* GLfloat material_specular[] = { 0.0, 0.0, 0.0, 1.0 }; */
  /* GLfloat material_shininess[] = { 20.0 }; */
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material_ambient);
  glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material_diffuse);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material_specular);
  glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, material_shininess);

  screen_plot_all_objects(screen);

  glDisable(GL_LIGHTING);

  if (settings->show_axis) {
    glColor4d(0.3, 0.3, 1.0, 1.0);
    screen_show_axis(screen);
  }

  glColor4d(0.6, 0.0, 0.6, 0.5);
  screen_show_plane(screen);

  glDisable(GL_DEPTH_TEST);
}

static gboolean drawing_expose_event (GtkWidget *widget, GdkEventExpose *event, gpointer data)
{
  GdkGLContext *glcontext = gtk_widget_get_gl_context (widget);
  GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (widget);
 
  if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)) {
    return FALSE;
  }
  display_func();

  drawing_redisplay(gldrawable);
  gdk_gl_drawable_gl_end (gldrawable);
  return TRUE;
}

static void drawing_realize (GtkWidget *widget, gpointer data)
{
  GdkGLContext *glcontext = gtk_widget_get_gl_context (widget);
  GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (widget);
  if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)) {
    return;
  }

  const char *init_plane = (const char *) data;

  glEnableClientState(GL_VERTEX_ARRAY);
  glShadeModel(GL_SMOOTH);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  /* glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE); */

  screen_initialize_font(screen);
  screen_apply_projection(screen);
  screen_apply_light_settings(screen);
  if (init_plane) {
    cli_screen_initial_rotation(screen, init_plane);
  }

  gdk_gl_drawable_gl_end (gldrawable);
}

static gboolean drawing_configure_event (GtkWidget *widget, GdkEventConfigure *event, gpointer data)
{
  if (screen->win_size[0] != event->width || screen->win_size[1] != event->height) {
    GdkGLContext *glcontext = gtk_widget_get_gl_context (widget);
    GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (widget);
 
    if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)) {
      return FALSE;
    }
    screen_change_window_size(screen, event->width, event->height);
    screen_apply_projection(screen);
    glViewport (0, 0, screen->win_size[0], screen->win_size[1]);
    gdk_gl_drawable_gl_end (gldrawable);
    return TRUE;
  }
  return FALSE;
}

static void drawing_key_func(GtkWidget *widget, GdkEventKey *event, gpointer data)
{
  GtkWidget *drawing_area;
  drawing_area = (GtkWidget *) data;
  GdkGLContext *glcontext = gtk_widget_get_gl_context (drawing_area);
  GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (drawing_area);
  if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)) {
    return;
  }

  switch (event->string[0]) {
  case 'q':
    gtk_main_quit();
    break;
  case 'C':
    screen_reset_rotation(screen);
    break;
  case 'r':
    screen_rotate_around_x(screen, SCREEN_ROTATION);
    break;
  case 'R':
    screen_rotate_around_x(screen, -SCREEN_ROTATION);
    break;
  case 'e':
    screen_rotate_around_y(screen, SCREEN_ROTATION);
    break;
  case 'E':
    screen_rotate_around_y(screen, -SCREEN_ROTATION);
    break;
  case 'w':
    screen_rotate_around_z(screen, SCREEN_ROTATION);
    break;
  case 'W':
    screen_rotate_around_z(screen, -SCREEN_ROTATION);
    break;
  case 'i':
    screen_zoom(screen, 2.0);
    break;
  case 'u':
    screen_zoom(screen, 1.0 / 2.0);
    break;
  case 'h':
    screen_left(screen, SCREEN_MOVEMENT);
    break;
  case 'j':
    screen_down(screen, SCREEN_MOVEMENT);
    break;
  case 'k':
    screen_up(screen, SCREEN_MOVEMENT);
    break;
  case 'l':
    screen_right(screen, SCREEN_MOVEMENT);
    break;
  case 'f':
    screen_forward(screen, SCREEN_MOVEMENT);
    break;
  case 'b':
    screen_backward(screen, SCREEN_MOVEMENT);
    break;
  case 'o':
    disp_cimage_screenshot(cimage, screen->win_size[0], screen->win_size[1]);
    break;
  case 'O':
    disp_cimage_output(cimage, display_func);
    break;
  default:
    return;
  }

  drawing_redisplay(gldrawable);
  gdk_gl_drawable_gl_end (gldrawable);
  gtk_widget_queue_draw(widget);
}

GtkWidget *create_drawing_window (const char *init_plane)
{
  GtkWidget *window;
  GtkWidget *drawing_area;
  GdkGLConfig *glconfig;
  glconfig = drawing_create_glconfig ();
  if ( glconfig == NULL ) {
    g_printerr ("Failed to create rendering configuration\n");
    return NULL;
  }
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), INITIAL_WINDOW_TITLE);

  drawing_area = gtk_drawing_area_new ();
  gtk_widget_set_size_request (drawing_area, INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);
  gtk_widget_set_gl_capability (drawing_area, glconfig, NULL, TRUE, GDK_GL_RGBA_TYPE);

  g_signal_connect (G_OBJECT (window), "delete_event", G_CALLBACK (gtk_main_quit), NULL);
  gtk_signal_connect(GTK_OBJECT(window), "key_press_event", GTK_SIGNAL_FUNC(drawing_key_func), (gpointer) drawing_area);

  g_signal_connect_after (G_OBJECT (drawing_area), "realize", G_CALLBACK (drawing_realize), (gpointer) init_plane);
  g_signal_connect (G_OBJECT (drawing_area), "configure_event", G_CALLBACK (drawing_configure_event), NULL);
  g_signal_connect (G_OBJECT (drawing_area), "expose_event", G_CALLBACK (drawing_expose_event), NULL);
  gtk_container_add (GTK_CONTAINER(window), drawing_area);
  return window;
}
