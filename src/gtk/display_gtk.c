#include "display_gtk.h"

#define INITIAL_SETTINGS_CENTER { 0.0, 0.0, 0.0 }
#define INITIAL_SETTINGS_SIZE { 8.0, 8.0, 10.0, 100.0 }
#define INITIAL_SETTINGS_LIGHT_POSITION { 1.0, 0.0, 1.0, 0.0 }
#define INITIAL_SETTINGS_PLANE_CENTER { 0.0, 0.0, -2.0 }

#define DIMENSION 3

static void parse_arguments (int argc, char *argv[])
{
  const char *progname = "display-double-3d";

  struct arg_lit *help = arg_lit0("h", "help", "print this help and exit");

  struct arg_file *pt_files = arg_filen("p", "points", "<file>", 0, argc + 2, "files for displaying points");
  struct arg_file *dots_files = arg_filen("d", "dots", "<file>", 0, argc + 2, "files for displaying dots");
  struct arg_file *lines_files = arg_filen("l", "lines", "<file>", 0, argc + 2, "files for displaying lines");
  struct arg_file *linespts_files = arg_filen("i", "linespoints", "<file>", 0, argc + 2, "files for displaying linespoints");
  struct arg_file *surface_files = arg_filen("s", "surface", "<file>", 0, argc + 2, "files for displaying surface");
  struct arg_file *files = arg_filen(NULL, NULL, "<file>", 0, argc + 2, "files for displaying lines");

  struct arg_str *init_plane = arg_strn(NULL, "plane", NULL, 0, 1, "Set the initial plane.");

  struct arg_end *end = arg_end(20);
  void* argtable[] = {
    pt_files, dots_files, lines_files, linespts_files, surface_files, help, files, init_plane, end
  };

  int nerrors = arg_parse(argc, argv, argtable);
  
  if (help->count > 0) {
    printf("Usage: %s", progname);
    arg_print_syntax(stdout, argtable,"\n");
    printf("Display points in files.\n\n");
    arg_print_glossary(stdout, argtable," %-30s %s\n");
    printf("\nReport bugs to <yt@math.sci.hokudai.ac.jp>.\n");
    exit(0);
  }

  if (nerrors > 0){
    /* Display the error details contained in the arg_end struct.*/
    arg_print_errors(stdout, end, progname);
    printf("Try '%s --help' for more information.\n", progname);
    exit(1);
  }

  DataFileArg *arg;
  arg = data_file_arg_new();
  data_file_arg_add(arg, pt_files, DISPLAY_TYPE_POINTS, DATA_DOUBLE, DIMENSION);
  data_file_arg_add(arg, dots_files, DISPLAY_TYPE_DOTS, DATA_DOUBLE, DIMENSION);
  data_file_arg_add(arg, linespts_files, DISPLAY_TYPE_LINES_POINTS, DATA_DOUBLE, DIMENSION);
  data_file_arg_add(arg, surface_files, DISPLAY_TYPE_SURFACE, DATA_DOUBLE, DIMENSION);
  data_file_arg_add(arg, files, DISPLAY_TYPE_LINE_STRIP, DATA_DOUBLE, DIMENSION);

  DataObj **obj_list;
  int obj_num, i;
  obj_list = data_file_arg_initialize_objects(arg, &obj_num);

  disp_cimage_init(cimage);
  disp_cimage_set_basename(cimage, progname);
  
  screen = screen_new(3, 256);
  screen_set_window_size(screen, INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);
  for (i = 0; i < obj_num; i++) {
    screen_add_object(screen, obj_list[i]);
  }
  screen_set_center(screen, settings->center);
  screen_set_size(screen, settings->size[0], settings->size[1]);
  screen_set_depth(screen, settings->size[2], settings->size[3]);
  screen_set_light_value(screen, GL_POSITION, settings->light_position);
  screen_set_basal_plane_center(screen, settings->plane_center);
  screen_set_basal_plane_type(screen, PLANE_GRID_XY);

  if (init_plane->count > 0) {
    settings->default_plane = init_plane->sval[0];
  }
}

int main (int argc, char *argv[])
{
  InitSettings init_setting = {
    INITIAL_SETTINGS_CENTER,
    INITIAL_SETTINGS_SIZE,
    INITIAL_SETTINGS_LIGHT_POSITION,
    INITIAL_SETTINGS_PLANE_CENTER,
    NULL,
    TRUE
  };

  settings = &init_setting;

  parse_arguments(argc, argv);

  int gtk_argc = 0;
  GtkWidget *window, *menu;

  gtk_init (&gtk_argc, NULL);
  gtk_gl_init (&gtk_argc, NULL);

  window = create_drawing_window (settings->default_plane);
  if ( window == NULL ) {
    g_printerr ("Failed to create window\n");
    return -1;
  }

  menu = create_menu_window();
  if ( menu == NULL ) {
    g_printerr ("Failed to create window\n");
    return -1;
  }

  gtk_widget_show_all (menu);
  gtk_widget_show_all (window);
  gtk_main ();
  return 0;
}

/* void reshape_func(int w, int h){ */
/*   screen_change_window_size(screen, w, h); */
/*   glViewport(0, 0, w, h); */
/* } */

/* void mouse_func(int button, int state, int x, int y){ */
/*   /\* if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){ *\/ */
/*   /\*   screen2d_move_to_pixel(screen, x, y); *\/ */
/*   /\*   glutPostRedisplay(); *\/ */
/*   /\* } *\/ */
/* } */


/* double sp_angle, spiral[3]; */

/* /\* void animate_func(int value) { *\/ */
/* /\*   /\\* DataObj *obj; *\\/ *\/ */
/* /\*   /\\* obj = screen_get_object(screen, 0); *\\/ *\/ */
/* /\*   /\\* sp_angle += SPIRAL_ROTATION; *\\/ *\/ */
/* /\*   /\\* spiral[0] += SPIRAL_ROTATION; *\\/ *\/ */
/* /\*   /\\* spiral[1] = cos(sp_angle); *\\/ *\/ */
/* /\*   /\\* spiral[2] = sin(sp_angle); *\\/ *\/ */
/* /\*   /\\* dataobj_copy_insert(obj, -1, spiral); *\\/ *\/ */
/* /\*   /\\* if (sp_angle > 2 * M_PI) { *\\/ *\/ */
/* /\*   /\\*   sp_angle -= 2 * M_PI; *\\/ *\/ */
/* /\*   /\\* } *\\/ *\/ */
/* /\*   glutPostRedisplay(); *\/ */
/* /\*   glutTimerFunc(100, animate_func, 0); *\/ */
/* /\* } *\/ */

