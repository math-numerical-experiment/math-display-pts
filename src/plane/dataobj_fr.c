#include "dataobj_fr.h"

#define NUMBER_OF_SPLIT 64
#define DATA_DELIMITERS "\t "
#define DIM 2

void mpfr_dataobj_init(MPFRDataObject *obj){
  obj->split = NULL;
  obj->split_allocated = 0;
  obj->split_used = 0;
  obj->data_size = 0;
  mpfr_ptdata_init(obj->data);
  obj->dbl = NULL;
  obj->dbl_size = 0;
}

void mpfr_dataobj_free_fr(MPFRDataObject *obj){
  mpfr_ptdata_free(obj->data);
  obj->data_size = 0;
}

void mpfr_dataobj_free_dbl(MPFRDataObject *obj){
  ptdata_free(obj->dbl);
  obj->dbl = NULL;
  obj->dbl_size = 0;
}

void mpfr_dataobj_free(MPFRDataObject *obj){
  mpfr_dataobj_free_fr(obj);
  mpfr_dataobj_free_dbl(obj);
  free(obj->split);
}



static void mpfr_dataobj_allocate_split(MPFRDataObject *obj){
  obj->split = (int *) realloc(obj->split, NUMBER_OF_SPLIT * sizeof(int));
  if (obj->split == NULL) {
    printf("Can not allocate memory.\n");
    abort();
  }
  obj->split_allocated += NUMBER_OF_SPLIT;
}

void mpfr_dataobj_allocate_fr_if_needed(MPFRDataObject *obj, int num){
  while(!mpfr_ptdata_allocated_p(obj->data, num)){
    mpfr_ptdata_allocate_farther(obj->data);
  }
}

void mpfr_dataobj_set_split(MPFRDataObject *obj, int split_num){
  if(obj->split_used >= obj->split_allocated){
    mpfr_dataobj_allocate_split(obj);
  }
  *(obj->split + obj->split_used) = split_num;
  obj->split_used += 1;
}

void mpfr_dataobj_delete_all_splits(MPFRDataObject *obj){
  obj->split_used = 0;
}

void mpfr_dataobj_add_fr(MPFRDataObject *obj, mpfr_t fr){
  mpfr_dataobj_allocate_fr_if_needed(obj, obj->data_size);
  mpfr_set(mpfr_ptdata_get_mpfr_ptr(obj->data, obj->data_size), fr, GMP_RNDN);
  obj->data_size += 1;
}

void mpfr_dataobj_add_frs(MPFRDataObject *obj, int num, __mpfr_struct *frs){
  int i;
  mpfr_dataobj_allocate_fr_if_needed(obj, obj->data_size + num - 1);
  for (i = 0; i < num; i++) {
    mpfr_set(mpfr_ptdata_get_mpfr_ptr(obj->data, obj->data_size) + i, frs + i, GMP_RNDN);
  }
  obj->data_size += num;
}

void mpfr_dataobj_add_vfrs(MPFRDataObject *obj, int num, ...){
  va_list list;
  int i;
  mpfr_dataobj_allocate_fr_if_needed(obj, obj->data_size + num - 1);
  va_start( list, num );
  for (i = 0; i < num; i += 1) {
    mpfr_set(mpfr_ptdata_get_mpfr_ptr(obj->data, obj->data_size) + i, va_arg(list, __mpfr_struct *), GMP_RNDN);
  }
  va_end(list);
  obj->data_size += num;
}

void mpfr_dataobj_allocate_dbl_if_needed(MPFRDataObject *obj, int num){
  while(!ptdata_allocated_p(obj->dbl, num)){
    ptdata_allocate_farther(obj->dbl);
  }
}

void mpfr_dataobj_add_dbl(MPFRDataObject *obj, double dbl){
  mpfr_dataobj_allocate_dbl_if_needed(obj, obj->dbl_size);
  *ptdata_get_dbl_ptr(obj->dbl, obj->dbl_size) = dbl;
  obj->dbl_size += 1;
}

void mpfr_dataobj_add_dbls(MPFRDataObject *obj, int num, double *dbls){
  int i;
  mpfr_dataobj_allocate_dbl_if_needed(obj, obj->dbl_size + num - 1);
  for (i = 0; i < num; i++) {
    *(ptdata_get_dbl_ptr(obj->dbl, obj->dbl_size) + i) = *(dbls + i);
  }
  obj->dbl_size += num;
}

void mpfr_dataobj_add_vdbls(MPFRDataObject *obj, int num, ...){
  va_list list;
  int i;
  mpfr_dataobj_allocate_dbl_if_needed(obj, obj->dbl_size + num - 1);
  va_start( list, num );
  for (i = 0; i < num; i += 1) {
    *(ptdata_get_dbl_ptr(obj->dbl, obj->dbl_size) + i) = va_arg(list, double);
  }
  va_end(list);
  obj->dbl_size += num;
}


static void mpfr_dataobj_initialize_dbl(MPFRDataObject *obj){
  if(obj->dbl == NULL){
    obj->dbl = (PtData *) malloc(sizeof(PtData));
    ptdata_init(obj->dbl);
  }
}

void mpfr_dataobj_translation(double *ret, __mpfr_struct *x, __mpfr_struct *base_pt, __mpfr_struct *base_scale){
  mpfr_t tmp;
  int prec[2] = { mpfr_get_prec(x), mpfr_get_prec(x + 1) };
  mpfr_init2(tmp, (prec[0] <= prec[1] ? prec[1] : prec[0]));
  mpfr_sub(tmp, x, base_pt, GMP_RNDN);
  mpfr_mul(tmp, tmp, base_scale, GMP_RNDN);
  *ret = mpfr_get_d(tmp, GMP_RNDN);
  mpfr_sub(tmp, (x + 1), (base_pt + 1), GMP_RNDN);
  mpfr_mul(tmp, tmp, base_scale, GMP_RNDN);
  *(ret + 1) = mpfr_get_d(tmp, GMP_RNDN);
  mpfr_clear(tmp);
}

void mpfr_dataobj_set_mpfr_to_dbl(MPFRDataObject *obj){
  int i;
  mpfr_dataobj_initialize_dbl(obj);
  for(i = 0; i < obj->data_size; i += 2){
    mpfr_dataobj_allocate_dbl_if_needed(obj, obj->dbl_size + 1);
    mpfr_dataobj_add_vdbls(obj, 2, mpfr_get_d(mpfr_ptdata_get_mpfr_ptr(obj->data, i), GMP_RNDN),
			   mpfr_get_d(mpfr_ptdata_get_mpfr_ptr(obj->data, i + 1), GMP_RNDN));
  }    
}

void mpfr_dataobj_set_dbl_data(MPFRDataObject *obj,  __mpfr_struct *base_pt, __mpfr_struct *base_scale){
  int i;
  mpfr_dataobj_initialize_dbl(obj);
  mpfr_dataobj_allocate_dbl_if_needed(obj, obj->data_size);
  for(i = 0; i < obj->data_size; i += 2){
    mpfr_dataobj_translation(ptdata_get_dbl_ptr(obj->dbl, i), mpfr_ptdata_get_mpfr_ptr(obj->data, i), base_pt, base_scale);
  }
  obj->dbl_size += obj->data_size;
}

void mpfr_dataobj_set_default_style(MPFRDataObject *obj, PlotStyle style){
  obj->default_style = style;
}

void mpfr_dataobj_init_read_file (MPFRDataObject *obj, const char *file)
{
  DATA_FILE *data_file;
  gchar **buf;
  int max_line_size, line_size, i;
  char *line;

  mpfr_dataobj_init(obj);
  data_file = data_file_new(file);
  data_file_open_read(data_file);
  data_file_init_line(&max_line_size, &line);
  while (data_file_gets_line(&line, &line_size, &max_line_size, data_file)) {
    if (line[0] == '\0') {
      mpfr_dataobj_set_split(obj, obj->data_size);
    } else if (line[0] != '#') {
      mpfr_dataobj_allocate_fr_if_needed(obj, obj->data_size + DIM);
      buf = g_strsplit_set(line, DATA_DELIMITERS, 0);
      for (i = 0; i < DIM; i++) {
        if (buf[i]) {
          mpfr_set_str(mpfr_ptdata_get_mpfr_ptr(obj->data, obj->data_size), buf[i], 10, GMP_RNDN);
          obj->data_size += 1;
        } else {
          fprintf(stderr, "Invalid data: number of culomns of data and dimension does not coincide.\n");
          abort();
        }
      }
      g_strfreev(buf);
    }
  }
  free(line);
  data_file_delete(data_file);
}

void mpfr_dataobj_draw_lines(MPFRDataObject *obj){
  if(obj->split_used > 0){
    int i;
    draw_ptdata_lines(obj->dbl, 0, obj->split[0]);
    for(i = 0; i < obj->split_used - 1; i++){
      draw_ptdata_lines(obj->dbl, obj->split[i], obj->split[i + 1]);
    }
    draw_ptdata_lines(obj->dbl, obj->split[i], obj->data_size);
  } else {
    draw_ptdata_lines(obj->dbl, 0, obj->data_size);
  }
}

void mpfr_dataobj_draw_dots(MPFRDataObject *obj){
  draw_ptdata_dots(obj->dbl, 0, obj->data_size);
}

void mpfr_dataobj_draw_quads(MPFRDataObject *obj){
  draw_ptdata_quads(obj->dbl, 0, obj->data_size);
}

void mpfr_dataobj_draw_quads_edge(MPFRDataObject *obj){
  draw_ptdata_quads_edge(obj->dbl, 0, obj->data_size);
}

void mpfr_dataobj_draw_points_cross(MPFRDataObject *obj, double size){  
  draw_ptdata_point_cross(obj->dbl, 0, obj->data_size, size);
}

void mpfr_dataobj_output_to_gnuplot(MPFRDataObject *obj, FILE *pipe_gp){
  draw_ptdata_to_gnuplot(obj->dbl, 0, obj->data_size, pipe_gp);
}

void mpfr_dataobj_output_to_gnuplot_for_quads(MPFRDataObject *obj, FILE *pipe_gp){
  draw_ptdata_to_gnuplot_as_quads(obj->dbl, 0, obj->data_size, pipe_gp);
}

void mpfr_dataobj_output_to_gnuplot_default_style(MPFRDataObject *obj, FILE *pipe_gp){
  switch(obj->default_style){
  case STY_QUADS:
  case STY_QUADS_EDGE:
    mpfr_dataobj_output_to_gnuplot_for_quads(obj, pipe_gp);
    break;
  default:
    mpfr_dataobj_output_to_gnuplot(obj, pipe_gp);
    break;
  }
  fprintf(pipe_gp, "e\n");
}

