#include "screen_fr.h"

void mpfr_screen2d_init(MPFRScreen2D *screen, int win_size_x, int win_size_y){
  screen->base_pt = (__mpfr_struct *) malloc(sizeof(__mpfr_struct) * 2);

  screen->prec = mpfr_get_default_prec();
  
  mpfr_init2(screen->base_pt, screen->prec);
  mpfr_init2(screen->base_pt + 1, screen->prec);
  mpfr_init2(screen->base_scale, screen->prec);

  mpfr_set_d(screen->base_pt, SCREEN_INITIAL_CRD_X, GMP_RNDN);
  mpfr_set_d(screen->base_pt + 1, SCREEN_INITIAL_CRD_Y, GMP_RNDN);
  mpfr_set_d(screen->base_scale, SCREEN_INITIAL_ZOOM, GMP_RNDN);

  screen2d_set_variables(screen->dbl_scrn, SCREEN_INITIAL_CRD_X, SCREEN_INITIAL_CRD_Y, SCREEN_INITIAL_ZOOM,
			 SCREEN_INITIAL_CRD_SIZE_X, SCREEN_INITIAL_CRD_SIZE_y, SCREEN_DEFAULT_MOVE, SCREEN_DEFAULT_ZOOM, SCREEN_GRID_SIZE, SCREEN_POINT_CROSS_SIZE);
  screen2d_set_win_size(screen->dbl_scrn, win_size_x, win_size_y);
}

void mpfr_screen2d_set_prec(MPFRScreen2D *screen, int prec){
  mpfr_t tmp;
  screen->prec = prec;

  mpfr_init2(tmp, screen->prec);

  mpfr_set(tmp, screen->base_pt, GMP_RNDN);
  mpfr_set_prec(screen->base_pt, prec);
  mpfr_set(screen->base_pt, tmp, GMP_RNDN);
  
  mpfr_set(tmp, screen->base_pt + 1, GMP_RNDN);
  mpfr_set_prec(screen->base_pt + 1, prec);
  mpfr_set(screen->base_pt + 1, tmp, GMP_RNDN);
  
  mpfr_set(tmp, screen->base_scale, GMP_RNDN);
  mpfr_set_prec(screen->base_scale, prec);
  mpfr_set(screen->base_scale, tmp, GMP_RNDN);

  mpfr_clear(tmp);
}

bool mpfr_screen2d_move(MPFRScreen2D *screen, double x, double y){
  double moved[2] = { screen->dbl_scrn->center[0] + x, screen->dbl_scrn->center[1] + y };
  if(moved[0] < SCREEN_MAX_SIZE && moved[0] > -SCREEN_MAX_SIZE && moved[1] < SCREEN_MAX_SIZE && moved[1] > -SCREEN_MAX_SIZE){
    screen2d_move(screen->dbl_scrn, x, y);
    return true;
  }else{
    return false;
  }
}

bool mpfr_screen2d_zoom(MPFRScreen2D *screen, double zoom){
  if((screen->dbl_scrn->zoom > SCREEN_MIN_SCALE && screen->dbl_scrn->zoom < SCREEN_MAX_SCALE) ||
     (screen->dbl_scrn->zoom <= SCREEN_MIN_SCALE && zoom > 1.0) ||
     (screen->dbl_scrn->zoom >= SCREEN_MAX_SCALE && zoom < 1.0)){
    screen2d_zoom(screen->dbl_scrn, zoom);
    return true;
  }else{
    return false;
  }
}

void mpfr_screen2d_change_base(MPFRScreen2D *screen, __mpfr_struct *new_pt, __mpfr_struct *new_scale){
  mpfr_set(screen->base_pt, new_pt, GMP_RNDN);
  mpfr_set(screen->base_pt + 1, new_pt + 1, GMP_RNDN);
  mpfr_set(screen->base_scale, new_scale, GMP_RNDN);
}

static void mpfr_screen2d_dbl_to_fr(mpfr_t fr, double dbl, mpfr_t center, mpfr_t scale){
  mpfr_t tmp;
  mpfr_init2(tmp, mpfr_get_prec(fr));

  mpfr_d_div(tmp, dbl, scale, GMP_RNDN);
  mpfr_add(tmp, tmp, center, GMP_RNDN);
  mpfr_set(fr, tmp, GMP_RNDN);
  
  mpfr_clear(tmp);
}

void mpfr_screen2d_change_crd_dbl_to_fr(MPFRScreen2D *screen, __mpfr_struct *fr_crd, double *dbl_crd){
  mpfr_screen2d_dbl_to_fr(fr_crd, *dbl_crd, screen->base_pt, screen->base_scale);
  mpfr_screen2d_dbl_to_fr(fr_crd + 1, *(dbl_crd + 1), screen->base_pt + 1, screen->base_scale);
}

static double mpfr_screen2d_fr_to_dbl(mpfr_t fr, mpfr_t center, mpfr_t scale){
  double ret;
  mpfr_t tmp;
  mpfr_init2(tmp, mpfr_get_prec(fr));

  mpfr_sub(tmp, fr, center, GMP_RNDN);
  mpfr_mul(tmp, tmp, scale, GMP_RNDN);
  ret = mpfr_get_d(tmp, GMP_RNDN);
  
  mpfr_clear(tmp);
  return ret;
}

void mpfr_screen2d_change_crd_fr_to_dbl(MPFRScreen2D *screen, double *dbl_crd, __mpfr_struct *fr_crd){
  *dbl_crd = mpfr_screen2d_fr_to_dbl(fr_crd, screen->base_pt, screen->base_scale);
  *(dbl_crd + 1) = mpfr_screen2d_fr_to_dbl(fr_crd + 1, screen->base_pt + 1, screen->base_scale);
}

void mpfr_screen2d_change_base_scale(MPFRScreen2D *screen, double expansion){
  __mpfr_struct *new_pt;
  mpfr_t new_scale;
  new_pt = (__mpfr_struct *) malloc(sizeof(__mpfr_struct) * 2);
  mpfr_init2(new_pt, screen->prec);
  mpfr_init2(new_pt + 1, screen->prec);
  mpfr_init2(new_scale, screen->prec);
  
  mpfr_screen2d_change_crd_dbl_to_fr(screen, new_pt, screen->dbl_scrn->center);
  mpfr_mul_d(new_scale, screen->base_scale, expansion * screen->dbl_scrn->zoom, GMP_RNDN);

  mpfr_screen2d_change_base(screen, new_pt, new_scale);

  screen2d_move_to(screen->dbl_scrn, 0, 0);
  screen2d_zoom_to(screen->dbl_scrn, 1.0);

  mpfr_clear(new_pt);
  mpfr_clear(new_pt + 1);
  free(new_pt);
  mpfr_clear(new_scale);
}

bool mpfr_screen2d_translation(MPFRScreen2D *screen, double *ret, __mpfr_struct *x){
  bool suited = true;
  mpfr_t tmp;
  int prec[2] = { mpfr_get_prec(x), mpfr_get_prec(x + 1) };
  mpfr_init2(tmp, (prec[0] <= prec[1] ? prec[1] : prec[0]));
  mpfr_sub(tmp, x, screen->base_pt, GMP_RNDN);
  mpfr_mul(tmp, tmp, screen->base_scale, GMP_RNDN);
  if(mpfr_cmp_d(tmp, SCREEN_MAX_SIZE) > 0 && mpfr_cmp_d(tmp, -SCREEN_MAX_SIZE) < 0){
    suited = false;
  }
  *ret = mpfr_get_d(tmp, GMP_RNDN);
  mpfr_sub(tmp, x + 1, screen->base_pt + 1, GMP_RNDN);
  mpfr_mul(tmp, tmp, screen->base_scale, GMP_RNDN);
  if(suited && mpfr_cmp_d(tmp, SCREEN_MAX_SIZE) > 0 && mpfr_cmp_d(tmp, -SCREEN_MAX_SIZE) < 0){
    suited = false;
  }
  *(ret + 1) = mpfr_get_d(tmp, GMP_RNDN);
  return suited;
}

void mpfr_screen2d_center_pt(MPFRScreen2D *screen, __mpfr_struct *center_pt){
  mpfr_t tmp[2];
  mpfr_init2(tmp[0], mpfr_get_prec(center_pt));
  mpfr_init2(tmp[1], mpfr_get_prec(center_pt + 1));

  mpfr_d_div(tmp[0], screen->dbl_scrn->center[0], screen->base_scale, GMP_RNDN);
  mpfr_d_div(tmp[1], screen->dbl_scrn->center[1], screen->base_scale, GMP_RNDN);
  mpfr_add(tmp[0], tmp[0], screen->base_pt, GMP_RNDN);
  mpfr_add(tmp[1], tmp[1], screen->base_pt + 1, GMP_RNDN);
  mpfr_set(center_pt, tmp[0], GMP_RNDN);
  mpfr_set(center_pt + 1, tmp[1], GMP_RNDN);
  
  mpfr_clear(tmp[0]);
  mpfr_clear(tmp[1]);
}

void mpfr_screen2d_scale(MPFRScreen2D *screen, __mpfr_struct *scale){
  mpfr_mul_d(scale, screen->base_scale, screen->dbl_scrn->zoom, GMP_RNDN);
}

void mpfr_screen2d_ranges(MPFRScreen2D *screen, __mpfr_struct *x, __mpfr_struct *y){
  __mpfr_struct *center_pt;
  mpfr_t dist_x, dist_y;

  center_pt = (__mpfr_struct *) malloc(sizeof(__mpfr_struct) * 2);
  mpfr_init2(center_pt, mpfr_get_prec(x));
  mpfr_init2(center_pt + 1, mpfr_get_prec(y));

  mpfr_init2(dist_x, mpfr_get_prec(x));
  mpfr_init2(dist_y, mpfr_get_prec(y));
  
  mpfr_screen2d_center_pt(screen, center_pt);
  mpfr_d_div(dist_x, screen->dbl_scrn->size[0] / screen->dbl_scrn->zoom, screen->base_scale, GMP_RNDN);
  mpfr_d_div(dist_y, screen->dbl_scrn->size[1] / screen->dbl_scrn->zoom, screen->base_scale, GMP_RNDN);
  mpfr_sub(x, center_pt, dist_x, GMP_RNDN);
  mpfr_add(x + 1, center_pt, dist_x, GMP_RNDN);
  mpfr_sub(y, center_pt + 1, dist_y, GMP_RNDN);
  mpfr_add(y + 1, center_pt + 1, dist_y, GMP_RNDN);
  
  mpfr_clear(center_pt);
  mpfr_clear(center_pt + 1);
  free(center_pt);
  mpfr_clear(dist_x);
  mpfr_clear(dist_y);
}

void mpfr_screen2d_grid_size(MPFRScreen2D *screen, mpfr_t grid_size){
  mpfr_d_div(grid_size, screen->dbl_scrn->grid_size / screen->dbl_scrn->zoom, screen->base_scale, GMP_RNDN);
}

void mpfr_screen2d_show_current_state(MPFRScreen2D *screen){
  char **lines;
  int num = 8;
  __mpfr_struct *center_pt, *range_x, *range_y;
  mpfr_t scale, grid_size;

  screen2d_malloc_lines(&lines, num, SCREEN_MAX_STRING_SIZE_ONE_LINE);

  center_pt = (__mpfr_struct *) malloc(sizeof(__mpfr_struct) * 2);
  range_x = (__mpfr_struct *) malloc(sizeof(__mpfr_struct) * 2);
  range_y = (__mpfr_struct *) malloc(sizeof(__mpfr_struct) * 2);
  mpfr_init(center_pt);
  mpfr_init(center_pt + 1);
  mpfr_init(range_x);
  mpfr_init(range_x + 1);
  mpfr_init(range_y);
  mpfr_init(range_y + 1);

  mpfr_init(scale);
  mpfr_init(grid_size);

  mpfr_screen2d_center_pt(screen, center_pt);
  mpfr_screen2d_scale(screen, scale);
  mpfr_screen2d_grid_size(screen, grid_size);
  mpfr_screen2d_ranges(screen, range_x, range_y);
  
  mpfr_sprintf(*lines, "center: % .24Re", center_pt);
  mpfr_sprintf(*(lines + 1), "        % .24Re", center_pt + 1);
  mpfr_sprintf(*(lines + 2), "zoom  : % .24Re", scale);
  mpfr_sprintf(*(lines + 3), "grid  : % .24Re", grid_size);
  mpfr_sprintf(*(lines + 4), "range : % .24Re", range_x);
  mpfr_sprintf(*(lines + 5), "        % .24Re", range_x + 1);
  mpfr_sprintf(*(lines + 6), "        % .24Re", range_y);
  mpfr_sprintf(*(lines + 7), "        % .24Re", range_y + 1);

  screen2d_show_lines_from_top(screen->dbl_scrn, lines, num);
  screen2d_free_lines(&lines, num);

  mpfr_clear(center_pt);
  mpfr_clear(center_pt + 1);
  free(center_pt);
  mpfr_clear(range_x);
  mpfr_clear(range_x + 1);
  free(range_x);
  mpfr_clear(range_y);
  mpfr_clear(range_y + 1);
  free(range_y);
  mpfr_clear(scale);
  mpfr_clear(grid_size);
}

void mpfr_screen2d_show_basic_info(MPFRScreen2D *screen){
  char **lines;
  int num = 10;
  double xrng[2], yrng[2];

  screen2d_malloc_lines(&lines, num, SCREEN_MAX_STRING_SIZE_ONE_LINE);
  
  mpfr_sprintf(*lines, "base points: % .24Re", screen->base_pt);
  mpfr_sprintf(*(lines + 1), "             % .24Re", screen->base_pt + 1);
  mpfr_sprintf(*(lines + 2), "base scale : % .24Re", screen->base_scale);
  sprintf(*(lines + 3), "precision  :  %d", (int)mpfr_get_default_prec());

  screen2d_ranges(screen->dbl_scrn, xrng, yrng);
  
  sprintf(*(lines + 4), "center     : % .18e", screen->dbl_scrn->center[0]);
  sprintf(*(lines + 5), "             % .18e", screen->dbl_scrn->center[1]);
  sprintf(*(lines + 6), "zoom       : % .18e", screen->dbl_scrn->zoom);
  sprintf(*(lines + 7), "grid       : % .18e", screen->dbl_scrn->grid_size / screen->dbl_scrn->zoom);
  sprintf(*(lines + 8), "range      : % .18e % .18e", xrng[0], xrng[1]);
  sprintf(*(lines + 9), "             % .18e % .18e", yrng[0], yrng[1]);

  screen2d_show_lines_from_top(screen->dbl_scrn, lines, num);
  screen2d_free_lines(&lines, num);
}

void mpfr_screen2d_show_scale_bar(MPFRScreen2D *screen){
  char scale[SCREEN_MAX_STRING_SIZE_ONE_LINE];
  mpfr_t tmp;

  glPushMatrix();

  glTranslated(screen->dbl_scrn->center[0], screen->dbl_scrn->center[1], 0);
  glScaled(1.0 / screen->dbl_scrn->zoom, 1.0 / screen->dbl_scrn->zoom, 1.0);
  glTranslated(-screen->dbl_scrn->size[0] + 0.5, -screen->dbl_scrn->size[1] + 0.15, 0);

  glBegin(GL_LINES);
  glVertex3d(-SCALE_BAR_LENGTH * 0.5, 0.0, 0.0);
  glVertex3d(SCALE_BAR_LENGTH * 0.5, 0.0, 0.0);
  glVertex3d(-SCALE_BAR_LENGTH * 0.5, SCALE_BAR_LENGTH * 0.05, 0.0);
  glVertex3d(-SCALE_BAR_LENGTH * 0.5, -SCALE_BAR_LENGTH * 0.05, 0.0);
  glVertex3d(SCALE_BAR_LENGTH * 0.5, SCALE_BAR_LENGTH * 0.05, 0.0);
  glVertex3d(SCALE_BAR_LENGTH * 0.5, -SCALE_BAR_LENGTH * 0.05, 0.0);
  glEnd();

  glTranslated(SCALE_BAR_LENGTH * 0.7, -SCALE_BAR_LENGTH * 0.06, 0);
  glScaled(0.1, 0.1, 1.0);

  mpfr_init2(tmp, screen->prec);
  
  mpfr_d_div(tmp, SCALE_BAR_LENGTH / screen->dbl_scrn->zoom, screen->base_scale, GMP_RNDN);

  mpfr_sprintf(scale, "% .24Re", tmp);
  glcRenderStyle(GLC_TRIANGLE);  
  glcRenderString(scale);

  mpfr_clear(tmp);
  
  glPopMatrix();
}

static void mpfr_screen2d_gnuplot_get_variables(int *exp, mpfr_t base, mpfr_t min, mpfr_t max){
  mpfr_t factor, base_candidate, base_integer, midpt;
  int prec;
  prec = mpfr_get_prec(base);
  mpfr_init2(factor, prec);
  mpfr_init2(base_candidate, prec);
  mpfr_init2(base_integer, prec);
  mpfr_init2(midpt, prec);

  mpfr_sub(factor, max, min, GMP_RNDN);
  mpfr_log10(factor, factor, GMP_RNDN);
  mpfr_floor(factor, factor);
  *exp = (int) mpfr_get_si(factor, GMP_RNDN);

  mpfr_neg(factor, factor, GMP_RNDN);
  mpfr_ui_pow(factor, 10, factor, GMP_RNDN);

  mpfr_mul(base_integer, min, factor, GMP_RNDN);
  mpfr_div_ui(base_integer, base_integer, 10, GMP_RNDN);
  mpfr_floor(base_integer, base_integer);
  mpfr_add_ui(base_candidate, base_integer, 1, GMP_RNDN);
  mpfr_mul_ui(base_candidate, base_candidate, 10, GMP_RNDN);
  mpfr_div(base_candidate, base_candidate, factor, GMP_RNDN);

  mpfr_add(midpt, min, max, GMP_RNDN);
  mpfr_div_ui(midpt, midpt, 2, GMP_RNDN);

  if(mpfr_cmp(base_candidate, midpt) <= 0){
    mpfr_set(base, base_candidate, GMP_RNDN);
  }else{
    mpfr_mul_ui(base, base_integer, 10, GMP_RNDN);
    mpfr_div(base, base, factor, GMP_RNDN);
  }

  mpfr_clear(factor);
  mpfr_clear(base_candidate);
  mpfr_clear(base_integer);
  mpfr_clear(midpt);
}

static void mpfr_screen2d_gnuplot_puts_tics(FILE *pipe_gp, int exp, mpfr_t base_pt, mpfr_t base_scale,
					    mpfr_t mark_base, mpfr_t min, mpfr_t max){
  mpfr_t rng, factor, size, real_pitch, real_crd;
  double print_pitch, print_crd;
  int prec = mpfr_get_prec(mark_base);
  bool first = true;

  mpfr_init2(rng, prec);
  mpfr_init2(factor, prec);
  mpfr_init2(size, prec);
  mpfr_init2(real_pitch, prec);
  mpfr_init2(real_crd, prec);

  mpfr_sub(rng, max, min, GMP_RNDN);
  mpfr_log10(factor, rng, GMP_RNDN);
  mpfr_neg(factor, factor, GMP_RNDN);
  mpfr_ceil(factor, factor);
  mpfr_ui_pow(factor, 10, factor, GMP_RNDN);

  mpfr_mul(size, rng, factor, GMP_RNDN);
  if(mpfr_cmp_ui(size, 5) >= 0){
    print_pitch = 1.0;
  }else if(mpfr_cmp_ui(size, 2) >= 0){
    print_pitch = 0.5;
  }else{
    print_pitch = 0.2;
  }
  mpfr_d_div(real_pitch, print_pitch, factor, GMP_RNDN);

  print_crd = 0.0;
  mpfr_set(real_crd, mark_base, GMP_RNDN);
  while(mpfr_cmp(real_crd, min) > 0){
    if(mpfr_cmp(real_crd, max) < 0){
      if(!first){
  	fprintf(pipe_gp, ", ");
      }else{
  	first = false;
      }
      fprintf(pipe_gp, "'%.1lf' %.18e", print_crd, mpfr_screen2d_fr_to_dbl(real_crd, base_pt, base_scale));
    }
    print_crd -= print_pitch;
    mpfr_sub(real_crd, real_crd, real_pitch, GMP_RNDN);
  }

  print_crd = print_pitch;
  mpfr_add(real_crd, mark_base, real_pitch, GMP_RNDN);
  while(mpfr_cmp(real_crd, max) < 0){
    if(mpfr_cmp(real_crd, min) > 0){
      if(!first){
  	fprintf(pipe_gp, ", ");
      }else{
  	first = false;
      }
      fprintf(pipe_gp, "'%.1lf' %.18e", print_crd, mpfr_screen2d_fr_to_dbl(real_crd, base_pt, base_scale));
    }
    print_crd += print_pitch;
    mpfr_add(real_crd, real_crd, real_pitch, GMP_RNDN);
  }

  mpfr_clear(rng);
  mpfr_clear(factor);
  mpfr_clear(size);
  mpfr_clear(real_pitch);
  mpfr_clear(real_crd);
}

void mpfr_screen2d_gnuplot_set_divisions(MPFRScreen2D *screen, FILE *pipe_gp){
  int exp;
  double xrng[2], yrng[2];
  mpfr_t mark_base;
  mpfr_t endpoints[2];
  char format[32];
  char str[10240];

  mpfr_init2(mark_base, screen->prec);
  mpfr_init2(endpoints[0], screen->prec);
  mpfr_init2(endpoints[1], screen->prec);

  screen2d_ranges(screen->dbl_scrn, xrng, yrng);
  
  mpfr_screen2d_dbl_to_fr(endpoints[0], xrng[0], screen->base_pt, screen->base_scale);
  mpfr_screen2d_dbl_to_fr(endpoints[1], xrng[1], screen->base_pt, screen->base_scale);
  mpfr_screen2d_gnuplot_get_variables(&exp, mark_base, endpoints[0], endpoints[1]);
  if(exp > 2 || exp < -3){
    if(exp < 0){
      sprintf(format, "%%+.%dRf", -exp - 1);
    }else{
      sprintf(format, "%%+Rf");
    }

    fprintf(pipe_gp, "set xlabel 'x1.0e%+d ", exp);
    mpfr_sprintf(str, format, mark_base);
    /* The following make segmentation fault. 2009/05/31 15:20:41 */
    /* mpfr_asprintf(str, format, mark_base); */
    fprintf(pipe_gp, "%s", str);
    fprintf(pipe_gp, "'\nset xtics(");
    mpfr_screen2d_gnuplot_puts_tics(pipe_gp, exp, screen->base_pt, screen->base_scale, mark_base, endpoints[0], endpoints[1]);
    fprintf(pipe_gp, ")\n");
  }

  mpfr_screen2d_dbl_to_fr(endpoints[0], yrng[0], screen->base_pt + 1, screen->base_scale);
  mpfr_screen2d_dbl_to_fr(endpoints[1], yrng[1], screen->base_pt + 1, screen->base_scale);
  mpfr_screen2d_gnuplot_get_variables(&exp, mark_base, endpoints[0], endpoints[1]);
  if(exp > 2 || exp < -3){
    if(exp < 0){
      sprintf(format, "%%+.%dRf", -exp - 1);
    }else{
      sprintf(format, "%%+Rf");
    }

    fprintf(pipe_gp, "set ylabel 'x1.0e%+d ", exp);
    mpfr_sprintf(str, format, mark_base);
    fprintf(pipe_gp, "%s", str);
    fprintf(pipe_gp, "'\nset ytics(");
    mpfr_screen2d_gnuplot_puts_tics(pipe_gp, exp, screen->base_pt + 1, screen->base_scale, mark_base, endpoints[0], endpoints[1]);
    fprintf(pipe_gp, ")\n");
  }
  
  mpfr_clear(mark_base);
  mpfr_clear(endpoints[0]);
  mpfr_clear(endpoints[1]);
}

