#include "quaternion.h"

void quat_copy(GLdouble ret[], GLdouble u[])
{
  ret[0] = u[0];
  ret[1] = u[1];
  ret[2] = u[2];
  ret[3] = u[3];
}

void quat_conjugate(GLdouble ret[], GLdouble r[])
{
  ret[0] = r[0];
  ret[1] = -r[1];
  ret[2] = -r[2];
  ret[3] = -r[3];
}

void quat_multiple(GLdouble ret[], GLdouble u[], GLdouble v[])
{
  ret[0] = u[0] * v[0] - u[1] * v[1] - u[2] * v[2] - u[3] * v[3];
  ret[1] = u[0] * v[1] + v[0] * u[1] + u[2] * v[3] - u[3] * v[2];
  ret[2] = u[0] * v[2] + v[0] * u[2] + u[3] * v[1] - u[1] * v[3];
  ret[3] = u[0] * v[3] + v[0] * u[3] + u[1] * v[2] - u[2] * v[1];
}

static void quat_normalize_vector(GLdouble ret[], GLdouble v[])
{
  GLdouble len;
  len = v[0] * v[0] + v[1] * v[1] + v[2] * v[2];
  if (len > 0) {
    len = sqrt(len);
    ret[0] = v[0] / len;
    ret[1] = v[1] / len;
    ret[2] = v[2] / len;
  } else {
    ret = NULL;
  }
}

void quat_rotation_to_quaternion(GLdouble q[], GLdouble angle, GLdouble direction[])
{
  GLdouble a, s, n[3];
  quat_normalize_vector(n, direction);
  a = angle / 2.0;
  s = sin(a);
  q[0] = cos(a);
  q[1] = n[0] * s;
  q[2] = n[1] * s;
  q[3] = n[2] * s;
}

void quat_rotation_matrix(GLdouble m[][4], GLdouble q[])
{
  m[0][0] = 1.0 - 2.0 * (q[2] * q[2] + q[3] * q[3]);
  m[0][1] = 2.0 * (q[1] * q[2] + q[0] * q[3]);
  m[0][2] = 2.0 * (q[1] * q[3] - q[0] * q[2]);
  m[0][3] = 0.0;

  m[1][0] = 2.0 * (q[1] * q[2] - q[0] * q[3]);
  m[1][1]= 1.0 - 2.0 * (q[1] * q[1] + q[3] * q[3]);
  m[1][2] = 2.0 * (q[2] * q[3] + q[0] * q[1]);
  m[1][3] = 0.0;

  m[2][0] = 2.0 * (q[1] * q[3] + q[0] * q[2]);
  m[2][1] = 2.0 * (q[2] * q[3] - q[0] * q[1]);
  m[2][2] = 1.0 - 2.0 * (q[1] * q[1] + q[2] * q[2]);
  m[2][3] = 0.0;

  m[3][0] = 0.0;
  m[3][1] = 0.0;
  m[3][2] = 0.0;
  m[3][3] = 1.0;
}

void quat_inverse_rotation_matrix(GLdouble m[][4], GLdouble q[])
{
  m[0][0] = 1.0 - 2.0 * (q[2] * q[2] + q[3] * q[3]);
  m[0][1] = 2.0 * (q[1] * q[2] - q[0] * q[3]);
  m[0][2] = 2.0 * (q[1] * q[3] + q[0] * q[2]);
  m[0][3] = 0.0;

  m[1][0] = 2.0 * (q[1] * q[2] + q[0] * q[3]);
  m[1][1]= 1.0 - 2.0 * (q[1] * q[1] + q[3] * q[3]);
  m[1][2] = 2.0 * (q[2] * q[3] - q[0] * q[1]);
  m[1][3] = 0.0;

  m[2][0] = 2.0 * (q[1] * q[3] - q[0] * q[2]);
  m[2][1] = 2.0 * (q[2] * q[3] + q[0] * q[1]);
  m[2][2] = 1.0 - 2.0 * (q[1] * q[1] + q[2] * q[2]);
  m[2][3] = 0.0;

  m[3][0] = 0.0;
  m[3][1] = 0.0;
  m[3][2] = 0.0;
  m[3][3] = 1.0;
}

